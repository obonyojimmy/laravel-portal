$(function () {
      
  var importerNames = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundimportersnames ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputImporters').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'importerNames',
  source: importerNames
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
   
    console.log(datum);
});

 });
