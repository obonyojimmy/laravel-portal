$(function () {
      
  var clearingAgents = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundclearingAgents ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputClearingAgent').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'clearingAgents',
  source: clearingAgents
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
     
   
    console.log(datum);
});

 });
