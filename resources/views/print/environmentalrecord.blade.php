<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
<div class="visible-print-block  container">
	<div class="row visible-print-block">
		<page size="A4">
	<div class="col-md-12">
	<h4 class="text-center printmargin05cm">RADIATION INSPECTION REPORT OF ENVIRONMENTAL GOODS</h4>
	
	<div class="row visible-print-block printmargin1cm">
		 <div class="col-xs-6 ">
			  <p> <strong>Ref : </strong> RSL/ADM/MSA/02/{{ $now->year  }}/<span class="print_span  print_car_id">{{ $allenvdata -> id or ' ' }}</span> </p>
		 </div>
		 <div class="col-xs-6 ">
			<p class="text-right"> <strong>Date : </strong> {{ $now  }}</p>
		 </div>
		 
	 </div>
	
	<div class="row visible-print-block printmargin1cm">
		<h5>1). Importers /Exporters details</h5>
		<p class="printmarginleft1cm"><strong>a) Importers/Exporter Name </strong> : <span class="print_span print_env_importers_name">{{ $allenvdata -> importer -> name or '' }}</span></p>
		<p class="printmarginleft1cm"><strong>b) Address </strong> : <span class="print_span print_importers_env_address">{{ $allenvdata -> importer -> address or ' ' }}</span></p>
		<p class="printmarginleft1cm"><strong>c) Customs Entry No</strong> : <span class="print_span print_env_customs_no">{{ $allenvdata -> customsno  or ' ' }}</span></p>
	</div>
    
    <div class="row visible-print-block printmargin1cm">
		<h5>2). Particulars of Consignments as declared by importer/Exporter</h5>
		
		<div class="col-xs-12 visible-print-inline-block">
					<p class="printmarginleft1cm"><strong> a) Goods Description : </strong> : <span class="print_span print_env_goods_desc">{{ $allenvdata -> description  or ' ' }}</span></p>
		           
		            <p class="printmarginleft1cm"><strong> b) 1)Date of Manufacture  </strong> : <span class="print_span print_env_d_man">{{ $allenvdata -> manufacture  or ' ' }}</span></p>
		            
		            
		            <p class="printmarginleft1cm"><strong>  2)Date of Expiry </strong> : <span class="print_span print_env_d_exp">{{ $allenvdata -> expiry  or ' ' }}</span></p>
		            
		            
		            <p class="printmarginleft1cm"><strong> c)Products Batch Marks/Codes  </strong> : <span class="print_span print_env_batch_codes">{{ $allenvdata -> batchcode  or ' ' }}</span></p>
		            
		            <p class="printmarginleft1cm"><strong> d)Batch size  </strong> : <span class="print_span print_env_batch_size">{{ $allenvdata -> batchsize  or ' ' }}</span></p>
		            
		            <p class="printmarginleft1cm"><strong> e)Destination  </strong> : <span class="print_span print_env_dest">{{ $allenvdata -> destcountry -> name  or ' ' }}</span></p>
		            
		            <p class="printmarginleft1cm"><strong> f)Clearing Agent </strong> : <span class="print_span print_env_clearing_agent">{{ $allenvdata -> clearingagent -> name  or ' ' }}</span></p>
		            
		            <p class="printmarginleft1cm"><strong> g)Shipping Vessel </strong> : <span class="print_span print_env_ship">{{ $allenvdata -> ship -> name or ' ' }}</span></p>
		            
		            <p class="printmarginleft1cm"><strong> h)Port of Exit </strong> : <span class="print_span print_env_port">{{ $allenvdata -> port  or ' ' }}</span></p>
		            
		           
		            
		            
		            
	    </div>
	    
    </div>
    
     
	  <div class="row visible-print-block printmargin1cm">
		  <h5> 3). Results of Analysis in Bq/Kg/Ltr</h5>
		   <div class="col-xs-12">
			   <p class="printmarginleft1cm"><strong> Background radiation Count </strong> : <span class="print_span print_env_Background">{{ $allenvdata -> environmentalinspection -> background  or ' ' }}</span></p>
		       <p class="printmarginleft1cm"><strong> Net counts </strong> : <span class="print_span print_env_net_counts">{{ $allenvdata -> environmentalinspection -> netcounts  or ' ' }}</span></p>
		        
		   </div>
	  </div>
	  <div class="row visible-print-block printmargin1cm">
		  <h5>4).Recommendations </h5>
		  <div class="col-md-12 ">
			  <p  class="print_span print_Inspection_info">{{ $allenvdata -> environmentalinspection -> info or ''  }} </p>
		  </div>
	   </div>
	  <div class="row visible-print-block printmargin1cm">
		 <div class="col-xs-6 ">
			  <h4 ><strong> Signature Of Officer : </strong> </h4>
		 </div>
		 <div class="col-xs-6 ">
			  <img id="print_Barcode">
		 </div>
		 
	 </div>
	
    </div>
	
    </div>
	</page>
</div>
