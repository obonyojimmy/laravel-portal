<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
<div class="visible-print-block  container">
	
	<page size="A4">
		@for ($i = 0; $i < 10; $i++)
			<span class="watermark" style="transform: rotate(<?php echo rand(0, 360);?>deg); top: <?php echo rand(0, 100);?>%;left: <?php echo rand(0, 100);?>%;"> &#169; Radiotech </span>
		@endfor
		
	<div class="row visible-print-block">
		
	<div class="col-xs-12">
	<h4 class="text-center printmargin05cm">RADIATION INSPECTION REPORT OF MOTOR VEHICLE</h4>
	<div class="row visible-print-block printmargin1cm">
		 <div class="col-xs-6 ">
			  <p> <strong>Ref : </strong> RSL/ADM/MSA/01/{{ $now->year  }}/<span class="print_span  print_car_id">{{ $allcardata -> id or ' ' }}</span> </p>
		 </div>
		 <div class="col-xs-6 ">
			<p class="text-right"> <strong>Date : </strong> {{ $now  }}</p>
		 </div>
		 
	 </div>
	
	<div class="row visible-print-block printmargin1cm">
		<h5>1). IMPORTER DETAILS</h5>
		<p class="printmarginleft1cm"><strong>a) Importers/Exporter Name </strong> : <span class="print_span print_importers_name">{{ $allcardata -> importer->name or ' ' }}</span></p>
		<p  class="printmarginleft1cm"><strong>b) Address </strong> : <span class="print_span print_importers_address">{{ $allcardata -> importer->address or ' ' }}</span></p>
		<p  class="printmarginleft1cm"><strong>c) Customs Entry No</strong> : <span class="print_span print_customs_no">{{ $allcardata -> customsno or ' ' }}</span></p>
	</div>
    
    <div class="row visible-print-block printmargin1cm">
		<h5>2). PARTICULARS OF CONSIGNMENTS AS DECLARED BY IMPORTER/EXPORTER</h5>
		<div class="row visible-print-block">
		<div class="col-xs-10 printmarginleft1cm">
			<table class="table table-bordered">
			  <tr>
			   <td><p><strong> Car make </strong> : <span class="print_span print_carmake">{{ $allcardata -> name or ' ' }}</span></p></td>
			   <td><p><strong> Colour </strong> : <span class="print_span print_Colour">{{ $allcardata -> colour or ' ' }}</span></p></td>
			  </tr>
			  <tr>
			   <td><p><strong> Chassis No </strong> : <span class="print_span print_chassis_no">{{ $allcardata -> chassisno or ' ' }}</span></p></td>
			   <td><p><strong> Pre Reg.Country </strong> : <span class="print_span print_Pre_Reg_Country">{{ $allcardata -> prevcountry->name or ' ' }}</span></p></td>
			  </tr>
			  <tr>
			   <td> <p><strong> Engine No </strong> : <span class="print_span print_engine_no">{{ $allcardata -> engineno or ' ' }}</span></p></td></td>
			   <td><p><strong> Man.Year </strong> : <span class="print_span print_Man_Year">{{ $allcardata -> year or ' ' }}</span></p>
			  </td>
			  </tr>
			  <tr>
			   <td><p><strong> Fuel </strong> : <span class="print_span print_fuel">{{ $allcardata -> fuel or ' ' }}</span></p></td>
			   <td><p><strong> Sticker No </strong> : <span class="print_span print_Sticker_No"></span></p></td>
			  </tr>
			</table>
				  
	    </div>
	   
      </div>
    </div>
    
     <div class="row visible-print-block printmargin1cm">
		   <div class="col-xs-12">
			   <p class="printmarginleft1cm"><strong> Destination </strong> : <span class="print_span print_Destination">{{ $allcardata -> destcountry->name or ' ' }}</span></p>
		       <p class="printmarginleft1cm"><strong> Clearing Agent </strong> : <span class="print_span print_Clearing_Agent">{{ $allcardata -> clearingagent->name or ' ' }}</span></p>
		        <p class="printmarginleft1cm"><strong> Shipping Vessel </strong> : <span class="print_span print_Shipping_Vessel">{{ $allcardata -> ship -> name or ' ' }}</span></p>
		   </div>
	  </div>
	  <div class="row visible-print-block printmargin1cm">
		  <h5>3). Radiation Survey Report in Microsievert Per Hour( µsv/hr )</h5>
		   <div class="col-md-12 ">
			   <p class="printmarginleft1cm"><strong> Background </strong> : <span class="print_span print_Background"><?php if(isset($allcardata->carinspection->background)){ $bckprint =implode(" ",$allcardata->carinspection->background); echo $bckprint ; } ?></span></p>
		       <p class="printmarginleft1cm"><strong> Exterior </strong> : <span class="print_span print_Exterior"><?php if(isset($allcardata->carinspection->exterior)){ $extprint =implode(" ",$allcardata->carinspection->exterior); echo $extprint ; } ?></span></p>
		        <p class="printmarginleft1cm"><strong> Engine </strong> : <span class="print_span print_Engine"><?php if(isset($allcardata->carinspection->engine)){ $engprint =implode(" ",$allcardata->carinspection->engine); echo $engprint ; } ?></span></p>
		        <p class="printmarginleft1cm"><strong> Interior </strong> : <span class="print_span print_Interior"><?php if(isset($allcardata->carinspection->interior)){ $intprint =implode(" ",$allcardata->carinspection->interior); echo $intprint ; } ?></span></p>
		        <p class="printmarginleft1cm"><strong> Extras </strong> : <span class="print_span print_Extras"><?php if(isset($allcardata->carinspection->extras)){ $extrasprint =implode(" ",$allcardata->carinspection->extras); echo $extrasprint ; } ?></span></p>
		   </div>
		   
		  
	  </div>
	   <div class="row visible-print-block printmargin1cm">
		  <h5>4).Recommendations </h5>
		  <div class="col-md-12 ">
			  <p  class="print_span print_Inspection_info">{{ $allcardata->carinspection->info or ' '  }} </p>
		  </div>
	   </div>
	 <div class="row visible-print-block printmargin1cm">
		 <div class="col-xs-6 ">
			  <h4 class="printmargin1cm"><strong> Gross Net Quartile : </strong> <span class="print_span print_Quartile" >{{  $allcardata -> quartiles or ''}} </span> Below limit 0.1143 </h4>
			  <h4 ><strong> Signature Of Officer : </strong> </h4>
		 </div>
		 <div class="col-xs-6 ">
			  <img id="print_Barcode">
		 </div>
		 
	 </div>
    </div>
	
    </div>
</page>	
</div>
