<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
if(isset($allinvdata->paymentdue)){
	$duedate = new Carbon($allinvdata->paymentdue) ;
    $dueDate = $duedate ->format('d/m/Y');
}

?>
<div class="visible-print-block  container">
 <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">
              <i class="fa fa-globe"></i> Radiotech Ltd
              <small class="pull-right">Date: {{ $now -> format('l jS \\of F Y h:i:s A')  }}</small>
            </h2>
          </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">
            From
            <address>
              <strong>Radiotech Ltd</strong><br>
              Mombasa, 1314-80100 <br>
              Phone: 0714234044<br>
              Email: info@hakikahost.net
            </address>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
            To
            <address>
              <strong class="print_span print_env_inv_name"></strong><br>
              Address: <span class="print_span print_env_inv_address">{{ $allinvdata->address or ''}}</span><br>
              Id Number: <span class="print_span print_env_inv_id_no">{{ $allinvdata->idnumber or ''}}</span><br>
              Phone: <span class="print_span print_env_inv_phone">{{ $allinvdata->phone or ''}}</span><br>
              Email: <span class="print_span print_env_inv_email">{{ $allinvdata->email or ''}}</span>
            </address>
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
            <b>Invoice #<span class="print_span print_env_inv_no">{{ $allinvdata->id or ''}}</span></b><br>
            <br>
            <b>Payment Due:</b> <span class="print_span print_env_inv_payment_date">{{ $allinvdata->paymentdue or ''}}</span><br>
            
          </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Qty</th>
                  <th>Product/Service</th>
                  <th>Description</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody class="print_span print_env_inv_items_loop">
				  
               <?php
             if(isset($allinvdata->items)){
                $invoice_items = json_decode($allinvdata->items) ;
                 ?>
               @foreach ($invoice_items as $invoice_item)
               
                 <tr>
                  <th>{{ $invoice_item -> qty }}</th>
                  <th>{{ $invoice_item -> name }}</th>
                  <th>{{ $invoice_item -> desc }}</th>
                  <th>{{ $invoice_item -> qty }}</th>
                  <th>{{ $invoice_item -> amount }}</th>
                </tr>
               
               @endforeach
               <?php
			 }  
			   ?>
              </tbody>
            </table>
          </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
          <!-- accepted payments column -->
          <div class="col-xs-6">
            <p class="lead">Payment Methods:</p>
            <img src="{{ asset("img/credit/visa.png") }}" alt="Visa">
            <img src="{{ asset("img/credit/mastercard.png") }}" alt="Mastercard">
            <img src="{{ asset("img/credit/american-express.png") }}" alt="American Express">
            <img src="{{ asset("img/credit/paypal2.png") }}" alt="Paypal">
            <p class="text-muted well well-sm no-shadow print_span print_env_inv_info" style="margin-top: 10px;">
            </p>
          </div><!-- /.col -->
          <div class="col-xs-6">
            <p class="lead">Amount Due : <span class="print_span print_env_inv_payment_date">{{ $dueDate or '' }}</span> </p>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%">Subtotal:</th>
                  <td class="print_span print_env_inv_subtotal">{{ $allinvdata->subtotal or ''}}</td>
                </tr>
                <tr>
                  <th>Tax (16%)</th>
                  <td class="print_span print_env_inv_tax">{{ $allinvdata -> tax or '' }}</td>
                </tr>
                
                <tr>
                  <th>Total:</th>
                  <td class="print_span print_env_inv_total">{{ $allinvdata->total or ''}}</td>
                </tr>
              </table>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- ./wrapper -->

</div>
