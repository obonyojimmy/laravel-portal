            <form action="{!! route('api.users.update' , ['id' => $id]) !!}" method="post" class="form-horizontal">
	              
                      <div class="form-group ">
                        <label for="inputFName" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputFName" placeholder="Names" value="{{ $singleuser->first_name or '' }}" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputLName" class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputLName" placeholder="Names" value="{{ $singleuser->last_name  or '' }}" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
							
							
                          <input type="text" name="username" class="form-control"  id="inputUsername" placeholder="Username" value="{{ $singleuser->username or ' ' }}" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
							
							
                          <input type="password" name="password"  class="form-control"  id="inputPassword" placeholder="Password" value="" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
							
							
                          <input type="email" name="email"  class="form-control"  id="inputEmail" placeholder="Email" value="{{ $singleuser->email or ' ' }}" />
                          
                        </div>
                      </div>
                     <div class="form-group ">
                        <label for="inputIdnumber" class="col-sm-2 control-label">Id Number</label>
                        <div class="col-sm-10">
							
							
                          <input type="text" name="idnumber"  class="form-control"  id="inputIdnumber" placeholder="ID Number" value="{{ $singleuser->idnumber or ' ' }}" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                        <div class="col-sm-10">
							
							
                          <input type="tel"  name="phone" class="form-control"  id="inputPhone" placeholder="Phone" value="{{ $singleuser->phone or ' ' }}" />
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputInfo" class="col-sm-2 control-label">Brief Info</label>
                        <div class="col-sm-10">
							
							 <textarea name="info"  class="form-control" id="inputInfo" placeholder="Info">{{ $singleuser->info or ' ' }}</textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                    <label for="inputRoles" class="col-sm-2 control-label">User Roles</label>
                    <div class="col-sm-10">
                    <select id="inputRoles"  multiple="multiple"  class="form-control select2" style="width: 100%;">
						@foreach ($users_roles as $user_role)
						    <option value="{{ $user_role->id }}" >{{ $user_role->slug }}</option>
						@endforeach	
                   </select>
                    </div>
                  </div><!-- /.form-group -->
                  
                     
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" id="updateUser" class="btn btn-danger">Update User</button>
                        </div>
                      </div>
                    </form>
                 
                  

