            <form action="{!! route('api.users.store') !!}" method="post" class="form-horizontal">
	              
                      <div class="form-group ">
                        <label for="inputName" class="col-sm-2 control-label">First Names</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputFName" placeholder="Names" value="" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputName" class="col-sm-2 control-label">Last Names</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputLName" placeholder="Names" value="" />
                          
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
							
							
                          <input type="text" name="inputEmail" class="form-control"  id="inputEmail" placeholder="Email" value="" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
							
							
                          <input type="text" name="username" class="form-control"  id="inputUsername" placeholder="Username" value="" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
							
							
                          <input type="text" name="password"  class="form-control"  id="inputPassword" placeholder="Password" value="" />
                          
                        </div>
                      </div>
                     
                     <div class="form-group ">
                        <label for="inputIdnumber" class="col-sm-2 control-label">Id Number</label>
                        <div class="col-sm-10">
							
							
                          <input type="text" name="idnumber"  class="form-control"  id="inputIdnumber" placeholder="ID Number" value="" />
                          
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                        <div class="col-sm-10">
							
							
                          <input type="tel"  name="phone" class="form-control"  id="inputPhone" placeholder="Phone" value="" />
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputInfo" class="col-sm-2 control-label">Brief Info</label>
                        <div class="col-sm-10">
							
							 <textarea name="info"  class="form-control" id="inputInfo" placeholder="Info"></textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                    <label for="inputUserRole" class="col-sm-2 control-label">User Role</label>
                    <div class="col-sm-10">
                    <select id="inputUserRole" class="form-control select2" style="width: 100%;">
						@foreach ($user_roles as $user_role)
						    <option value="{{ $user_role->name }}" >{{ $user_role->slug }}</option>
							
						@endforeach	
                   </select>
                    </div>
                  </div><!-- /.form-group -->
                  
                     
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" id="createNewuser" class="btn btn-danger">Create User</button>
                        </div>
                      </div>
                    </form>
                 
                  

