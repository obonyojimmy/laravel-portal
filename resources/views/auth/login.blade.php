@extends('layouts.app')

@section('login')

<div class="login-box">
      <div class="login-logo">
       <a href="{!!  url('/') !!}"><b> Portal Example </b> </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="{!! route('api.users.login') !!}" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="inputUsername" placeholder="Username">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" id="LoginUser" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

        
        <a href="#">I forgot my password</a><br>
        

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->


@endsection

@push('scripts')
    <script src="{{ asset ('plugins/iCheck/icheck.min.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
       LaravelApiroute["loginuser"] = " {!! route('api.users.login') !!}";
    </script>
@endpush

@push('css')
  <link href="{{ asset("plugins/iCheck/square/blue.css")}}" rel="stylesheet" type="text/css" />
@endpush
