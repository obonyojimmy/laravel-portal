

@extends('layouts.app')


@section('usertype', 'Jimmy' )

@section('content')
<!-- Main content -->
       

          <div class="row">
           
           <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Chart Data</a></li>
                  
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Environment Records (30 days)</h3>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="envrecordsLineChart" style="height:45vh;"></canvas>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
         
            
          </div><!-- /.row -->

      
      

@endsection


@push('scripts')
   <!-- ChartJS 1.0.1 -->
    <script src="{{ asset ('plugins/chartjs/Chart.min.js') }}"></script>
  
     <!-- FastClick -->
    <script src="{{ asset ('plugins/fastclick/fastclick.min.js') }}"></script>
    
    <script type="text/javascript">
      $(function () {
		    
     
        
       
        
      });
      LaravelApiroute["envrecordschart"] = " {!! route('api.environmental.chartdata') !!}";
    
     
    </script>
    <script src="{{ asset ('js/chartenvrecords.js') }}"></script>
@endpush






