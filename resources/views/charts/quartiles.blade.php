<?php 
use Carbon\Carbon ;
$now = Carbon::now() ;

$Dfromdate = $now->startOfMonth()->format('m/d/Y') ;
$Dtodate = $now->endOfMonth()->format('m/d/Y') ;
//$Dtodate = $EndMoth->addDay()->format('d/m/Y');


?>


@extends('layouts.app')


@section('content')
<!-- Main content -->
       

          <div class="row">
           
           <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">All Time Chart Data</a></li>
                  <li class=""><a href="#timepiechart" data-toggle="tab">Date Range Chart Data</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">All Time Quartiles General Comparison</h3>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="quartilesPieChart" style="height:50vh;"></canvas>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                  <div class=" tab-pane" id="timepiechart">
                   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Date Range Quartiles  Comparison</h3>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
					<div class="row">
							     <div class="col-xs-4">
									 <!-- Date range -->
								  <div class="form-group">
									<label for="inputDateMan">From:</label>
									<div class="input-group">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="FromDate" value="{{ $Dfromdate or '' }}">
									</div><!-- /.input group -->
								  </div><!-- /.form group -->

									</div>
								 <div class="col-xs-4">
								    <!-- Date range -->
								  <div class="form-group">
									<label for="inputDateExpiry">To:</label>
									<div class="input-group">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="ToDate" value="{{$Dtodate or '' }}">
									</div><!-- /.input group -->
								  </div><!-- /.form group -->
								 </div>
							   </div>
				             
                  <div id="chartpie_daterange">
                    <canvas id="datequartilespiechart" style="height:50vh"></canvas>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
         
            
          </div><!-- /.row -->

      
      

@endsection


@push('scripts')

  <script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
<script src=" {{ asset ('plugins/daterangepicker/daterangepicker.js') }}"></script>
   <!-- ChartJS 2.0.1-->
    <script src="{{ asset ('js/Chart.js') }}"></script>
  
     <!-- FastClick -->
    <script src="{{ asset ('plugins/fastclick/fastclick.min.js') }}"></script>
    
    <script type="text/javascript">
      $(function () {
		   $('#FromDate').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
		
		 $('#ToDate').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});  
     
        
       
        
      });
      LaravelApiroute["daterangequartilespiechart"] = " {!! route('api.cars.daterangequartilespiechart') !!}";
      LaravelApiroute["quartilespiechart"] = " {!! route('api.cars.quartilespiechart') !!}";
     
    </script>
    <script src="{{ asset ('js/quartilespiechart.js') }}"></script>
    <script src="{{ asset ('js/daterangequartilespiechart.js') }}"></script>
@endpush

@push('css')
 
 <link rel="stylesheet" href="{{ asset ('plugins/daterangepicker/daterangepicker-bs3.css') }}">
 
@endpush






