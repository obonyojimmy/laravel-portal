<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Car Record</h3>
              <div class="box-tools pull-right">
                <button class="btn " id="editFullcar"><i class="fa fa-floppy-o"></i> Save</button>
                <button class="btn" id="printeditFullcar"><i class="fa fa-print"></i> Print</button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
				  <form action="{!! route('api.cars.store') !!}" method="post" class="">
                <div class="col-md-6">
					<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Document details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputChasis" class="col-sm-2 control-label">Chasis</label>
												<div id="bloodhound" class="col-sm-10">
													<input disabled class="typeahead form-control" id="inputChassis" type="text" placeholder="Enter Chasis Number" value="{{ $allcardata->chassisno }}">	
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputShiping" class="col-sm-2 control-label"> Shiping</label>
												<div class="col-sm-10">
													
												  <input  type="text" class="form-control " id="inputShiping" placeholder="Enter Shiping Vessel"  value="{{ $allcardata->ship->name }}">
												  
												</div>
											  </div>
											 
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                   <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Importers /Exporters details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputImporters" class="col-sm-2 control-label">Names</label>
												<div class="col-sm-10">
													
												  <input  type="text" name="name" class="form-control "  id="inputImporters" placeholder="Importers /Exporters Names"  value="{{ $allcardata->importer->name }}" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputAddress" class="col-sm-2 control-label"> Address</label>
												<div class="col-sm-10">
													
												  <input  type="text" name="name" class="form-control "  id="inputAddress" placeholder="Importers Address" value="{{ $allcardata->importer->address }}" />
												  
												</div>
											  </div>
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

 <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Radiation Survey Report in Microsievert Per Hour( <span class="">µ</span>sv/hr )</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputBackground" class="col-sm-2 control-label">Background</label>
												<div class="col-sm-10">
													
												  <input type="text" name="name" class="form-control"  id="inputBackground" placeholder="eg.0.04, 0.08, 0.06" value="<?php $backg =implode(" ",$allcardata->carinspection->background); echo $backg ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputExterior" class="col-sm-2 control-label">Exterior</label>
												<div class="col-sm-10">
													
												  <input type="text" name="name" class="form-control"  id="inputExterior" placeholder="eg.0.04, 0.08, 0.06" value="<?php $extr =implode(" ",$allcardata->carinspection->exterior); echo $extr ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputEngine" class="col-sm-2 control-label">Engine</label>
												<div class="col-sm-10">
													
												  <input type="text" name="name" class="form-control"  id="inputEngine" placeholder="eg.0.04, 0.08, 0.06" value="<?php $eng =implode(" ",$allcardata->carinspection->engine); echo $eng ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputInterior" class="col-sm-2 control-label">Interior</label>
												<div class="col-sm-10">
													
												  <input type="text" name="name" class="form-control"  id="inputInterior" placeholder="eg.0.04, 0.08, 0.06" value="<?php $intr =implode(" ",$allcardata->carinspection->interior); echo $intr ; ?>" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputExtras" class="col-sm-2 control-label">Extras</label>
												<div class="col-sm-10">
													
												  <input type="text" name="name" class="form-control"  id="inputExtras" placeholder="eg.0.04, 0.08, 0.06" value="<?php $extras =implode(" ",$allcardata->carinspection->extras); echo $extras ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group">
												<label for="inputCarInspectionInfo" class="col-sm-2 control-label">Other Info</label>
												<div class="col-sm-10">
													
													 <textarea name="info"  class="form-control" id="inputCarInspectionInfo" placeholder="Any other related  information on the car on inspection">{{ $allcardata->carinspection->info }}</textarea>
												  
												</div>
											  </div> 
                    
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                <div class="col-md-6">
                      <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Particulars of Consignments as declared by importer/Exporter</h3>
                </div>
                <div class="box-body ">
                     
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCarMake">Car make</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input  type="text" class="form-control " id="inputCarMake" placeholder="eg.Nissan or Toyota" value="{{ $allcardata->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputColour">Colour</label>
									  <input  type="text" class="form-control " id="inputColour" placeholder="Eg Red"  value="{{ $allcardata->colour }}" />
								    </div>
								 </div>
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputEngineNumber">Engine Number</label>
									  <input  type="text" class="form-control " id="inputEngineNumber" placeholder="Engine Number"  value="{{ $allcardata->engineno }}" />
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCustoms">Customs</label>
									 <input  type="text" name="name" class="form-control "  id="inputCustoms" placeholder="Customs Entry No"  value="{{ $allcardata->customsno }}" />
												  
								 </div>
							  </div>
							   </div>
				             <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPrevRegCountry">Previous Reg.Country</label>
									   <select  class="form-control  select2" id="inputPrevRegCountry" style="width: 100%;">
										
										   <option value="{{ $allcardata->prevcountry_id }}">{{ $allcardata->prevcountry->name }}</option>
												


										</select>
									  
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPreviousReg">Previous Reg</label>
									  <input  type="text" class="form-control " id="inputPreviousReg" placeholder="Enter Previous Registration"  value="{{ $allcardata->previousreg }}" />
								    </div>
								 </div>
								 
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputFuel">Fuel</label>
									  <select  class="form-control select2 " id="inputFuel" style="width: 100%;">
										  
										  <option value="{{ $allcardata->fuel }}">{{ $allcardata->fuel }}</option>
										 
										</select>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputManYear">Manufacture  Year</label>
									  <select  class="form-control   select2" id="inputManYear" style="width: 100%;">
										 
												
												<option value="{{ $allcardata->year }}">{{ $allcardata->year }}</option>
										
										  
										 
										 
										</select>
									  
								    </div>
								 </div>
								 
							  </div>
				               <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputClearingAgent">Clearing Agent</label>
									  <input  type="text" class="form-control " id="inputClearingAgent" placeholder="Enter Clearing Agent"  value="{{ $allcardata->clearingagent->name }}">
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputDestination">Destination</label>
									  <select  class="form-control select2 " id="inputDestination" style="width: 100%;">
										  
										
										   <option value="{{ $allcardata->destcountry_id }}">{{ $allcardata->destcountry->name }}</option>
									
										</select>
									  
								    </div>
								 </div>
								  
							</div>
				         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>

<script type="text/javascript">
       $(function () {
		   
        //Initialize Select2 Elements
         $(".select2").select2();
    
var chassisengine = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  
 remote : {
	  url: " {!! route('api.cars.chasisno') !!}" ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
		
	  }
	 
 }
});

//console.log(chassisengine);

$('#bloodhound .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'chassisengine',
  source: chassisengine
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
    $(this).attr('disabled', true).css("background-color", "");
   // $(this).attr('disabled', true);
     $.ajax({
              url: "{!! route('api.cars.singlecar') !!}",
              type: 'get',
              dataType: 'json',
              data:{ chasisno : datum },
            })
            .done(function(result) {
			  
              console.log(result);
            })
            .fail(function(error) {
             
              //alert(error);
              console.log(error);
            });
            
   
    //console.log(datum);
});
       
       
       });
       
    
    
    
    </script>
    
    <script type="text/javascript">
     LaravelApiroute["editfullcarsave"] = " {!! route('api.cars.update' , ['id' => $allcardata -> id ]) !!}";
     LaravelApiroute["fullcarsprintcount"] = " {!! route('api.cars.updateprint') !!}"; 
     
    
    </script>
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
 
@endpush

@include('print.carrecord')


