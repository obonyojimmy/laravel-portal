

<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;

$counter = 1 ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $title }} </h3>
              <div class="box-tools pull-right">
                <a class="btn " href="{!! route('admin.users.newsinglerole') !!}" id=""><i class="fa fa-floppy-o"></i> New Role</button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                 <table id="" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Role Name</th>
                        
                        <th>Date</th>
                        <th></th>
                      </tr>
                    </thead>
                     <tbody>
						 @foreach ($usersroles as $usersrole)
						 <tr>
                        <td>{{ $counter++ }}</td>
                        <td>{{ $usersrole -> slug }}</td>
                      
                        <td>{{ $usersrole -> created_at }}</td>
                        <td>
                        <div class="btn-group">
										  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Action <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu">
											
											<li><a href="{!! route('admin.users.singlerole' ,['id' => $usersrole->id ]) !!}">Manage</a></li>
											<li><a href="">Delete</a></li>
										  </ul>
						</div>
                        </td>
                      </tr>
                      @endforeach
				     </tbody>
                    
                  </table>
                
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection












