<?php 
use Carbon\Carbon ;
$now = Carbon::now() ;

if($fromdate && $todate){
	
	$FromDate = $fromdate ;
	$ToDate = $todate ;
	
	$fdate = Carbon::parse($fromdate) ;
    $tdate= Carbon::parse($todate) ;
    
    $Dfromdate = $fdate->format('m/d/Y') ;
    $Dtodate = $tdate ->format('m/d/Y') ;
    
    $daterangestringtext = $Dfromdate .' - ' .$Dtodate ;
}else{
	
  
    $daterangestringtext = $now -> format('F') ;
	$Sfromdate = $now->startOfMonth()->format('m/d/Y') ;
	$Etodate = $now->endOfMonth()->format('m/d/Y') ;	
	
	$FromDate = $now->startOfMonth()->toDateString()  ;
	$ToDate = $now->endOfMonth()->toDateString()  ;
}


?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">All Car Records for : <strong class="monthdaterange">{{ $daterangestringtext }}</strong> </h3>
              <div class="box-tools pull-right">
                
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
				     <div class="row">
					  <div class="col-md-6 col-md-offset-6">
						  <div class="row">
							     <div class="col-md-6">
									 <!-- Date range -->
								  <div class="form-group">
									<label for="inputDateMan">From:</label>
									<div class="input-group">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="FromDate" value="{{ $Dfromdate or $Sfromdate }}">
									</div><!-- /.input group -->
								  </div><!-- /.form group -->

									</div>
								 <div class="col-md-6">
								    <!-- Date range -->
								  <div class="form-group">
									<label for="inputDateExpiry">To:</label>
									<div class="input-group">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="ToDate" value="{{$Dtodate or $Etodate }}">
									</div><!-- /.input group -->
								  </div><!-- /.form group -->
								 </div>
				          </div>
				      </div>
				  </div>
				    
                 <table id="car_records" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Chassisno No</th>
                        <th>Importer Name</th>
                        <th>Customs No</th>
                        <th>Car Make</th>
                        <th>System user</th>
                        <th>Created On</th>
                         <th></th>
                      </tr>
                    </thead>
                     <tbody>
				     </tbody>
                    
                  </table>
                
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src=" {{ asset ('js/jquery.dataTables.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.buttons.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.colVis.min.js') }}"></script>
<script src=" {{ asset ('js/jszip.min.js') }}"></script>
<script src=" {{ asset ('js/pdfmake.min.js') }}"></script>
<script src=" {{ asset ('js/vfs_fonts.js') }}"></script>
<script src=" {{ asset ('js/buttons.html5.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.print.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.bootstrap.min.js') }}"></script>

<script src=" {{ asset ('js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
<script src=" {{ asset ('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script>
      $(function () {
        $('#FromDate').daterangepicker({
			 
			singleDatePicker: true,
			showDropdowns: true
		});
		
		 $('#ToDate').daterangepicker({
			
			singleDatePicker: true,
			showDropdowns: true
		});  
       var t =  $('#car_records').DataTable({
				processing: true,
				serverSide: true,
				dom: 'Bfrtip',
				 buttons: [
					
					{
						extend: 'excelHtml5',
						exportOptions: {
							columns: [ 0, 1, 2, 3, 4, 5 , 6 ]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
							columns: [ 0, 1, 2, 3, 4, 5, 6]
						}
					},
					{
						extend: 'print',
						exportOptions: {
							columns: [ 0, 1, 2,3 ,4 , 5, 6]
						}
					}
					
					
				],
				//~ buttons: [
					//~ 'print' , 'excel', 'pdf'
				//~ ],
				 "order": [[ 1, 'asc' ]],
			    ajax: { 
					url : '{!! route('api.cars.datatable' , ['fromdate'=>$FromDate , 'todate' => $ToDate ]) !!}' ,
					data: function (d) {
						 var td = $('#ToDate').val() ;
						 var fd = $('#FromDate').val() ;
						 td = moment(td).format('YYYY-MM-DD');
						 fd = moment(fd).format('YYYY-MM-DD');
						d.fromdate = fd;
						d.todate = td;
					}
			    
			    } ,
			
				
				  columns: [
					 {data: 'rownum', name: 'rownum',orderable: false, searchable: false},
					{ data: 'chassisno', name: 'cars.chassisno' },
					{ data: 'importername', name: 'importers.name' },
					{ data: 'customsno', name: 'cars.customsno' },
					{ data: 'name', name: 'cars.name' },
					{ data: 'username', name: 'users.name' },
					{ data: 'created_at', name: 'cars.created_at' },
					{ data: 'action', name: 'cars.id',orderable: false, searchable: false },
					
				]
			});
			
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				} );
			} ).draw();
			
			 $("#ToDate").change( function(e) {
				$('.monthdaterange').html() ;
				moment.locale();
				var td = $('#ToDate').val() ;
				var fd = $('#FromDate').val() ;
				var vtd = moment(td).format('lll');
			    var vfd = moment(fd).format('lll');
				$('.monthdaterange').text(vfd +' - '+ vtd) ;
				 t.draw();
				 e.preventDefault();
			 });
			$("#FromDate").change( function(e) {
				$('.monthdaterange').html() ;
				moment.locale();
				var td = $('#ToDate').val() ;
				var fd = $('#FromDate').val() ;
				var vtd = moment(td).format('lll');
			    var vfd = moment(fd).format('lll');
				$('.monthdaterange').text(vfd +' - '+ vtd) ;
				 t.draw();
				 e.preventDefault();
			 });
			
      });
    </script>
    
  
    
@endpush

@push('css')

 <link rel="stylesheet" href="{{ asset ('css/dataTables.bootstrap.min.css') }}">
 
 <link rel="stylesheet" href="{{ asset ('css/buttons.bootstrap.min.css') }}">
 
 <link rel="stylesheet" href="{{ asset ('plugins/daterangepicker/daterangepicker-bs3.css') }}">
 
@endpush




