<?php 
use Carbon\Carbon ;
$now = Carbon::now() ;

$Dfromdate = $now->startOfMonth()->toFormattedDateString() ;
$Dtodate = $now->endOfMonth()->toFormattedDateString() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')

@if($user->hasAccess(['admin.dashboard.index']))

 <div class="row">
           
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-android-car"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Car Records</span>
                  <span class="info-box-number">{{ $carCount }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-android-boat"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Env Records</span>
                  <span class="info-box-number">{{ $envCount }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Invoices</span>
                  <span class="info-box-number">{{ $invCount }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Users</span>
                  <span class="info-box-number">{{ $userCount }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          
  
</div><!-- /.row -->





<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Monthly Recap Report</h3>
                  <div class="box-tools pull-right">
                   
                     <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-center">
                        <strong>Car Records: {{ $Dfromdate or '' }} - {{ $Dtodate or '' }}</strong>
                      </p>
                      <div class="chart">
                        <!-- Carrecords Chart Canvas -->
                        <canvas id="carrecordsLineChart" style="height: 180px;"></canvas>
                      </div><!-- /.chart-responsive -->
                    </div><!-- /.col -->
                   </div><!-- /.row -->
                </div><!-- ./box-body -->
                 </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

@endif
<div class="row">
            <div class="col-md-12">
				<!-- TABLE: LATEST ORDERS -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Car Records</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="row">
            <div class="col-md-12">
                 <table id="car_environmental_records" class="table table-bordered table-striped">
                    <thead>
                       <tr>
                        <th></th>
                        <th>Chassisno No</th>
                        <th>Importer Name</th>
                        <th>Customs No</th>
                        <th>Car Make</th>
                        <th>System user</th>
                        <th>Created On</th>
                         <th></th>
                      </tr>
                    </thead>
                     <tbody>
				     </tbody>
                    
                  </table>
                  </div>
                </div>
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
           
            </div><!-- /.col -->
</div><!-- /.row -->
@endsection


@push('scripts')



<script src=" {{ asset ('js/jquery.dataTables.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.buttons.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.colVis.min.js') }}"></script>
<script src=" {{ asset ('js/jszip.min.js') }}"></script>
<script src=" {{ asset ('js/pdfmake.min.js') }}"></script>
<script src=" {{ asset ('js/vfs_fonts.js') }}"></script>
<script src=" {{ asset ('js/buttons.html5.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.print.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.bootstrap.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.responsive.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset ('plugins/chartjs/Chart.min.js') }}"></script>
<script src="{{ asset ('js/dashchartcarrecords.js') }}"></script>


<script type="text/javascript">
      $(function () {
       
       var t =    $('#car_environmental_records').DataTable({
				processing: true,
				responsive: true,
				 "columnDefs": [ {
					"searchable": false,
					"orderable": false,
					"targets": 0
				} ],
				//~ "autoWidth": false,
				 //~ "columnDefs": [
					//~ { "width": "5%", "targets": 0 },
					//~ { "width": "20%", "targets": 1 },
					//~ { "width": "15%", "targets": 2 },
					//~ { "width": "15%", "targets": 3 },
					//~ { "width": "15%", "targets": 4 },
					//~ { "width": "5%", "targets": 5 },
					//~ { "width": "15%", "targets": 6 },
					//~ { "width": "10%", "targets": 7 }
				  //~ ],
				serverSide: true,
				dom: 'Bfrtip',
				 buttons: [
					
					{
						extend: 'excelHtml5',
						exportOptions: {
							columns: [  0,1, 2,3,4, 5 , 6 ]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
							columns: [ 0, 1, 2,3,4, 5, 6 ]
						}
					},
					{
						extend: 'print',
						exportOptions: {
							columns: [ 0, 1, 2,3,4, 5, 6]
						}
					}
					
					
				],
				//~ buttons: [
					//~ 'print' , 'excel', 'pdf'
				//~ ],
				//~ "fnCreatedRow": function (row, data, index) {
					//~ $('td', row).eq(0).html(index + 1);
				//~ },
				 "order": [[ 1, 'asc' ]],
				"pageLength": 10,
			    ajax: '{!! route('admin.dashboard.datatable') !!}',
			    
				columns: [
					 {data: 'rownum', name: 'rownum' ,orderable: false, searchable: false},
					{ data: 'chassisno', name: 'cars.chassisno' },
					{ data: 'importername', name: 'importers.name' },
					{ data: 'customsno', name: 'cars.customsno' },
					{ data: 'name', name: 'cars.name' },
					{ data: 'first_name', name: 'users.first_name' },
					{ data: 'created_at', name: 'cars.created_at' },
					{ data: 'action', name: 'cars.id' ,orderable: false, searchable: false},
					
				]
			});
			
			 t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				} );
			} ).draw();
			
      });
   
    
    </script>
    
    <script type="text/javascript">
     LaravelApiroute["dashcarrecordschart"] = " {!! route('admin.dashboard.chartdata') !!}";
     
    </script>
    
@endpush

@push('css')
 
 <link rel="stylesheet" href="{{ asset ('css/dataTables.bootstrap.min.css') }}">
 
 <link rel="stylesheet" href="{{ asset ('css/buttons.bootstrap.min.css') }}">
@endpush





