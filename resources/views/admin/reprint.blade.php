
<?php 
use Carbon\Carbon ;
$now = Carbon::now() ;

$Dfromdate = $now->startOfMonth()->format('m/d/Y') ;
$Dtodate = $now->endOfMonth()->format('m/d/Y') ;
//$Dtodate = $EndMoth->addDay()->format('d/m/Y');


?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Users  Reprints for Chassis Number : <strong> {{ $chassisno }} </strong> </h3>
              <div class="box-tools pull-right">
                
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
				     
                 <table id="car_reprint_record" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Names</th>
                        <th>Reprints Count</th>
                       
                        <th></th>
                      </tr>
                    </thead>
                     <tbody>
				     </tbody>
                    
                  </table>
                
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src=" {{ asset ('js/jquery.dataTables.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.buttons.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.colVis.min.js') }}"></script>
<script src=" {{ asset ('js/jszip.min.js') }}"></script>
<script src=" {{ asset ('js/pdfmake.min.js') }}"></script>
<script src=" {{ asset ('js/vfs_fonts.js') }}"></script>
<script src=" {{ asset ('js/buttons.html5.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.print.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.bootstrap.min.js') }}"></script>

<script src=" {{ asset ('js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
<script src=" {{ asset ('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script>
      $(function () {
        //~ $('#FromDate').daterangepicker({
			//~ singleDatePicker: true,
			//~ showDropdowns: true
		//~ });
		
		 //~ $('#ToDate').daterangepicker({
			//~ singleDatePicker: true,
			//~ showDropdowns: true
		//~ });  
       var t =  $('#car_reprint_record').DataTable({
				processing: true,
				serverSide: true,
				dom: 'Bfrtip',
				 buttons: [
					
					{
						extend: 'excelHtml5',
						exportOptions: {
							columns: [ 0, 1, 2, 3, 4, 5 , 6 ]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
							columns: [ 0, 1, 2, 3, 4, 5, 6]
						}
					},
					{
						extend: 'print',
						exportOptions: {
							columns: [ 0, 1, 2,3 ,4 , 5, 6]
						}
					}
					
					
				],
				//~ buttons: [
					//~ 'print' , 'excel', 'pdf'
				//~ ],
				 "order": [[ 1, 'asc' ]],
			    ajax: "{!! route('api.cars.singlereprintdatatable' , ['id' => $carId ]) !!}" ,
				
				  columns: [
					 {data: 'rownum', name: 'rownum',orderable: false, searchable: false},
					
					{ data: 'first_name', name: 'users.first_name' },
					{data: 'count', name: 'count', searchable: false},
					{ data: 'action', name: 'prints.car_id',orderable: false, searchable: false },
					
				]
			});
			
			t.on( 'order.dt search.dt', function () {
				t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = i+1;
				} );
			} ).draw();
			
			 
			
      });
    </script>
    
  
    
@endpush

@push('css')

 <link rel="stylesheet" href="{{ asset ('css/dataTables.bootstrap.min.css') }}">
 
 <link rel="stylesheet" href="{{ asset ('css/buttons.bootstrap.min.css') }}">
 
@endpush





