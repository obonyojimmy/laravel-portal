

<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $title }} </h3>
              <div class="box-tools pull-right">
                <button class="btn " id="saveFullInvoice"><i class="fa fa-floppy-o"></i> Save</button>
                <button class="btn" id="printFullInvoice"><i class="fa fa-print"></i> Print</button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
				  <form action="{!! route('api.invoice.store') !!}" method="post" class="">
					  <div class="col-md-6">
						   <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Invoicee Details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputInvNames" class="col-sm-3 control-label">Names</label>
												<div class="col-sm-9">
													
												  <input  type="text" name="name" class="form-control "  id="inputInvNames" placeholder="Invoicee Names" value="" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputInvAddress" class="col-sm-3 control-label"> Address</label>
												<div class="col-sm-9">
													
												  <input  type="text" name="name" class="form-control "  id="inputInvAddress" placeholder="Invoicee Address" value="" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputInvEmail" class="col-sm-3 control-label"> Email :</label>
												<div class="col-sm-9">
													<input  type="text" name="name" class="form-control "  id="inputInvEmail" placeholder="Invoicee Email" value="" />
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputInvPhone" class="col-sm-3 control-label"> Phone :</label>
												<div class="col-sm-9">
													<input  type="text" name="name" class="form-control "  id="inputInvPhone" placeholder="Invoicee Phone" value="" />
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputInvIDnumber" class="col-sm-3 control-label"> ID Number :</label>
												<div class="col-sm-9">
													<input  type="text" name="name" class="form-control "  id="inputInvIDnumber" placeholder="Invoicee Id Number/passport Number" value="" />
												
												  
												</div>
											  </div>
											  
											  <div class="form-group">
												  <label for="inputDatepayment" class="col-sm-3 control-label"> Payment Date:</label>
									<div class="col-sm-9">
									<div class="input-group ">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="inputDatepayment">
									  </div>
									</div><!-- /.input group -->
								  </div><!-- /.form group -->
								  
								  <div class="form-group ">
												<label for="inputInvInfo" class="col-sm-3 control-label"> Other Info :</label>
												<div class="col-sm-9">
													<textarea name="info"  class="form-control " id="inputInvInfo" placeholder="Any other related information to invoice"></textarea>
										
												  
												</div>
											  </div>
											 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                          
					  </div>
					  <div class="col-md-6">
						 <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Invoice Items Details</h3>
                </div>
                <div class="box-body ">
                     <div id="invoiceitems">
					    <div class="invoiceitems">
						 <div class="row singleInvItemId">
							     <div class="col-xs-5">
									 
								  <div class="form-group">
									<label for="inputSingleInvItem">Name</label>
									<input  type="text" name="name" class="form-control inputSingleInvItemClass" id="inputSingleInvItem"   placeholder="Item name" value="" />
												
								</div>

									</div>
								 <div class="col-xs-2">
								   
								  <div class="form-group">
									<label for="inputSingleInvQty">Qty</label>
									<input  type="text" name="name" class="form-control inputSingleInvQtyClass"  id="inputSingleInvQty" placeholder="Qty" value="" />
														
								  </div>
								 </div>
							      <div class="col-xs-5">
								    
								  <div class="form-group">
									<label for="inputSingleInvAmount">Amount:</label>
									<input  type="text" name="name" class="form-control inputSingleInvAmountClass"  id="inputSingleInvAmount" placeholder="Amount" value="" />
											
								  </div>
								 </div>
							   
						  </div>
				          <div class="row singleInvItemId">
							  <div class="col-xs-12">
								
								  <div class="form-group">
									<label for="inputSingleInvDesc">Description</label>
									<textarea name="info"  class="form-control inputSingleInvDescClass" id="inputSingleInvDesc" placeholder="Item Description "></textarea>
												  		
								</div>
								<div class="form-group">
									<ul class="list-inline">
										<li>
									<button class="btn inputSingleInvAdditembtn" id=""><i class="fa fa-floppy-o"></i> Add Item</button>
									   </li>
									   <li>
									<button class="btn inputSingleInvRemoveitembtn" id="singleInvItemId"><i class="fa fa-floppy-o"></i> Remove Item</button>
									   </li>
									   </ul>
                                </div>
 
									</div>
						  </div>  		  
					    </div>
					  </div>
					  
						 
					 		  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

					  </div>
					  
                  </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')


<script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
<script src=" {{ asset ('plugins/daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">
       $(function () {
		   
       
         
          $('#inputDatepayment').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
		
		 
     });
       
    
    
    
    </script>
    
    <script type="text/javascript">
     LaravelApiroute["invoicesave"] = " {!! route('api.invoice.store') !!}";
     
    
    </script>
    
@endpush

@push('css')

 <link rel="stylesheet" href="{{ asset ('plugins/daterangepicker/daterangepicker-bs3.css') }}">
 
@endpush

@include('print.invoiceprint')


