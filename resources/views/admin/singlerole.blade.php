


<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;

$counter = 1 ;
$counter1 = 1 ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $title }} </h3>
              <div class="box-tools pull-right">
                
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
				
				<div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Active Permisions</a></li>
                  
                  <li><a href="#settings" data-toggle="tab">All Permisions</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Active Permisions</h3>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
                  <div class="">
                      <table id="" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Permision Name</th>
                         <th>Status</th>
                      </tr>
                    </thead>
                     <tbody>
						 @foreach ($permisions as  $permision)
						 <tr>
                        <td>{{ $counter++ }}</td>
                        <td>{{ $permision['name'] }}</td>
                        
                        <td>
                        <input type="checkbox" @if ($permision['status'] == 1) checked value=1 @else value=0 @endif data-role="{{ $role }}" data-name="{{ $permision['permision'] }}" />
                        </td>
                       
                        
                      </tr>
                      @endforeach
				     </tbody>
                    
                  </table>
                
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                 <div class="tab-pane" id="settings">
					 <div class="box box-info">
                 <div class="box-header with-border">
                  <h3 class="box-title">All Permisions</h3>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
                  <div class="">
					 <table id="" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Permision Name</th>
                         <th>Status</th>
                      </tr>
                    </thead>
                     <tbody>
						 @foreach ($allpermisions as  $allpermision)
						 <tr>
                        <td>{{ $counter1++ }}</td>
                        <td>{{ $allpermision['name'] }}</td>
                        
                        <td>
                        <input type="checkbox" @if ($allpermision['status'] == 1) checked value=1 @else value=0 @endif data-role="{{ $role }}" data-name="{{ $allpermision['permision'] }}" />
                        </td>
                       
                        
                      </tr>
                      @endforeach
				     </tbody>
                    
                  </table>
                
                    
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                 
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
           
                 
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection

@push('scripts')

<script src="{{ asset ('js/bootstrap-checkbox.js') }}"></script>
<script type="text/javascript">
      $(function () {
		  $(':checkbox').checkboxpicker();
		  
		  $(':checkbox').checkboxpicker().change(function() {
			 
			 data={
			  'status' : $(this).val() ,
			  'name' : $(this).attr( "data-name" ),
			  'roleId' :  $(this).attr( "data-role" ),
			 };
			 console.log(data);
			  $.ajax({
					  url: '{!! route('api.users.roleaddpermission') !!}' ,
					  type: 'get',
					  data : data ,
					  dataType: 'json',
					})
					.done(function(r) {
					 
					  //alert(r);
					  console.log(r);
					  
					  
						
					})
					.fail(function(e) {
					 
					  //alert(error);
					  console.log(e);
					});
			
			});
		});
		
</script>
@endpush





