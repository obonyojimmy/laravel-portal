<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">New Inspected Car</h3>
              <div class="box-tools pull-right">
                <button class="btn " id="saveInspectedcar"><i class="fa fa-floppy-o"></i> Save</button>
                <button class="btn" id="PrintInspectedcar"><i class="fa fa-print"></i> Print</button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
				  <form action="{!! route('api.cars.store') !!}" method="post" class="">
                <div class="col-md-6">
					<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Document details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputChasis" class="col-sm-2 control-label">Chasis</label>
												<div id="bloodhound" class="col-sm-10">
													<input class="typeahead form-control" id="inputChassis" type="text" placeholder="Enter Chasis Number">	
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputShiping" class="col-sm-2 control-label"> Shiping</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" class="form-control inputHidden" id="inputShiping" placeholder="Enter Shiping Vessel">
												  
												</div>
											  </div>
											 
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                   <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Importers /Exporters details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputImporters" class="col-sm-2 control-label">Names</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="form-control inputHidden"  id="inputImporters" placeholder="Importers /Exporters Names" value="" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputAddress" class="col-sm-2 control-label"> Address</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="form-control inputHidden"  id="inputAddress" placeholder="Importers Address" value="" />
												  
												</div>
											  </div>
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                <div class="col-md-6">
                      <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Particulars of Consignments as declared by importer/Exporter</h3>
                </div>
                <div class="box-body ">
                     
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCarMake">Car make</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class="form-control inputHidden" id="inputCarMake" placeholder="eg.Nissan or Toyota">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputColour">Colour</label>
									  <input disabled type="text" class="form-control inputHidden" id="inputColour" placeholder="Eg Red">
								    </div>
								 </div>
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputEngineNumber">Engine Number</label>
									  <input disabled type="text" class="form-control inputHidden" id="inputEngineNumber" placeholder="Engine Number">
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCustoms">Customs</label>
									 <input disabled type="text" name="name" class="form-control inputHidden"  id="inputCustoms" placeholder="Customs Entry No" value="" />
												  
								 </div>
							  </div>
							   </div>
				             <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPrevRegCountry">Previous Reg.Country</label>
									   <select disabled class="form-control inputHidden select2" id="inputPrevRegCountry" style="width: 100%;">
										  @foreach ($prevcountry as $prev)
										   <option value="{{ $prev->id }}">{{ $prev->name }}</option>
												
										  @endforeach

										</select>
									  
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPreviousReg">Previous Reg</label>
									  <input disabled type="text" class="form-control inputHidden" id="inputPreviousReg" placeholder="Enter Previous Registration">
								    </div>
								 </div>
								 
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputFuel">Fuel</label>
									  <select disabled class="form-control select2 inputHidden" id="inputFuel" style="width: 100%;">
										  
										  <option value="Petrol">Petrol</option>
										  <option value="Diesel">Diesel</option>
										 
										</select>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputManYear">Manufacture  Year</label>
									  <select disabled class="form-control  inputHidden select2" id="inputManYear" style="width: 100%;">
										  @for ($i = 2005; $i < $now->year ; $i++)
												
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										  
										 
										 
										</select>
									  
								    </div>
								 </div>
								 
							  </div>
				               <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputClearingAgent">Clearing Agent</label>
									  <input disabled type="text" class="form-control inputHidden" id="inputClearingAgent" placeholder="Enter Clearing Agent">
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputDestination">Destination</label>
									  <select disabled class="form-control select2 inputHidden" id="inputDestination" style="width: 100%;">
										  
										  @foreach ($destcountry as $dest)
										   <option value="{{ $dest->id }}">{{ $dest->name }}</option>
												
										  @endforeach
										</select>
									  
								    </div>
								 </div>
								  
							</div>
				         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>

<script type="text/javascript">
       $(function () {
		   
        //Initialize Select2 Elements
         $(".select2").select2();
        

var chassisengine = new Bloodhound({
  initialize: false,
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
        url: '{!! route('api.cars.searchchasisno') !!}',
        cache: false 
    },
 remote : {
	  url: " {!! route('api.cars.chasisno') !!}" ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
		
	  }
	 
 }
});
chassisengine.clear();
chassisengine.initialize();
//~ //console.log(chassisengine);

$('#bloodhound .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'chassisengine',
  source: chassisengine
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
    $(this).attr('disabled', true).css("background-color", "");
   // $(this).attr('disabled', true);
     $.ajax({
              url: "{!! route('api.cars.singlecar') !!}",
              type: 'get',
              dataType: 'json',
              data:{ chasisno : datum },
            })
            .done(function(result) {
			  $( '.inputHidden' ).prop("disabled", false).css("background-color", "");
              console.log(result);
            })
            .fail(function(error) {
             
              //alert(error);
              console.log(error);
            });
            
   
    //console.log(datum);
});
      //~ $('#bloodhound .typeahead').typeahead({
        //~ source: function (query, process) {
            //~ return $.get('{!! route('api.cars.chasisno') !!}?q=' + query, function (data) {
                //~ return process(data.search_results);
            //~ });
        //~ }
    //~ });
    
     
       });
       
    
    
    
    </script>
    
    <script type="text/javascript">
     LaravelApiroute["inspecatedcarsave"] = " {!! route('api.cars.inspecatedcarsave') !!}";
     LaravelApiroute["inspectedcarsprintcount"] = " {!! route('api.cars.updateprint') !!}"; 
    
    </script>
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
 
@endpush

@include('print.carrecord')
