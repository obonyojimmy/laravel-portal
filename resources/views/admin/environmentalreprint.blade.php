
<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Reprint Enviromental Record </h3>
              <div class="box-tools pull-right">
                
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
				  <form action="" method="post" class="">
                <div class="col-md-6">
					
                   <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Importers /Exporters details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputImporters" class="col-sm-3 control-label">Names</label>
												<div class="col-sm-9">
													
												  <input disabled  type="text" name="name" class="form-control "  id="inputImporters" placeholder="" value="{{ $allenvdata -> importer -> name }}" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputAddress" class="col-sm-3 control-label"> Address</label>
												<div class="col-sm-9">
													
												  <input  disabled type="text" name="name" class="form-control "  id="inputAddress" placeholder="" value="{{ $allenvdata -> importer -> address }}" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputCustoms" class="col-sm-3 control-label"> Customs No :</label>
												<div class="col-sm-9">
													<input disabled type="text" name="name" class="form-control "  id="inputCustoms" placeholder="" value="{{ $allenvdata -> customsno }}" />
												
												  
												</div>
											  </div>
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

 <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Results of Analysis in Bq/Kg/Ltr</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputBackground" class="col-sm-3 control-label">Background</label>
												<div class="col-sm-9">
													
										             <input disabled type="text" name="name" class="form-control"  id="inputBackground" placeholder="eg.0.04, 0.08, 0.06" value="{{ $allenvdata -> environmentalinspection -> background }}" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputNetcounts" class="col-sm-3 control-label">Net counts</label>
												<div class="col-sm-9">
													
												  <input disabled type="text" name="name" class="form-control"  id="inputNetcounts" placeholder="eg.0.04, 0.08, 0.06" value="{{ $allenvdata -> environmentalinspection -> netcounts  }}" />
												  
												</div>
											  </div>
											   <div class="form-group">
												<label for="inpuEnviromnetradInfo" class="col-sm-3 control-label">Other Info</label>
												<div class="col-sm-9">
													
													 <textarea  disabled name="info"  class="form-control" id="inpuEnviromnetradInfo" placeholder="Any other related  information on the on analysis">{{ $allenvdata -> environmentalinspection -> info }}</textarea>
												  
												</div>
											  </div> 
                    
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                <div class="col-md-6">
                      <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Particulars of Consignments as declared by importer/Exporter</h3>
                </div>
                <div class="box-body ">
                     
							  <div class="row">
							     <div class="col-xs-12">
									<div class="form-group">
									 <label for="inputGoodsDesc">Goods Description</label>
									  <textarea disabled name="inputGoodsDesc"  class="form-control" id="inputGoodsDesc" placeholder="Goods Descriptions Information">{{ $allenvdata -> description }}</textarea>
										
								    </div>
								 </div>
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									 <!-- Date range -->
								  <div class="form-group">
									<label for="inputDateMan">Date of Manufacture:</label>
									<div class="input-group">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input disabled type="text" class="form-control pull-right" id="inputDateMan" value="{{ $allenvdata -> manufacture }}">
									</div><!-- /.input group -->
								  </div><!-- /.form group -->

									</div>
								 <div class="col-xs-6">
								    <!-- Date range -->
								  <div class="form-group">
									<label for="inputDateExpiry">Date of Expiry:</label>
									<div class="input-group">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input disabled type="text" class="form-control pull-right" id="inputDateExpiry" value="{{ $allenvdata -> expiry }}">
									</div><!-- /.input group -->
								  </div><!-- /.form group -->
								 </div>
							   </div>
				             <div class="row">
							     <div class="col-xs-6">
								    <div class="form-group">
									  <label for="inputBatchCodes">Products Batch Marks/Codes</label>
									  <input disabled type="text" class="form-control " id="inputBatchCodes" placeholder="EnterProducts Batch Marks/Codes" value="{{ $allenvdata -> batchcode }}">
								    </div>
								 
								  </div>
								 <div class="col-xs-6">
									 <div class="form-group">
									  <label for="inputBatchSize">Batch size</label>
									  <input disabled type="text" class="form-control " id="inputBatchSize" placeholder="EnterProducts Batch Size" value="{{ $allenvdata -> batchsize }}">
								    </div>
								 
									
									</div>
								 
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									  <div class="form-group">
									  <label for="inputDestination">Destination</label>
									  <select disabled class="form-control select2 " id="inputDestination" style="width: 100%;">
										  
										
										   <option value="{{ $allenvdata -> destcountry_id }}">{{ $allenvdata -> destcountry -> name }}</option>
												
										 
										</select>
									  
								    </div>
								 
									</div>
								 <div class="col-xs-6">
									 <div class="form-group">
									  <label for="inputClearingAgent">Clearing Agent</label>
									  <input disabled type="text" class="form-control " id="inputClearingAgent" placeholder="Enter Clearing Agent" value="{{ $allenvdata -> clearingagent -> name }}">
								    </div>
								 
									
							  </div>
				             </div>  
				               <div class="row">
							     <div class="col-xs-6">
									 <div class="form-group">
									  <label for="inputShiping">Ship</label>
									  <input disabled  type="text" class="form-control " id="inputShiping" placeholder="Enter Shipping Vessel" value="{{ $allenvdata -> ship -> name }}">
								    </div>
								 
								</div>
								 <div class="col-xs-6">
									 <div class="form-group">
									  <label for="inputPort">Port of Exit</label>
									  <input disabled type="text" class="form-control " id="inputPort" placeholder="Enter Port of Exit" value="{{ $allenvdata -> port  }}">
								    
								    </div>
								 
									</div>
								  
							</div>
				         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>


<script type="text/javascript">
       $(function () {
		   
        var customsno =  '{{ $allenvdata -> customsno  }}' ;
       $("#print_Barcode").JsBarcode(customsno,{displayValue:true, fontSize:20});
         
         window.print();
              
              dataprint = {
				  id : {{ $allenvdata -> id or ' ' }} ,
				  status : 1
				  };
			var url = '{!! route('api.environmental.updateprint') !!}' ;
				  
					   $.ajax({
					  url:  url ,
					  type: 'post',
					  dataType: 'json',
					  data: dataprint
					})
					.done(function(r) {
					  $.growl.notice({ message: "Environmental Record  sucessfully Reprinted !" });
					  //alert(error);
					  console.log(r);
					})
					.fail(function(e) {
					 
					  //alert(error);
					  console.log(e);
					});
           
         
          
     });
       
    
    
    
    </script>
    
    
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
 <link rel="stylesheet" href="{{ asset ('plugins/daterangepicker/daterangepicker-bs3.css') }}">
 
@endpush

@include('print.environmentalrecord')


