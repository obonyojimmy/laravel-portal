<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Full  Car Record</h3>
              <div class="box-tools pull-right">
                <button class="btn " id="saveFullcar"><i class="fa fa-floppy-o"></i> Save</button>
                <button class="btn" id="printFullcar"><i class="fa fa-print"></i> Print</button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
				  <form action="{!! route('api.cars.store') !!}" method="post" class="">
                <div class="col-md-6">
					<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Document details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputChasis" class="col-sm-2 control-label">Chasis</label>
												<div id="bloodhound" class="col-sm-10">
													<input class="typeahead form-control" id="inputChassis" type="text" placeholder="Enter Chasis Number">	
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputShiping" class="col-sm-2 control-label"> Shiping</label>
												<div class="col-sm-10">
													
												  <input  type="text" class="form-control " id="inputShiping" placeholder="Enter Shiping Vessel">
												  
												</div>
											  </div>
											 
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                   <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Importers /Exporters details</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputImporters" class="col-sm-2 control-label">Names</label>
												<div class="col-sm-10">
													
												  <input  type="text" name="name" class="form-control "  id="inputImporters" placeholder="Importers /Exporters Names" value="" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputAddress" class="col-sm-2 control-label"> Address</label>
												<div class="col-sm-10">
													
												  <input  type="text" name="name" class="form-control "  id="inputAddress" placeholder="Importers Address" value="" />
												  
												</div>
											  </div>
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

 <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Radiation Survey Report in Microsievert Per Hour( <span class="">µ</span>sv/hr )</h3>
                </div>
                <div class="box-body  form-horizontal">
                         <div class="form-group ">
												<label for="inputBackground" class="col-sm-2 control-label">Background</label>
												<div class="col-sm-10">
													
												 
												  <div class="row">
													  <div class="col-xs-7">
														  <input type="text" name="name" class="form-control" placeholder="eg.0.04, 0.08, 0.06"  id="inputBackground"  value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomBackground" style="width: 100%;">
										                      <option value="Quartile1">Quartiles Select </option>
															  <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															  
															 
															</select>
													  </div>
													  
													</div>
												
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputExterior" class="col-sm-2 control-label">Exterior</label>
												<div class="col-sm-10">
													<div class="row">
													  <div class="col-xs-7">
														 <input type="text" name="name" class="form-control"  id="inputExterior" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														 <select  class="form-control  " id="randomExterior" style="width: 100%;">
										                     <option value="Quartile1">Quartiles Select </option>
															  <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															  
															</select>
													  </div>
													  
													</div>
												
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputEngine" class="col-sm-2 control-label">Engine</label>
												<div class="col-sm-10">
													 <div class="row">
													  <div class="col-xs-7">
														  <input type="text" name="name" class="form-control"  id="inputEngine" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomEngine" style="width: 100%;">
															  <option value="Quartile1">Quartiles Select </option>
										                        <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															</select>
													  </div>
													  
													</div>
												
												 
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputInterior" class="col-sm-2 control-label">Interior</label>
												<div class="col-sm-10">
													
												  <div class="row">
													  <div class="col-xs-7">
														 <input type="text" name="name" class="form-control"  id="inputInterior" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomInterior" style="width: 100%;">
															  <option value="Quartile1">Quartiles Select </option>
										                      <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															 
															</select>
													  </div>
													  
													</div>
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputExtras" class="col-sm-2 control-label">Extras</label>
												<div class="col-sm-10">
													  <div class="row">
													  <div class="col-xs-7">
														 <input type="text" name="name" class="form-control"  id="inputExtras" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomExtras" style="width: 100%;">
															  <option value="Quartile1">Quartiles Select </option>
										                       <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															</select>
													  </div>
													  
													</div>
												
												 
												</div>
											  </div>
											   <div class="form-group">
												<label for="inputCarInspectionInfo" class="col-sm-2 control-label">Other Info</label>
												<div class="col-sm-10">
													
													 <textarea name="info"  class="form-control" id="inputCarInspectionInfo" placeholder="Any other related  information on the car on inspection"></textarea>
												  
												</div>
											  </div> 
                    
											  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                <div class="col-md-6">
                      <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Particulars of Consignments as declared by importer/Exporter</h3>
                </div>
                <div class="box-body ">
                     
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCarMake">Car make</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input  type="text" class="form-control " id="inputCarMake" placeholder="eg.Nissan or Toyota">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputColour">Colour</label>
									  <input  type="text" class="form-control " id="inputColour" placeholder="Eg Red">
								    </div>
								 </div>
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputEngineNumber">Engine Number</label>
									  <input  type="text" class="form-control " id="inputEngineNumber" placeholder="Engine Number">
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCustoms">Customs</label>
									 <input  type="text" name="name" class="form-control "  id="inputCustoms" placeholder="Customs Entry No" value="" />
												  
								 </div>
							  </div>
							   </div>
				             <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPrevRegCountry">Previous Reg.Country</label>
									   <select  class="form-control  select2" id="inputPrevRegCountry" style="width: 100%;">
										  @foreach ($prevcountry as $prev)
										   <option value="{{ $prev->id }}">{{ $prev->name }}</option>
												
										  @endforeach

										</select>
									  
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPreviousReg">Previous Reg</label>
									  <input  type="text" class="form-control " id="inputPreviousReg" placeholder="Enter Previous Registration">
								    </div>
								 </div>
								 
							  </div>
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputFuel">Fuel</label>
									  <select  class="form-control select2 " id="inputFuel" style="width: 100%;">
										  
										  <option value="Petrol">Petrol</option>
										  <option value="Diesel">Diesel</option>
										 
										</select>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputManYear">Manufacture  Year</label>
									  <select  class="form-control   select2" id="inputManYear" style="width: 100%;">
										  @for ($i = 2005; $i < $now->year ; $i++)
												
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										  
										 
										 
										</select>
									  
								    </div>
								 </div>
								 
							  </div>
				               <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputClearingAgent">Clearing Agent</label>
									  <input  type="text" class="form-control " id="inputClearingAgent" placeholder="Enter Clearing Agent">
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputDestination">Destination</label>
									  <select  class="form-control select2 " id="inputDestination" style="width: 100%;">
										  
										  @foreach ($destcountry as $dest)
										   <option value="{{ $dest->id }}">{{ $dest->name }}</option>
												
										  @endforeach
										</select>
									  
								    </div>
								 </div>
								  
							</div>
				         
                </div><!-- /.box-body -->
              </div><!-- /.box -->

                </div><!-- /.col -->
                </form>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>
<script src="  {{ asset ('js/randomgenerator.js') }}"></script>
<script type="text/javascript">
       $(function () {
		   
        //Initialize Select2 Elements
         $(".select2").select2();
    
var chassisengine = new Bloodhound({
  initialize: false,
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
   prefetch: {
        url: '{!! route('api.cars.searchchasisno') !!}',
        cache: false 
    },
 remote : {
	  url: " {!! route('api.cars.chasisno') !!}" ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
		
	  }
	 
 }
});

//console.log(chassisengine);
chassisengine.clear();
chassisengine.initialize();
$('#bloodhound .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'chassisengine',
  source: chassisengine
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
    $(this).attr('disabled', true).css("background-color", "");
   // $(this).attr('disabled', true);
     $.ajax({
              url: "{!! route('api.cars.singlecar') !!}",
              type: 'get',
              dataType: 'json',
              data:{ chasisno : datum },
            })
            .done(function(result) {
				var bkString = result.carinspection.background.join(" ");
				var engString = result.carinspection.engine.join(" ");
				var extString = result.carinspection.exterior.join(" ");
				var intString = result.carinspection.interior.join(" ");
				var extrasString = result.carinspection.extras.join(" ");
			     $('#inputBackground').val(bkString);
			     $('#inputEngine').val(engString);
			     $('#inputExterior').val(extString);
			     $('#inputInterior').val(intString);
			     $('#inputExtras').val(extrasString);
			     
			     if(typeof(result.carinspection.info) != "undefined" && result.carinspection.info !== null) {
						 $('#inputCarInspectionInfo').val(result.carinspection.info);
				  }
              console.log(result);
            })
            .fail(function(error) {
             
              //alert(error);
              console.log(error);
            });
            
   
    //console.log(datum);
});
       
       
       });
       
    
    
    
    </script>
    
    <script type="text/javascript">
     LaravelApiroute["fullcarsave"] = " {!! route('api.cars.fullcarsave') !!}";
     LaravelApiroute["fullcarsprintcount"] = " {!! route('api.cars.updateprint') !!}"; 
     
    
    </script>
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
 
@endpush

@include('print.carrecord')


