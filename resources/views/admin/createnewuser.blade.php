@extends('layouts.app')


@section('content')
<!-- Main content -->
       

          <div class="row">
           <div class="col-md-8">
				
           <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">New System User</h3>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body">
                @include('auth.register') 
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->
       
				
              
            </div><!-- /.col -->
            <div class="col-md-4">
             
              
             
              
            </div><!-- /.col -->
            
          </div><!-- /.row -->

      
      

@endsection


@push('scripts')
 <script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
      $(function () {
		   
        //Initialize Select2 Elements
        $(".select2").select2();

        
      });
      LaravelApiroute["createUser"] = " {!! route('api.users.store') !!}";
    </script>
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
@endpush



