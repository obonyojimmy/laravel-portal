


<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;

$counter = 1 ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $title }} </h3>
              <div class="box-tools pull-right">
               
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                  <form action="{!! route('api.users.setroles') !!}" method="post" class="form-horizontal">
	              
	               <div class="form-group ">
                        <label for="inputRoleName" class="col-sm-2 control-label">Role name</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputRoleName" placeholder="Role Names" value="" />
                          
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="inputRoleSlug" class="col-sm-2 control-label">Role Slug(Good Name)</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputRoleSlug" placeholder="Role Slug" value="" />
                          
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" id="saveNewRole" class="btn btn-success">Save Role</button>
                        </div>
                      </div>
                    </form>
              
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')


<script type="text/javascript">
      $(function () {
		
		  
		   $('#saveNewRole').click(function() {
			    event.preventDefault();
			 data={
			  'name' : $('#inputRoleName').val() ,
			  'slug' : $('#inputRoleSlug').val() ,
			  
			 };
			 console.log(data);
			  $.ajax({
					  url: '{!! route('api.users.setroles') !!}' ,
					  type: 'get',
					  data : data ,
					  dataType: 'json',
					})
					.done(function(r) {
					 
					  //alert(r);
					  console.log(r);
					   $.growl.notice({ message: "Role sucessfully Saved !" });
					     setTimeout(function() {location.reload(true)}, 2000);
					  
						
					})
					.fail(function(e) {
					 
					  //alert(error);
					  console.log(e);
					});
			
			});
		});
		
</script>
@endpush
















