@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
       

          <div class="row">
            
            <div class="col-md-8">
				
           <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">New Car Inspection</h3>
              <div class="box-tools pull-right">
              
              </div>
            </div>
            <div class="box-body">
                   <form action="{!! route('api.cars.store') !!}" method="post" class="form-horizontal">
	              
	               
                      <div class="form-group ">
                        <label for="inputChasis" class="col-sm-2 control-label">Chasis No</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputChasis" placeholder="Chasis No" value="" />
                          
                        </div>
                      </div>
                      <div class="panel panel-default">
						  <div class="panel-heading">Radiation Survey Report in Microsievert Per Hour( <span class="">µ</span>sv/hr )</div>
						  <div class="panel-body">
							
								 
											   <div class="form-group ">
												<label for="inputBackground" class="col-sm-2 control-label">Background</label>
												<div class="col-sm-10">
												  <div class="row">
													  <div class="col-xs-7">
														  <input type="text" name="name" class="form-control" placeholder="eg.0.04, 0.08, 0.06"  id="inputBackground"  value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomBackground" style="width: 100%;">
										  
															  <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															  
															 
															</select>
													  </div>
													  
													</div>
												
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputExterior" class="col-sm-2 control-label">Exterior</label>
												<div class="col-sm-10">
												   <div class="row">
													  <div class="col-xs-7">
														 <input type="text" name="name" class="form-control"  id="inputExterior" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														 <select  class="form-control  " id="randomExterior" style="width: 100%;">
										  
															  <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															  
															</select>
													  </div>
													  
													</div>
												
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputEngine" class="col-sm-2 control-label">Engine</label>
												<div class="col-sm-10">
												  <div class="row">
													  <div class="col-xs-7">
														  <input type="text" name="name" class="form-control"  id="inputEngine" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomEngine" style="width: 100%;">
										                        <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															</select>
													  </div>
													  
													</div>
												
												 
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputInterior" class="col-sm-2 control-label">Interior</label>
												<div class="col-sm-10">
												  
												  <div class="row">
													  <div class="col-xs-7">
														 <input type="text" name="name" class="form-control"  id="inputInterior" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomInterior" style="width: 100%;">
										                      <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															 
															</select>
													  </div>
													  
													</div>
												
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputExtras" class="col-sm-2 control-label">Extras</label>
												<div class="col-sm-10">
												    <div class="row">
													  <div class="col-xs-7">
														 <input type="text" name="name" class="form-control"  id="inputExtras" placeholder="eg.0.04, 0.08, 0.06" value="" />
												  
													  </div>
													  <div class="col-xs-5">
														  <select  class="form-control  " id="randomExtras" style="width: 100%;">
										                       <option value="Quartile1">Quartile 1 : (0.0001 - 0.0285) </option>
															  <option value="Quartile2">Quartile 2 : (0.0285 - 0.0571) </option>
															  <option value="Quartile3">Quartile 3 : (0.0571 - 0.0856) </option>
															  <option value="Quartile4">Quartile 4 : (0.0856 - 0.1142) </option>
															</select>
													  </div>
													  
													</div>
												
												 
												</div>
											  </div>
											  
						  </div>
						</div>
                      <div class="form-group">
                        <label for="inputCarInspectionInfo" class="col-sm-2 control-label">Other Info</label>
                        <div class="col-sm-10">
							
							 <textarea name="info"  class="form-control" id="inputCarInspectionInfo" placeholder="Any other related  information on the car on inspection"></textarea>
                          
                        </div>
                      </div> 
                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" id="SaveNewCarInpection" class="btn btn-success">Save Inspection</button>
                        </div>
                      </div>
                    </form>
              
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->
       
				
              
            </div><!-- /.col -->
            <div class="col-md-4">
            
            </div><!-- /.col -->
          </div><!-- /.row -->

      
      

@endsection


@push('scripts')
<script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>
<script src="  {{ asset ('js/randomgenerator.js') }}"></script>
<script type="text/javascript">
      $(function () {
		   
        //Initialize Select2 Elements
         //~ $(".select2").select2();
         
	 });
      LaravelApiroute["newcar"] = " {!! route('api.cars.store') !!}";
</script>
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/daterangepicker/daterangepicker-bs3.css') }}">
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
@endpush



