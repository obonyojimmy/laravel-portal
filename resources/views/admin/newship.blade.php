@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
       

          <div class="row">
            
            <div class="col-md-8">
				
           <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">New Ship</h3>
              
            </div>
            <div class="box-body">
                   <form action="{!! route('api.ships.store') !!}" method="post" class="form-horizontal">
	              
	               <!-- Date range -->
                  <div class="form-group">
					  <label for="shipArrivalDeparture" class="col-sm-2 control-label">Arrival - Departure:</label>
                   
                     <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="shipArrivalDeparture">
                    </div><!-- /.input group -->
                     </div>
                  </div><!-- /.form group -->
                      <div class="form-group ">
                        <label for="inputShipName" class="col-sm-2 control-label">Ship name</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputShipName" placeholder="Names" value="" />
                          
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="inputShipName" class="col-sm-2 control-label">Ship Voyage</label>
                        <div class="col-sm-10">
							
                          <input type="text" name="name" class="form-control"  id="inputShipVoyage" placeholder="Ship Voyage" value="" />
                          
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="inputShipInfo" class="col-sm-2 control-label">Ship Info</label>
                        <div class="col-sm-10">
							
							 <textarea name="info"  class="form-control" id="inputShipInfo" placeholder="Ship Information"></textarea>
                          
                        </div>
                      </div>
                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" id="saveNewShip" class="btn btn-danger">Save Ship</button>
                        </div>
                      </div>
                    </form>
              
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->
       
				
              
            </div><!-- /.col -->
            <div class="col-md-4">
             
              
            </div><!-- /.col -->
          </div><!-- /.row -->

      
      

@endsection

@push('scriptsbefore')
<script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
<script src=" {{ asset ('plugins/daterangepicker/daterangepicker.js') }}"></script>
@endpush
@push('scripts')

 
<script type="text/javascript">
      $(function () {
		   
        
         //Date range picker
        $('#shipArrivalDeparture').daterangepicker();

        
      });
      LaravelApiroute["newship"] = " {!! route('api.ships.store') !!}";
    </script>
    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush



