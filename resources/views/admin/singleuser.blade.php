
@extends('layouts.app')

@section('title',  $title ) 

@section('content')
<!-- Main content -->
       

          <div class="row">
           
                <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset("img/user4-128x128.jpg") }}" alt="User profile picture">
                  <h3 class="profile-username text-center">{{ $singleuser->first_name }} {{ $singleuser->last_name }}</h3>
                 

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Phone : </b> {{ $singleuser->phone }}
                    </li>
                    <li class="list-group-item">
                      <b>Email : </b> {{ $singleuser->email }}
                    </li>
                    <li class="list-group-item">
                      <b>Id Number : </b> {{ $singleuser->idnumber }}
                    </li>
                    <li class="list-group-item">
                      <b>Status : </b>  
                      @if ($singleuser->status)
									<span class="label label-success">Approved</span>
								
								@else
									<span class="label label-danger">Denied</span>
								@endif
                    </li>
                    
                    
                  </ul>

                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
                  
                  <li><a href="#settings" data-toggle="tab">Update user</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Documents Printed (7 days)</h3>
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="singleUserLineChart" style="height:250px"></canvas>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                 <div class="tab-pane" id="settings">
					 <div class="box box-info">
                
                <div class="box-body">
                  <div class="">
					  @include('auth.update', ['id'=>  $singleuser->id])
                   
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                 
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
         
            
          </div><!-- /.row -->

      
      

@endsection


@push('scripts')
   <!-- ChartJS 1.0.1 -->
    <script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset ('plugins/chartjs/Chart.min.js') }}"></script>
    <script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>
     <!-- FastClick -->
    <script src="{{ asset ('plugins/fastclick/fastclick.min.js') }}"></script>
    
    <script type="text/javascript">
      $(function () {
		      //Initialize Select2 Elements
        $(".select2").select2();
        $("#inputRoles").select2();
        
       
        var getuserroles =  {{ $singleuser->roles->lists('id')->toJson() }} ;
        console.log(getuserroles);
         $("#inputRoles").select2('val',getuserroles );
       
      });
      
     LaravelApiroute["usercarrecordprintcount"] = " {!! route('api.users.printcounts7days' , ['id' => $singleuser->id ]) !!}";
     LaravelApiroute["updateUser"] = "{!! route('api.users.update' , ['id' => $singleuser->id ]) !!}";
     
    </script>
    <script src="{{ asset ('js/appcharts.js') }}"></script>
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
@endpush




