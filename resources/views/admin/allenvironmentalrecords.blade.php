

<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">All Enviromental Record </h3>
              <div class="box-tools pull-right">
                
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                 <table id="environmental_records" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Importer Name</th>
                        <th>Customs No</th>
                        <th>Batchcode</th>
                        <th>Ship</th>
                        <th>Dest Country</th>
                        <th>Created On</th>
                         <th></th>
                      </tr>
                    </thead>
                     <tbody>
				     </tbody>
                    
                  </table>
                
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src=" {{ asset ('js/jquery.dataTables.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.buttons.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.colVis.min.js') }}"></script>
<script src=" {{ asset ('js/jszip.min.js') }}"></script>
<script src=" {{ asset ('js/pdfmake.min.js') }}"></script>
<script src=" {{ asset ('js/vfs_fonts.js') }}"></script>
<script src=" {{ asset ('js/buttons.html5.min.js') }}"></script>
<script src=" {{ asset ('js/buttons.print.min.js') }}"></script>
<script src=" {{ asset ('js/dataTables.bootstrap.min.js') }}"></script>

<script src=" {{ asset ('js/buttons.bootstrap.min.js') }}"></script>

<script>
      $(function () {
       
         $('#environmental_records').DataTable({
				processing: true,
				serverSide: true,
				dom: 'Bfrtip',
				 buttons: [
					
					{
						extend: 'excelHtml5',
						exportOptions: {
							columns: [ 0, 1, 2, 5 , 6 ]
						}
					},
					{
						extend: 'pdfHtml5',
						exportOptions: {
							columns: [ 0, 1, 2, 5, 6]
						}
					},
					{
						extend: 'print',
						exportOptions: {
							columns: [ 0, 1, 2, 5, 6]
						}
					}
					
					
				],
				//~ buttons: [
					//~ 'print' , 'excel', 'pdf'
				//~ ],
				"fnCreatedRow": function (row, data, index) {
					$('td', row).eq(0).html(index + 1);
				},
						ajax: '{!! route('api.environmental.datatable') !!}',
				columns: [
					{ data: 'id', name: 'environmentalrecords.id' },
					{ data: 'importername', name: 'importers.name' },
					{ data: 'customsno', name: 'environmentalrecords.customsno' },
					{ data: 'batchcode', name: 'environmentalrecords.batchcode' },
					{ data: 'shipname', name: 'ships.name' },
					{ data: 'destcountryname', name: 'destcountry.name' },
					{ data: 'created_at', name: 'environmentalrecords.created_at' },
					{ data: 'action', name: 'environmentalrecords.id' },
					
				]
			});
			
      });
    </script>
    
  
    
@endpush

@push('css')

 <link rel="stylesheet" href="{{ asset ('css/dataTables.bootstrap.min.css') }}">
 
 <link rel="stylesheet" href="{{ asset ('css/buttons.bootstrap.min.css') }}">
 
@endpush

@include('print.environmentalrecord')


