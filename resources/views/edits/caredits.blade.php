
<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
$editdate = Carbon::createFromTimestamp($editdate)->toDateTimeString();
?>
@extends('layouts.app')



@section('title',  $title ) 

@section('content')
<!-- Main content -->
<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Changes of Car - ChassiNo : <strong>{{ $currentCar -> chassisno }}</strong>
               By: <strong> {{ $beforeEditCar -> user_id ==  $afterEditCar -> user_id  ? $beforeEditCar -> user -> first_name .' '.$beforeEditCar -> user -> last_name  : ' Some Error here , Contact System Admin' }}  </strong>
                On : <strong>{{ $editdate }}</strong> </h3>
              <div class="box-tools pull-right">
                
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
				 <div class="col-md-6">
					
					  <form action="" method="post" class="">
						  <div class="box box-info">
							                  <div class="box-header">
											  <h3 class="box-title">Before Edit</h3>
											</div>
							  <div class="box-body">
								
								  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group ">
									  <label for="inputChassis">Chasis</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled class=" form-control" id="inputChassis" type="text" value="{{ $beforeEditCar->chassisno }}">	
									  </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputShiping">Shiping</label>
									  <input disabled type="text" class=" {{ $beforeEditCar->ship->name === $afterEditCar->ship->name ? '' : 'sucessbkg' }} form-control " id="inputShiping"  value="{{ $beforeEditCar->ship->name }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							      <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputImporters">Importer  Names</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class="{{ $beforeEditCar->importer->name  === $afterEditCar->importer->name  ? '' : 'sucessbkg' }} form-control " id="inputImporters"  value="{{ $beforeEditCar->importer->name  }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputAddress">Importer Address</label>
									  <input disabled type="text" class="{{ $beforeEditCar->importer->address === $afterEditCar->importer->address ? '' : 'sucessbkg' }} form-control " id="inputAddress"   value="{{ $beforeEditCar->importer->address }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCarMake">Car make</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled  type="text" class="{{ $beforeEditCar->name === $afterEditCar->name ? '' : 'sucessbkg' }} form-control " id="inputCarMake"  value="{{ $beforeEditCar->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputColour">Colour</label>
									  <input disabled  type="text" class="{{ $beforeEditCar->colour === $afterEditCar->colour ? '' : 'sucessbkg ' }} form-control " id="inputColour"   value="{{ $beforeEditCar->colour }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputEngineNumber">Engine Number</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input  disabled type="text" class="{{ $beforeEditCar->engineno === $afterEditCar->engineno ? '' : ' sucessbkg' }} form-control " id="inputEngineNumber"  value="{{ $beforeEditCar->engineno }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCustoms">Customs</label>
									  <input disabled  type="text" class="{{ $beforeEditCar->customsno === $afterEditCar->customsno ? '' : ' sucessbkg' }} form-control " id="inputCustoms"   value="{{ $beforeEditCar->customsno }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPrevRegCountry">Previous Reg.Country</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class="{{ $beforeEditCar->prevcountry->name  === $afterEditCar->prevcountry->name  ? '' : ' sucessbkg' }} form-control " id="inputPrevRegCountry"  value="{{ $beforeEditCar->prevcountry->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPreviousReg">Previous Reg</label>
									  <input disabled type="text" class="{{ $beforeEditCar->previousreg === $afterEditCar->previousreg ? '' : ' sucessbkg' }} form-control " id="inputPreviousReg"   value="{{ $beforeEditCar->previousreg }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputFuel">Fuel</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class="{{ $beforeEditCar->fuel === $afterEditCar->fuel ? '' : 'sucessbkg ' }} form-control " id="inputFuel"  value="{{ $beforeEditCar->fuel }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputManYear">Manufacture  Year</label>
									  <input disabled type="text" class="{{ $beforeEditCar->year === $afterEditCar->year ? '' : ' sucessbkg' }} form-control " id="inputManYear" value="{{ $beforeEditCar->year }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputClearingAgent">Clearing Agent</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled  type="text" class="{{ $beforeEditCar->clearingagent->name === $afterEditCar->clearingagent->name ? '' : ' sucessbkg' }} form-control " id="inputClearingAgent" value="{{ $beforeEditCar->clearingagent->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputDestination">Destination</label>
									  <input disabled type="text" class="{{ $beforeEditCar->destcountry->name === $afterEditCar->destcountry->name ? '' : ' sucessbkg' }} form-control " id="inputDestination"  value="{{ $beforeEditCar->destcountry->name }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row ">
								  <div class="col-md-12 form-horizontal">
							     <div class="form-group ">
												<label for="inputBackground" class="col-sm-2 control-label">Background</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="{{ implode(" ",$beforeEditCar->editcarinspections->background) === implode(" ",$afterEditCar->editcarinspections->background) ? '' : ' sucessbkg' }} form-control"  id="inputBackground" placeholder="eg.0.04, 0.08, 0.06" 
												  value="<?php $Bbackg =implode(" ",$beforeEditCar->editcarinspections->background); echo $Bbackg ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputExterior" class="col-sm-2 control-label">Exterior</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="{{ implode(" ",$beforeEditCar->editcarinspections->exterior) === implode(" ",$afterEditCar->editcarinspections->exterior) ? '' : ' sucessbkg' }} form-control"  id="inputExterior" placeholder="eg.0.04, 0.08, 0.06"
												   value="<?php $extr =implode(" ",$beforeEditCar->editcarinspections->exterior); echo $extr ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputEngine" class="col-sm-2 control-label">Engine</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="{{ implode(" ",$beforeEditCar->editcarinspections->engine) === implode(" ",$afterEditCar->editcarinspections->engine) ? '' : ' sucessbkg' }} form-control"  id="inputEngine" placeholder="eg.0.04, 0.08, 0.06"
												   value="<?php $eng =implode(" ",$beforeEditCar->editcarinspections->engine); echo $eng ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputInterior" class="col-sm-2 control-label">Interior</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="{{ implode(" ",$beforeEditCar->editcarinspections->interior) === implode(" ",$afterEditCar->editcarinspections->interior) ? '' : ' sucessbkg' }} form-control"  id="inputInterior" placeholder="eg.0.04, 0.08, 0.06" 
												  value="<?php $intr =implode(" ",$beforeEditCar->editcarinspections->interior); echo $intr ; ?>" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputExtras" class="col-sm-2 control-label">Extras</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class="{{ implode(" ",$beforeEditCar->editcarinspections->extras) === implode(" ",$afterEditCar->editcarinspections->extras) ? '' : ' sucessbkg' }} form-control"  id="inputExtras" placeholder="eg.0.04, 0.08, 0.06"
												   value="<?php $extras =implode(" ",$beforeEditCar->editcarinspections->extras); echo $extras ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group">
												<label for="inputCarInspectionInfo" class="col-sm-2 control-label">Other Info</label>
												<div class="col-sm-10">
													
													 <textarea disabled name="info"  class="{{ $beforeEditCar->editcarinspections->info === $afterEditCar->editcarinspections->info ? '' : 'sucessbkg ' }} form-control" id="inputCarInspectionInfo" placeholder="Any other related  information on the car on inspection">{{ $beforeEditCar->editcarinspections->info }}</textarea>
												  
												</div>
											  </div> 
                                 </div>
							  </div><!-- /.row -->
							  
							 </div> <!-- /.box-body -->
						  </div>
					  </form>
					
			     </div>
			     
			     
			     <div class="col-md-6">
					 
					  <form action="" method="post" class="">
						  <div class="box box-info">
							                  <div class="box-header">
											  <h3 class="box-title">After Edit</h3>
											</div>
							  <div class="box-body">
								
								  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputChassis">Chasis</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled class="  form-control" id="inputChassis" type="text" value="{{ $afterEditCar->chassisno }}">	
									  </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputShiping">Shiping</label>
									  <input disabled type="text" class=" form-control " id="inputShiping"  value="{{ $afterEditCar->ship->name }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							      <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputImporters">Importer  Names</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class=" form-control " id="inputImporters"  value="{{ $afterEditCar->importer->name  }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputAddress">Importer Address</label>
									  <input disabled type="text" class=" form-control " id="inputAddress"   value="{{ $afterEditCar->importer->address }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCarMake">Car make</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled  type="text" class=" form-control " id="inputCarMake"  value="{{ $afterEditCar->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputColour">Colour</label>
									  <input disabled  type="text" class=" form-control " id="inputColour"   value="{{ $afterEditCar->colour }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputEngineNumber">Engine Number</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input  disabled type="text" class=" form-control " id="inputEngineNumber"  value="{{ $afterEditCar->engineno }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputCustoms">Customs</label>
									  <input disabled  type="text" class=" form-control " id="inputCustoms"   value="{{ $afterEditCar->customsno }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPrevRegCountry">Previous Reg.Country</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class=" form-control " id="inputPrevRegCountry"  value="{{ $afterEditCar->prevcountry->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputPreviousReg">Previous Reg</label>
									  <input disabled type="text" class=" form-control " id="inputPreviousReg"   value="{{ $afterEditCar->previousreg }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputFuel">Fuel</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled type="text" class=" form-control " id="inputFuel"  value="{{ $afterEditCar->fuel }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputManYear">Manufacture  Year</label>
									  <input disabled type="text" class=" form-control " id="inputManYear" value="{{ $afterEditCar->year }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row">
							     <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputClearingAgent">Clearing Agent</label>
									  <div class="row">
										  <div class="col-xs-12">
									  <input disabled  type="text" class=" form-control " id="inputClearingAgent" value="{{ $afterEditCar->clearingagent->name }}">
								    </div>
								    </div>
								    </div>
								 </div>
								 <div class="col-xs-6">
									<div class="form-group">
									  <label for="inputDestination">Destination</label>
									  <input disabled type="text" class=" form-control " id="inputDestination"  value="{{ $afterEditCar->destcountry->name }}" />
								    </div>
								 </div>
							  </div><!-- /.row -->
							  <div class="row ">
								  <div class="col-md-12 form-horizontal">
							     <div class="form-group ">
												<label for="inputBackground" class="col-sm-2 control-label">Background</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class=" form-control"  id="inputBackground" placeholder="eg.0.04, 0.08, 0.06" value="<?php $Abackg =implode(" ",$afterEditCar->editcarinspections->background); echo $Abackg ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputExterior" class="col-sm-2 control-label">Exterior</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class=" form-control"  id="inputExterior" placeholder="eg.0.04, 0.08, 0.06" value="<?php $extr =implode(" ",$afterEditCar->editcarinspections->exterior); echo $extr ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputEngine" class="col-sm-2 control-label">Engine</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class=" form-control"  id="inputEngine" placeholder="eg.0.04, 0.08, 0.06" value="<?php $eng =implode(" ",$afterEditCar->editcarinspections->engine); echo $eng ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group ">
												<label for="inputInterior" class="col-sm-2 control-label">Interior</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class=" form-control"  id="inputInterior" placeholder="eg.0.04, 0.08, 0.06" value="<?php $intr =implode(" ",$afterEditCar->editcarinspections->interior); echo $intr ; ?>" />
												  
												</div>
											  </div>
											  <div class="form-group ">
												<label for="inputExtras" class="col-sm-2 control-label">Extras</label>
												<div class="col-sm-10">
													
												  <input disabled type="text" name="name" class=" form-control"  id="inputExtras" placeholder="eg.0.04, 0.08, 0.06" value="<?php $extras =implode(" ",$afterEditCar->editcarinspections->extras); echo $extras ; ?>" />
												  
												</div>
											  </div>
											   <div class="form-group">
												<label for="inputCarInspectionInfo" class="col-sm-2 control-label">Other Info</label>
												<div class="col-sm-10">
													
													 <textarea disabled name="info"  class=" form-control" id="inputCarInspectionInfo" placeholder="Any other related  information on the car on inspection">{{ $afterEditCar->editcarinspections->info }}</textarea>
												  
												</div>
											  </div> 
                                 </div>
							  </div><!-- /.row -->
							  
							 </div> <!-- /.box-body -->
						  </div>
					  </form>
					
			     </div>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->

@endsection


@push('scripts')

<script src="  {{ asset ('plugins/select2/select2.full.min.js') }}"></script>


    
@endpush

@push('css')
 <link rel="stylesheet" href="{{ asset ('plugins/select2/select2.min.css') }}">
 
@endpush

@include('print.carrecord')


