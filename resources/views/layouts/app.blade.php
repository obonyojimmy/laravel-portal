<?php 
use Carbon\Carbon ;

?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title> Portal Example - @yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link href="{{ asset("css/bootstrap.css") }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset("css/ionicons.min.css") }} " rel="stylesheet" type="text/css" />

    <link href="{{ asset("css/all.css")}}" rel="stylesheet" type="text/css" />
   
    <link href="{{ asset("css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    
    <link href="{{ asset("css/jquery.growl.css")}}" rel="stylesheet" type="text/css" />
    
    @stack('css')
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue  fixed sidebar-mini hold-transition 
 @unless(Sentinel::check())
   login-page
 @endunless
">

 <meta name="csrf-token" content="{{ csrf_token() }}">

@if(Sentinel::check())
   <div class="wrapper hidden-print ">

   
      @include('layouts.header')

    
      @include('layouts.sidebar')

     
      <div class="content-wrapper">
      
        <section class="content-header">
          
          
          
        </section>

       
        <section class="content">          
          
          @yield('content')
        </section>
      </div>
      
     @include('layouts.footer')
       
     
     

    </div>

 
@else
 
 @yield('login')
 
@endif
   
    
    <script src="{{ asset ("js/bootstrap.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("js/jquery.growl.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript" ></script>
    @stack('scriptsbefore')
    <script src="{{ asset ("js/app.min.js") }}" type="text/javascript"></script>
    <script src="  {{ asset ('plugins/typeahead/typeahead.bundle.min.js') }}"></script>
     <script src="  {{ asset ('js/bloodhoundLaravel.js') }}"></script>
     <script type="text/javascript"> 
	     var LaravelApiroute = {}; 
         LaravelApiroute["bloodhoundshipsearch"] = " {!! route('api.ships.singleship') !!}";
         LaravelApiroute["bloodhoundcarmakesearch"] = " {!! route('api.cars.carmakesearch') !!}";
          LaravelApiroute["bloodhoundcarcolours"] = " {!! route('api.cars.carcolours') !!}";
          LaravelApiroute["bloodhoundclearingAgents"] = " {!! route('api.cars.clearingagents') !!}"; 
          LaravelApiroute["bloodhoundimportersnames"] = " {!! route('api.cars.importersnames') !!}";
          LaravelApiroute["bloodhoundimportersaddress"] = " {!! route('api.cars.importersaddress') !!}";
          LaravelApiroute["dashurl"] =  "{!! route('admin.dashboard.index' ) !!}";
         LaravelApiroute["randomgenerator"] =  "{!! route('admin.dashboard.randomgenerator' ) !!}";
        
   </script>
   @if(Sentinel::check())
    @if($user->hasAccess(['admin.dashboard.index']))
     <script type="text/javascript"> 
        LaravelApiroute["getdailynotification"] = " {!! route('admin.dashboard.getdailynotification') !!}";
        LaravelApiroute["dailyreprintsurl"] = " {!! route('admin.reprints.all',['fromdate' => Carbon::today()->toDateString() , 'todate' => Carbon::tomorrow()->toDateString() ]) !!}";
        LaravelApiroute["dailyeditreprintsurl"] = " {!! route('admin.edits.all',['fromdate' => Carbon::today()->toDateString() , 'todate' => Carbon::tomorrow()->toDateString() ]) !!}";
        LaravelApiroute["dailyprintsurl"] = " {!! route('admin.car.allcarrecords',['fromdate' => Carbon::today()->toDateString() , 'todate' => Carbon::tomorrow()->toDateString() ]) !!}";
      </script>
	<script src=" {{ asset ('js/getdailynotification.js') }}"></script>
	@endif
	@endif
    @stack('scripts')
     <script src="{{ asset ("js/all.js") }}" type="text/javascript"></script>
     <script src="{{ asset ("js/JsBarcode.all.js") }}" type="text/javascript"></script>
     
  </body>
</html>
