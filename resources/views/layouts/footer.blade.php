<?php 
use Carbon\Carbon ;

$now = Carbon::now() ;
?>
<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; {{ $now->year  }} Portal Example , Developed by <a href="http://jimmycliff.me.net">Jimmy Cliff</a>.</strong> All rights reserved.
</footer>
