<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset("img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p> {{ $user->first_name }} </p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

   

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
		
     <li>
              <a href="{!! route('admin.dashboard.index') !!}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
              </a>
              
     </li>
     @if($user->hasAccess(['admin.newship']))
     <li>
		 
              <a href="{!! route('admin.newship') !!}">
                <i class="fa fa-ship"></i> <span>New Ship Record</span> 
              </a>
    </li>
    @endif
    @if($user->hasAccess(['admin.car.*']))
    <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Car Records</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
				@if($user->hasAccess(['admin.car.carinspection']))
                 <li><a href="{!! route('admin.car.carinspection') !!}"><i class="fa fa-circle-o"></i>New Car Inspection</a></li>
                @endif
                @if($user->hasAccess(['admin.car.inspectedcar']))
                <li><a href="{!! route('admin.car.inspectedcar') !!}"><i class="fa fa-circle-o"></i>Inspected Car</a></li>
                @endif
                @if($user->hasAccess(['admin.car.fullcarrecord']))
                 <li><a href="{!! route('admin.car.fullcarrecord') !!}"><i class="fa fa-circle-o"></i>Full Car Record</a></li>
                @endif
                @if($user->hasAccess(['admin.car.allcarrecords']))
                <li><a href="{!! route('admin.car.allcarrecords') !!}"><i class="fa fa-circle-o"></i>All Car Records</a></li>
                @endif
                
                
                
              </ul>
     </li>
     @endif
      @if($user->hasAccess(['admin.environmental.*']))
      <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Environmental Records</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			   @if($user->hasAccess(['admin.environmental.newenvironmentalrecord']))
                <li><a href="{!! route('admin.environmental.newenvironmentalrecord') !!}"><i class="fa fa-circle-o"></i>New Environmental Record</a></li>
               @endif
               @if($user->hasAccess(['admin.environmental.allenvironmentalrecords']))
               <li><a href="{!! route('admin.environmental.allenvironmentalrecords') !!}"><i class="fa fa-circle-o"></i>All Environmental Records</a></li>
                @endif
                
              </ul>
     </li>
     @endif
      @if($user->hasAccess(['admin.invoice.*']))
     <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Invoice Manager</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			    @if($user->hasAccess(['admin.invoice.newinvoice']))
                 <li><a href="{!! route('admin.invoice.newinvoice') !!}"><i class="fa fa-circle-o"></i>New Invoice</a></li>
                @endif
                @if($user->hasAccess(['admin.invoice.allinvoice']))
                <li><a href="{!! route('admin.invoice.allinvoice') !!}"><i class="fa fa-circle-o"></i>All Invoice</a></li>
               @endif
                
                
               
              </ul>
     </li>
     @endif
      @if($user->hasAccess(['admin.users.*']))
     <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>User Manager</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                @if($user->hasAccess(['admin.users.register']))
               <li><a href="{!! route('admin.users.register') !!}"><i class="fa fa-circle-o"></i>New User</a></li>
                @endif
                @if($user->hasAccess(['admin.users.listuser']))
                <li><a href="{!! route('admin.users.listuser') !!}"><i class="fa fa-circle-o"></i>All System Users</a></li>
                @endif
                @if($user->hasAccess(['admin.users.usersroles']))
                <li><a href="{!! route('admin.users.usersroles') !!}"><i class="fa fa-circle-o"></i>User Roles</a></li>
                @endif
                
                
                
               
              </ul>
     </li>
     @endif
     @if($user->hasAccess(['admin.analysis.*']))
     <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Record Analysis</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
				@if($user->hasAccess(['admin.analysis.carrecords']))
                <li><a href="{!! route('admin.analysis.carrecords') !!}"><i class="fa fa-circle-o"></i>Car Records</a></li>
                @endif
                @if($user->hasAccess(['admin.analysis.envrecords']))
                 <li><a href="{!! route('admin.analysis.envrecords') !!}"><i class="fa fa-circle-o"></i>Environmental Records</a></li>
                @endif
                @if($user->hasAccess(['admin.analysis.quartiles']))
                 <li><a href="{!! route('admin.analysis.quartiles') !!}"><i class="fa fa-circle-o"></i>Quartiles</a></li>
                @endif
                
              </ul>
     </li>
      @endif
       @if($user->hasAccess(['admin.reprints']))
     <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Reprints Manager</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
              
               <li><a href="{!! route('admin.reprints.all') !!}"><i class="fa fa-circle-o"></i>Car Record Reprints</a></li>
               
                
              </ul>
     </li>
     @endif
      @if($user->hasAccess(['admin.edits']))
     <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Edits Manager</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
              
               <li><a href="{!! route('admin.edits.all') !!}"><i class="fa fa-circle-o"></i>Car Record Edits</a></li>
               
                
              </ul>
     </li>
     @endif
      
    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
