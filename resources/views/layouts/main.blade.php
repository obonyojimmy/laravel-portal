<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ $page_title or "Admin" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link href="{{ asset("css/bootstrap.css") }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset("css/ionicons.min.css") }} " rel="stylesheet" type="text/css" />

    <link href="{{ asset("css/main.min.css")}}" rel="stylesheet" type="text/css" />
   
    <link href="{{ asset("css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    @stack('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
   <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="wrapper">

   
      @include('layouts.header')

    
      @include('layouts.sidebar')

     
      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            {{ $page_title or "Page Title" }}
            <small>{{ $page_description or null }}</small>
          </h1>
          
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
        </section>

       
        <section class="content">          
          
          @yield('content')
        </section>
      </div>

     
     

    </div>

    
    <script src="{{ asset ("js/bootstrap.js") }}" type="text/javascript"></script>
  
    <script src="{{ asset ("js/app.min.js") }}" type="text/javascript"></script>
    
   
    
   <script type="text/javascript"> 
	    var LaravelApiroute = {}; 
    
   </script>
   
    @stack('scripts')
    
     <script src="{{ asset ("js/all.js") }}" type="text/javascript"></script>
  </body>
</html>
