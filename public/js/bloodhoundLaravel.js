$(function () {
      
  var shipping = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundshipsearch ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputShiping').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'shipping',
  source: shipping
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
    //$(this).attr('disabled', true).css("background-color", "");
    
     //~ $.ajax({
              //~ url: "{!! route('api.cars.singlecar') !!}",
              //~ type: 'get',
              //~ dataType: 'json',
              //~ data:{ chasisno : datum },
            //~ })
            //~ .done(function(result) {
			  //~ $( '.inputHidden' ).prop("disabled", false);
              //~ console.log(result);
            //~ })
            //~ .fail(function(error) {
             
              //~ //alert(error);
              //~ console.log(error);
            //~ });
            
   
    console.log(datum);
});

 });

$(function () {
      
  var carmake = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundcarmakesearch ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputCarMake').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'carmake',
  source: carmake
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
     //~ $.ajax({
              //~ url: "{!! route('api.cars.singlecar') !!}",
              //~ type: 'get',
              //~ dataType: 'json',
              //~ data:{ chasisno : datum },
            //~ })
            //~ .done(function(result) {
			  //~ $( '.inputHidden' ).prop("disabled", false);
              //~ console.log(result);
            //~ })
            //~ .fail(function(error) {
             
              //~ //alert(error);
              //~ console.log(error);
            //~ });
            
   
    console.log(datum);
});

 });

$(function () {
      
  var carColour = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundcarcolours ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputColour').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'carColour',
  source: carColour
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
     //~ $.ajax({
              //~ url: "{!! route('api.cars.singlecar') !!}",
              //~ type: 'get',
              //~ dataType: 'json',
              //~ data:{ chasisno : datum },
            //~ })
            //~ .done(function(result) {
			  //~ $( '.inputHidden' ).prop("disabled", false);
              //~ console.log(result);
            //~ })
            //~ .fail(function(error) {
             
              //~ //alert(error);
              //~ console.log(error);
            //~ });
            
   
    console.log(datum);
});

 });

$(function () {
      
  var clearingAgents = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundclearingAgents ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputClearingAgent').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'clearingAgents',
  source: clearingAgents
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
     
   
    console.log(datum);
});

 });

$(function () {
      
  var importerNames = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundimportersnames ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputImporters').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'importerNames',
  source: importerNames
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
   
    console.log(datum);
});

 });

$(function () {
      
  var importersadress = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,

 remote : {
	  url: LaravelApiroute.bloodhoundimportersaddress ,
	  prepare: function (query, settings){
		  settings.type = "POST";
		  settings.data = {q: query };
		  return settings;
	  }
	 
 }
});


$('#inputAddress').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'importersadress',
  source: importersadress
}).bind('typeahead:selected', function (obj, datum) {
    console.log(obj);
   // $(this).attr('disabled', true).css("background-color", "");
    
            
   
    console.log(datum);
});

 });

//# sourceMappingURL=bloodhoundLaravel.js.map
