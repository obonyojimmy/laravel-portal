<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clearingagent extends Model
{
    //
      protected $table = 'clearingagents';
      
     protected $fillable = [
        'name'
    ];
   
    
      public function cars()
    {
        return $this->hasMany('App\Car');
    }
    
     public function environmentalrecords()
    {
        return $this->hasMany('App\Environmentalrecord');
    }
}
