<?php

namespace App;


//use Carbon\Carbon ;
use Illuminate\Database\Eloquent\Model;

class Environmentalrecord extends Model
{
    //
    protected $table = 'environmentalrecords';
    
     protected $fillable = [
        'name','batchcode' ,'batchsize' ,'destcountry_id' ,'ship_id' ,'user_id' , 'customsno' ,'port' ,'importer_id','clearingagent_id' ,'description','manufacture' , 'expiry'
    ];
    
     protected $dates = ['manufacture', 'expiry'];
     
     //~ public function getManufactureAttribute($value)
	//~ {
		 //~ $dt = new Carbon($value);  
		 
		 //~ return $dt->format('d/m/Y'); 
	//~ }
	
	//~ public function getExpiryAttribute($value)
	//~ {
		 //~ $dt = new Carbon($value);  
		 
		 //~ return $dt->format('d/m/Y'); 
	//~ }
     
      public function ship()
    {
        return $this->belongsTo('App\Ship');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
     public function destcountry()
    {
        return $this->belongsTo('App\DestCountry');
    }
     public function importer()
    {
        return $this->belongsTo('App\Importer');
    }
    
     public function clearingagent()
    {
        return $this->belongsTo('App\Clearingagent');
    }
    
     public function environmentalinspection()
    {
        return $this->hasOne('App\Environmentalinspection' , 'environmentalrecord_id');
    }
    
      public function docprint()
    {
        return $this->hasMany('App\Docprint');
    }
}
