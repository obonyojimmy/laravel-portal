<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrevCountry extends Model
{
    //
    protected $table = 'prevcountry';
     
     protected $fillable = [
        'code',
        'name'
    ];
    
    public function cars()
    {
        return $this->hasMany('App\Car');
    }
}
