<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //
      protected $fillable = [
        'name', 'chassisno', 'customsno','colour','prevcountry_id','engineno','year','fuel','previousreg','destcountry_id','clearingagent_id','ship_id','user_id','importer_id','status'
    ];
    
    public function setChassisnoAttribute($value)
    {
        $this->attributes['chassisno'] = strtoupper($value); 
    }
    
     public function ship()
    {
        return $this->belongsTo('App\Ship');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
      public function destcountry()
    {
        return $this->belongsTo('App\DestCountry');
    }
    
    public function prevcountry()
    {
        return $this->belongsTo('App\PrevCountry');
    }
    
     public function carinspection()
    {
        return $this->hasOne('App\Carinspection' );
    }
    
    //~ public function importers()
    //~ {
        //~ return $this->belongsToMany('App\Importer');
    //~ }
    
     public function importer()
    {
        return $this->belongsTo('App\Importer');
    }
    
     public function clearingagent()
    {
        return $this->belongsTo('App\Clearingagent');
    }
    
     public function docprint()
    {
        return $this->hasMany('App\Docprint');
    }
    //~ public function caredit()
    //~ {
        //~ return $this->hasMany('App\Caredit');
    //~ }
}
