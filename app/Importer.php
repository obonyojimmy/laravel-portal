<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importer extends Model
{
    //
    
     protected $table = 'importers';
     
      protected $fillable = [
        'name',
        'address'
    ];
    
     //~ public function cars()
    //~ {
        //~ return $this->belongsToMany('App\Car');
    //~ }
    
     public function cars()
    {
        return $this->hasMany('App\Car');
    }
    
    public function environmentalrecords()
    {
        return $this->hasMany('App\Environmentalrecord');
    }
}
