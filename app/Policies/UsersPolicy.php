<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;


class UsersPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
     /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return bool
     */
    public function user_show(User $user,$newuser)
    {
      
        $roleArray = false ;
		foreach ($newuser->roles as $role) {
			if($role->id === 1){
				$roleArray = true ;
				
				}
		 }
		return $roleArray ;
		
    }
     public function testthis(User $user,$newuser)
    {
       //~ if($user->id === $test->id ){
		   //~ return true ;
		  //~ }
		  $roleArray = false ;
		foreach ($user->roles as $role) {
			if($role->id == 1){
				$roleArray = true ;
				//return true ;
				}
		 }
		return $roleArray ;
		  
		//  return true ;
		//return $user->id === $test->id ;
    }
    
    //~ public function before($user, user_show)
	//~ {
		//~ if ($user->superadmin ) {
			//~ return true;
		//~ }
	//~ }
}
