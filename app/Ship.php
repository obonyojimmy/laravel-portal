<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    //
     protected $fillable = [
        'name', 'info', 'arrival','departure','user_id' ,'status' ,'voyage'
    ];
     protected $dates = ['arrival', 'departure'];
     
     public function user()
    {
        return $this->belongsTo('App\User' );
    }
    
     public function cars()
    {
        return $this->hasMany('App\Car');
    }
    
     public function environmentalrecords()
    {
        return $this->hasMany('App\Environmentalrecord');
    }
}
