<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caredit extends Model
{
    //
    protected $table = 'caredits';
      protected $fillable = [
        'name', 'chassisno', 'customsno','colour','prevcountry_id','engineno','year','fuel','previousreg','destcountry_id','clearingagent_id','ship_id','user_id','importer_id','status','car_id' ,'caredit_id'
    ];
    
    public function setChassisnoAttribute($value)
    {
        $this->attributes['chassisno'] = strtoupper($value); 
    }
    
     public function ship()
    {
        return $this->belongsTo('App\Ship');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
      public function destcountry()
    {
        return $this->belongsTo('App\DestCountry');
    }
    
    public function prevcountry()
    {
        return $this->belongsTo('App\PrevCountry');
    }
    
     public function editcarinspections()
    {
        return $this->hasOne('App\Editcarinspections','caredit_id' );
    }
    
    
    
     public function importer()
    {
        return $this->belongsTo('App\Importer');
    }
    
     public function clearingagent()
    {
        return $this->belongsTo('App\Clearingagent');
    }
    
     public function docprint()
    {
        return $this->hasOne('App\Docprint' );
    }
    
     //~ public function car()
    //~ {
        //~ return $this->belongsTo('App\Car');
    //~ }
}
