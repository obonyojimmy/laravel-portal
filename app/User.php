<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;

use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser ;

class User extends SentinelUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','permissions', 'email', 'password','username','idnumber','status','superadmin','info','phone'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //~ protected $hidden = [
        //~ 'password', 'remember_token',
    //~ ];
    protected $loginNames = ['email'];
    
    protected $casts = [
        'superadmin' => 'boolean' ,
        'status' => 'boolean'
    ];
    
  
     public function ship()
    {
        return $this->hasMany('App\Ship');
    }
    
     public function carinspections()
    {
        return $this->hasMany('App\Carinspection');
    }
    
     public function cars()
    {
        return $this->hasMany('App\Car');
    }
    
     public function environmentalrecords()
    {
        return $this->hasMany('App\Environmentalrecord');
    }
    public function docprint()
    {
        return $this->hasMany('App\Docprint');
    }
     public function caredits()
    {
        return $this->hasMany('App\Caredit');
    }
    
}
