<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestCountry extends Model
{
    //
     protected $table = 'destcountry';
     
     protected $fillable = [
        'code',
        'name'
    ];
    
    public function cars()
    {
        return $this->hasMany('App\Car');
    }
    
    public function environmentalrecords()
    {
        return $this->hasMany('App\Environmentalrecord');
    }
}
