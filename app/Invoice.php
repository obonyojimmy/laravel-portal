<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
     protected $table = 'invoices';
    
     protected $fillable = [
        'name','email' ,'idnumber' ,'address' ,'status' ,'phone' , 'user_id' ,'subtotal' ,'tax' ,'total','otherinfo' , 'items','paymentdue'
    ];
    
     //~ protected $dates = ['paymentdue'];
     
     //~ protected $dateFormat = 'd/m/Y';
     
     protected $casts = [
     // 'items' => 'array',
      'subtotal' => 'float',
        'subtotal' => 'float',
        'tax' => 'float',
        'total' => 'float'
    ];
     
     public function setItemsAttribute($value)
    {
        $this->attributes['items'] = json_encode($value);
    }
     
      public function user()
    {
        return $this->belongsTo('App\User');
    }
    
     public function docprint()
    {
        return $this->hasMany('App\Docprint');
    }
     
   
}
