<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docprint extends Model
{
    //
    
    //
    protected $table = 'prints';
    
    protected $fillable = [
        'user_id', 'car_id', 'environmentalrecord_id','invoice_id','status','reprintstatus' ,'caredit_id' ,'editedcar_id'
    ];
    
     //~ protected $casts = [
       //~ 'status' => 'boolean'
    //~ ];
    
     public function user()
    {
        return $this->belongsTo('App\User');
    }
    
     public function car()
    {
        return $this->belongsTo('App\Car');
    }
    
    public function environmentalrecord()
    {
        return $this->belongsTo('App\Environmentalrecord');
    }
    
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
    
    public function caredit()
    {
		return $this->belongsTo('App\Caredit');
        
    }
}
