<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
 Route::get('reg', ['uses' => 'UsersController@regis', 'as' => 'reg']);
  Route::get('replicate', ['uses' => 'CarController@testreplicate', 'as' => 'testreplicate']);
//Route::get('/', function () { return view('auth/login');  });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => ['web']], function () {
	//Route::group(['middleware' => 'web'], function () {
   // Route::auth();
     Route::get('login', function () {
       return view('auth/login');
    });
     Route::group(['prefix' => 'api'], function () {
		Route::any('users/login', ['uses' => 'UsersController@login', 'as' => 'api.users.login']);
		Route::any('users/datatable', ['uses' => 'UsersController@datatable', 'as' => 'api.users.datatable' ]);
		Route::any('users/printcounts7days/{id}', ['uses' => 'UsersController@printcounts7days', 'as' => 'api.users.printcounts7days' ]);
		Route::any('cars/chasisno', ['uses' => 'CarController@chasisno', 'as' => 'api.cars.chasisno' ]);
		Route::any('cars/singlecar', ['uses' => 'CarController@singlecar', 'as' => 'api.cars.singlecar' ]);
		Route::any('cars/searchchasisno', ['uses' => 'CarController@searchchasisno', 'as' => 'api.cars.searchchasisno' ]);
		Route::any('cars/carmakesearch', ['uses' => 'CarController@carmakesearch', 'as' => 'api.cars.carmakesearch' ]);
		Route::any('cars/carcolours', ['uses' => 'CarController@carcolours', 'as' => 'api.cars.carcolours' ]);
		Route::any('cars/clearingagents', ['uses' => 'CarController@clearingagents', 'as' => 'api.cars.clearingagents' ]);
		Route::any('cars/importersnames', ['uses' => 'CarController@importersnames', 'as' => 'api.cars.importersnames' ]);
		Route::any('cars/importersaddress', ['uses' => 'CarController@importersaddress', 'as' => 'api.cars.importersaddress' ]);
		Route::any('cars/datatable/{fromdate?}/{todate?}', ['uses' => 'CarController@datatable', 'as' => 'api.cars.datatable' ]);
		Route::any('cars/reprintsdatatable/{fromdate?}/{todate?}', ['uses' => 'CarController@reprintsdatatable', 'as' => 'api.cars.reprintsdatatable' ]);
		Route::any('cars/singlereprintdatatable/{id}', ['uses' => 'CarController@singlereprintdatatable', 'as' => 'api.cars.singlereprintdatatable' ]);
		Route::any('cars/singlereprintdatatable/{id}/{userid}', ['uses' => 'CarController@singlereprintwithdatesdatatable', 'as' => 'api.cars.singlereprintwithdatesdatatable' ]);
		Route::any('cars/editsdatatable/{fromdate?}/{todate?}', ['uses' => 'CarController@editsdatatable', 'as' => 'api.cars.editsdatatable' ]);
		Route::any('cars/singleditdatatable/{id}', ['uses' => 'CarController@singleditdatatable', 'as' => 'api.cars.singleditdatatable' ]);
		Route::any('cars/singlereeditwithdatesdatatable/{id}/{userid}', ['uses' => 'CarController@singlereeditwithdatesdatatable', 'as' => 'api.cars.singlereeditwithdatesdatatable' ]);
		
		Route::any('ships/singleship', ['uses' => 'ShipController@singleship', 'as' => 'api.ships.singleship' ]);
		Route::post('cars/inspecatedcarsave', ['uses' => 'CarController@inspecatedcarsave', 'as' => 'api.cars.inspecatedcarsave' ]);
		Route::any('cars/fullcarsave', ['uses' => 'CarController@fullcarsave', 'as' => 'api.cars.fullcarsave' ]);
		Route::any('cars/chartdata', ['uses' => 'CarController@chartdata', 'as' => 'api.cars.chartdata' ]);
		Route::any('cars/updateprint', ['uses' => 'CarController@updateprint', 'as' => 'api.cars.updateprint' ]);
		Route::any('cars/quartilespiechart', ['uses' => 'CarController@quartilespiechartdata', 'as' => 'api.cars.quartilespiechart' ]);
		Route::any('cars/daterangequartilespiechart', ['uses' => 'CarController@daterangequartilespiechart', 'as' => 'api.cars.daterangequartilespiechart' ]);
		Route::any('environmental/updateprint', ['uses' => 'EnvironmentalController@updateprint', 'as' => 'api.environmental.updateprint' ]);
		Route::any('environmental/datatable', ['uses' => 'EnvironmentalController@datatable', 'as' => 'api.environmental.datatable' ]);
		Route::any('environmental/chartdata', ['uses' => 'EnvironmentalController@chartdata', 'as' => 'api.environmental.chartdata' ]);
		Route::any('invoice/datatable', ['uses' => 'InvoiceController@datatable', 'as' => 'api.invoice.datatable' ]);
		
		//roles 
		 Route::get('users/setroles', ['uses' => 'UsersController@setroles', 'as' => 'api.users.setroles']);
		 Route::get('users/usersetrole', ['uses' => 'UsersController@usersetrole', 'as' => 'usersetrole']);
		 Route::get('users/usersetpermission', ['uses' => 'UsersController@usersetpermission', 'as' => 'usersetpermission']);
		 Route::get('users/useraddpermission', ['uses' => 'UsersController@useraddpermission', 'as' => 'useraddpermission']);
		 Route::get('users/userremovepermission', ['uses' => 'UsersController@userremovepermission', 'as' => 'userremovepermission']);
		 Route::get('users/roleaddpermission', ['uses' => 'UsersController@roleaddpermission', 'as' => 'api.users.roleaddpermission']);
		 
		Route::resource('users', 'UsersController');
		Route::resource('ships', 'ShipController');
		Route::resource('cars', 'CarController');
		Route::resource('environmental', 'EnvironmentalController');
		Route::resource('invoice', 'InvoiceController');
		
	});

   // Route::get('/', function () { return view('admin.dashboard');  });
   // Route::get('/home', 'HomeController@index');
   Route::get('/', ['uses' => 'UsersController@checklogin', 'as' => 'api.users.checklogin']);
    Route::group(['as' => 'admin.','prefix' => 'admin'], function () {
            
            
             Route::get('dashboard/logout', ['as' => 'logout', 'uses' => 'UsersController@logout']);
             
              Route::group(['as' => 'dashboard.','prefix' => 'dashboard'], function () {
				  Route::get('/',['uses' => 'DashboardController@dashboard', 'as' => 'index'] );
                  Route::any('datatable', ['uses' => 'DashboardController@datatable', 'as' => 'datatable' ]);
                  Route::any('chartdata', ['uses' => 'DashboardController@chartdata', 'as' => 'chartdata' ]);
                  Route::any('randomgenerator', ['uses' => 'DashboardController@randomgenerator', 'as' => 'randomgenerator' ]);
                  Route::any('getdailynotification', ['uses' => 'DashboardController@getdailynotification', 'as' => 'getdailynotification' ]);
		         
               
             
            });
             Route::get('newship', ['uses' => 'DashboardController@newship', 'as' => 'newship']);
            
            
             Route::group(['as' => 'car.','prefix' => 'car'], function () {
				 
               Route::get('carinspection', ['uses' => 'DashboardController@carinspection', 'as' => 'carinspection']);
               Route::get('inspectedcar', ['uses' => 'DashboardController@inspectedcar', 'as' => 'inspectedcar']);
               Route::get('fullcarrecord', ['uses' => 'DashboardController@fullcarrecord', 'as' => 'fullcarrecord']);
               Route::get('allcarrecords/{fromdate?}/{todate?}', ['uses' => 'DashboardController@allcarrecords', 'as' => 'allcarrecords']);
               Route::get('reprint/{id}', ['uses' => 'DashboardController@carreprint', 'as' => 'reprint']);
               Route::get('edit/{id}', ['uses' => 'DashboardController@caredit', 'as' => 'edit']);
             
            });
             Route::group(['as' => 'environmental.','prefix' => 'environmental'], function () {
				 
               Route::get('newrecord', ['uses' => 'DashboardController@newenvironmentalrecord', 'as' => 'newenvironmentalrecord']);
               Route::get('allrecords', ['uses' => 'DashboardController@allenvironmentalrecords', 'as' => 'allenvironmentalrecords']);
               Route::get('reprint/{id}', ['uses' => 'DashboardController@environmentalreprint', 'as' => 'reprint']);
               Route::get('edit/{id}', ['uses' => 'DashboardController@environmentaledit', 'as' => 'edit']);
               
             
            });
            Route::group(['as' => 'invoice.','prefix' => 'invoice'], function () {
				 
               Route::get('newinvoice', ['uses' => 'DashboardController@newinvoice', 'as' => 'newinvoice']);
               Route::get('allinvoice', ['uses' => 'DashboardController@allinvoice', 'as' => 'allinvoice']);
               Route::get('reprint/{id}', ['uses' => 'DashboardController@invoicereprint', 'as' => 'reprint']);
               Route::get('edit/{id}', ['uses' => 'DashboardController@invoiceedit', 'as' => 'edit']);
               
             
            });
             Route::group(['as' => 'users.','prefix' => 'users'], function () {
				  Route::get('newuser', ['uses' => 'DashboardController@register', 'as' => 'register']);
				  Route::get('listuser', ['uses' => 'DashboardController@listuser', 'as' => 'listuser']);
				  Route::get('user/{id}', ['uses' => 'DashboardController@singleuser', 'as' => 'user' ]);
				  Route::get('roles', ['uses' => 'DashboardController@usersroles', 'as' => 'usersroles' ]);
				   Route::get('role/{id}', ['uses' => 'DashboardController@singlerole', 'as' => 'singlerole' ]);
				   Route::get('newsinglerole', ['uses' => 'DashboardController@newsinglerole', 'as' => 'newsinglerole' ]);
				 
             });
             Route::group(['as' => 'analysis.','prefix' => 'analysis'], function () {
				  Route::get('carrecords', ['uses' => 'DashboardController@analysiscarrecords', 'as' => 'carrecords']);
				  Route::get('environmentrecords', ['uses' => 'DashboardController@analysisenvironmentrecords', 'as' => 'envrecords']);
				  Route::get('quartiles', ['uses' => 'DashboardController@quartiles', 'as' => 'quartiles']);
				  
             });
             
              Route::group(['as' => 'reprints.','prefix' => 'reprints'], function () {
			   Route::get('reprints/{fromdate?}/{todate?}', ['uses' => 'DashboardController@reprints', 'as' => 'all' ]);
			   Route::get('reprint/{id}', ['uses' => 'DashboardController@reprint', 'as' => 'single' ]);
			   Route::get('reprint/{id}/{userid}', ['uses' => 'DashboardController@singledates', 'as' => 'singledates' ]);
             
			});
			 Route::group(['as' => 'edits.','prefix' => 'edits'], function () {
			   Route::get('edits/{fromdate?}/{todate?}', ['uses' => 'DashboardController@carrecordsedits', 'as' => 'all' ]);
			   Route::get('edit/{id}', ['uses' => 'DashboardController@edits', 'as' => 'single' ]);
			   Route::get('edit/{id}/{userid}', ['uses' => 'DashboardController@singlecareditdates', 'as' => 'singlecareditdates' ]);
			   Route::get('changes/{id}/{edituserid}/{printdate}/{editedcarid}/{careditid}', ['uses' => 'DashboardController@singlecareditchanges', 'as' => 'singlecareditchanges' ]);
			   
             
			});
    });

    
});
