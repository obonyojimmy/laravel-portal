<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon ;
use Datatables;
use DB ;
use Sentinel ;

use App\User ;
use App\Invoice ;


class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.invoice.newinvoice']))
				{
									$user_id = $user -> id ;
						
						$cal=0;
					  
							
						foreach($request->items as $i => $Item){
							$subtotal = (float)$Item['amount'] * (int)$Item['qty'] ;
							
							$cal +=  (float)$subtotal ;
							
						} 
						$vat = $cal * 0.16 ;
						$totalAmount = $vat +  $cal ;
						
						
						$invoice= Invoice::firstOrCreate([
						   'name' => $request->name ,
						   'email' => $request->email ,
						   'idnumber' => $request->idnumber ,
						   'address' => $request->address ,
						   'status' => 0 ,
						   'phone' => $request -> phone ,
						   'user_id' => $user_id ,
						   'subtotal' => $cal ,
						   'tax' => $vat  ,
						   'total' => $totalAmount  ,
						   'otherinfo' => $request->otherinfo ,
						   'items' => $request->items  ,
						   'paymentdue' =>  Carbon::createFromFormat('m/d/Y', trim($request->paymentdue) )
						   ]);
						   $invoice -> save() ;
						return $invoice ;
			    }else{
				
				   return 0 ;
				}
		}
       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
     public function datatable()
    {
	     if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.invoice.allinvoice']))
				{
					 $allrecords = Invoice::select(['id','name','idnumber','phone','total','created_at']);
       
      
       
       return Datatables::of($allrecords)
        //->editColumn('created_at', '{!! $created_at->diffForHumans() !!}')
       ->addColumn('action', function ($allrecords) {
		   $urlPrint = route('admin.invoice.reprint' ,['id' => $allrecords->id ]);
		   $urlEdit = route('admin.invoice.edit' ,['id' => $allrecords->id ]);
		  
		 return '<div class="btn-group">
					  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Action <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu">
						<li><a href="'.$urlPrint.'">Reprint</a></li>
					  </ul>
					</div> ';
              })
       ->make(true);
			      
			    }else{
				
				   return 0 ;
				}
		}
     
	}
	
	
}
