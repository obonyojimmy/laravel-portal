<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Sentinel ;

use App\User ;
use App\Ship ;
use Carbon\Carbon ;

class ShipController extends Controller
{
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.newship']))
				{
					 
					   
						$user_id = $user -> id ;
						$daterange = explode('-', trim($request->daterange));
						
						$ship= new Ship ;
						  $ship -> name = $request->name ;
						  $ship -> info = $request->info ;
						  $ship -> status = 1 ;
						   $ship -> voyage = $request->voyage ;
						  $ship -> arrival = Carbon::createFromFormat('m/d/Y', trim($daterange[0]) ); 
						  $ship -> departure =  Carbon::createFromFormat('m/d/Y', trim($daterange[1]) ); 
						  $ship -> user_id = $user_id ;
						   $ship -> save() ;
						   $LastInsertId = $ship->id;
						   
						  // return $LastInsertId;
						  return 2 ;
					   
			        
			    }
			    else{
				
				   return 1 ;
				}
		}else{
			 return 0 ;
		}
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Display the single ship resource , using ship name .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function singleship(Request $request)
    {
       if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.inspectedcar']))
				{
					    $q = $request->input('q');
						$res   = Ship::where('name', 'LIKE', "%$q%")->lists('name');
						
					  
						return response()->json($res);
			        
			    }else{
				
				   return 0 ;
				}
		}
       
    }
}
