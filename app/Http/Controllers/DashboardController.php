<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Datatables;
use DB ;
use Carbon\Carbon;
use Sentinel ;
use Route ;


use App\User ;
use App\Car ;
use App\Caredit ;
use App\Editcarinspections ;
use App\Ship ;
use App\PrevCountry ;
use App\DestCountry ;
use App\Environmentalrecord ;
use App\Environmentalinspection ;
use App\Invoice ;
use App\Perm ;
use App\Docprint ;

class DashboardController extends Controller
{
	
    //
     public function index()
    {
        //
         return view('admin/createnewuser');
    }
      public function register()
    {
        
        if ($user = Sentinel::check())
		{
			// User is logged in and assigned to the `$user` variable.
			
             $user_roles = Sentinel::getRoleRepository()->all();
             //~ $routeCollection = Route::getRoutes();
             //~ $permissions = array();
             //~ foreach ($routeCollection as $value) {
        
		     	//~ $permissions[] = $value->getName()  ;
				   
			//~ }
            $permissions = array(
            'Dashboard' => 'admin.dashboard.index' ,
            'Save Ship' => 'admin.newship'  ,
            'Save Car Inspection ' => 'admin.car.carinspection'  ,
            'Save Inspected Car' => 'admin.car.inspectedcar' ,
            'Save Full Car' => 'admin.car.fullcarrecord' ,
            'Table All Cars' => 'admin.car.allcarrecords' ,
            'Reprint Car Record' => 'admin.car.reprint' ,
            'Edit Car Record' => 'admin.car.edit' ,
            'New Environmental Record' => 'admin.environmental.newenvironmentalrecord',
            'All Environmental Records' => 'admin.environmental.allenvironmentalrecords',
            'Reprint Environmental Record' => 'admin.environmental.reprint' ,
            'Edit Environmental Record' => 'admin.environmental.edit' ,
            'New Invoice Record' => 'admin.invoice.newinvoice' ,
            'All Invoice Records' => 'admin.invoice.allinvoice',
            'Reprint Invoice Record' => 'admin.invoice.reprint' ,
            'Analysis Chart Car records' => 'admin.analysis.carrecords',
            'Analysis Chart Environmental records' => 'admin.analysis.envrecords' ,
            'Analysis Quartiles Car  records' => 'admin.analysis.quartiles' ,
            'New User ' => 'admin.users.register' ,
            'Manage User Roles' => 'admin.users.usersroles' ,
            'Manage Single Roles' => 'admin.users.singlerole',
            'New Single Roles' => 'admin.users.newsinglerole',
            'All Users Records' => 'admin.users.listuser',
            'Manage Single User' => 'admin.users.user',
            'Update Single User' => 'api.users.update',
            );
         //return $user_roles ;
            return view('admin.createnewuser',['permissions' =>$permissions,'user_roles' => $user_roles,'user'=> $user  ]);
			
				
		}
       
         return redirect('/');
    }
    
     public function dashboard()
    {
        
       
        if ($user = Sentinel::check())
		{
			// User is logged in and assigned to the `$user` variable.
			  //~ if ($user->hasAccess(['admin.dashboard.index']))
				//~ {
				
					 $carCount =  Car::where('status', 1)->count();
					 $envCount =  Environmentalrecord::where('status', 1)->count();
					 $invCount =  Invoice::count();
					 $userCount =  User::count();
				   // return $user;
					return view('admin.dashboard', ['title' => 'Dashboard ' ,
					 'carCount'=>$carCount ,'envCount'=>$envCount ,'invCount'=>$invCount ,'userCount'=>$userCount ,'user'=> $user ]);	
				
				//~ }else{
					
				   //~ return 0;
				//~ }
			
				
		}
         
        return redirect('/');
        
     
    }
    
    public function listuser()
    {
        //
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.listuser']))
				{
			       $users = User::all();
                   return view('admin.listusers', [ 'title' => 'List Users','users' => $users ,'user' => $user]);
                  
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
     
    }
     public function singleuser($id)
    {
		 if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.user']))
				{
					$singleuser =  Sentinel::findById($id);
					//return $singleuser->roles()->get();
					$users_roles = Sentinel::getRoleRepository()->all();
                   //return $singleuser ;
                   return view('admin.singleuser', [ 'title' => 'Single Users','users_roles'=> $users_roles,'user' => $user,'singleuser' => $singleuser ]);
			      
                  
			    }else{
				
				   return 0 ;
				}
		}
        
         return redirect('/');
       
    }
    
     public function usersroles()
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.usersroles']))
				{
			       $usersroles =   Sentinel::getRoleRepository()->all();
                  //return $usersroles ;
                   return view('admin.usersroles', ['title' => 'List Users Roles','user' => $user ,'usersroles' => $usersroles ]);
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
    }
     public function singlerole($id)
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.singlerole']))
				{
				   $role = Sentinel::findRoleById($id);
			       $rolespermisions = $role->permissions ;
			       $permisions =array();
			       $allpermisions =array();
			       $perms =  Perm::get();
			        foreach($perms as $p){
					  
					   $allperm = array(
					     'name' => $p -> slug ,
					     'permision' => $p -> name ,
					     'status' => false
					   );
					   array_push($allpermisions , $allperm);
					   
					}
			       //~ foreach($rolespermisions as $rolespermision => $stat){
					   //~ $perm =  Perm::where('name', $rolespermision )->first();
					   //~ $key = array_search($perm -> slug ,array_column($permisions, 'name'));
					   //~ unset($permisions[$key]);
					   //~ $newperm = array(
					     //~ 'name' => $perm -> slug ,
					     //~ 'permision' => $rolespermision ,
					     //~ 'status' => $stat 
					   //~ );
					   //~ array_push($permisions , $newperm);
					  //~ // array_prepend($permisions , $newperm);
					   
					//~ }
					foreach($rolespermisions as $rolespermision => $stat){
					   $perm =  Perm::where('name', $rolespermision )->first();
					   //~ $removeperm = array(
					     //~ 'name' => $perm -> slug ,
					     //~ 'permision' => $rolespermision ,
					     //~ 'status' =>false
					   //~ );
					   //~ $result = array_diff($permisions, $removeperm);
					   $newperm = array(
					     'name' => $perm -> slug ,
					     'permision' => $rolespermision ,
					     'status' => $stat 
					   );
					   array_push($permisions , $newperm);
					  // array_prepend($permisions , $newperm);
					   
					}
                // return $permisions ;
                  return view('admin.singlerole', ['title' => 'Manage Permision Role','user' => $user ,'role' => $role->name ,'allpermisions' => $allpermisions ,'permisions' => $permisions ]);
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/'); 
       
    }
    
    public function newsinglerole()
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.newsinglerole']))
				{
			      
                  
                   return view('admin.newsinglerole', ['title' => 'New  Role','user' => $user ]);
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
    }
    
    public function newship()
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.newship']))
				{
			        return view('admin.newship', [ 'title' => "New Ship",'user'=> $user]);
			    }else{
				
				   return 0 ;
				}
		}
       
         return redirect('/');
        
       
    }
    public function carinspection()
    {
       
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.carinspection']))
				{
			         return view('admin.newcarinspection', ['title' => 'New Car Inspection','user'=> $user ]);
			        
			    }else{
				
				   return 0 ;
				}
		}
         
       return redirect('/');   
        
        
    }
    
     public function inspectedcar()
    {
		 if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.inspectedcar']))
				{
					  $prevcountry = PrevCountry::all();
                       $destcountry = DestCountry::all();
       
                       return view('admin.inspected', ['title' => 'New Inspected Car ' ,'prevcountry'=> $prevcountry,'destcountry' => $destcountry,'user'=> $user  ]);
			       
			        
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
       
    }
    
    public function fullcarrecord()
    {
       if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.fullcarrecord']))
				{
				// User is logged in and assigned to the `$user` variable.
					 $prevcountry = PrevCountry::all();
					 $destcountry = DestCountry::all();
				   
					 return view('admin.fullcarrecord', ['title' => 'New Inspected Car ' ,'prevcountry'=> $prevcountry,'destcountry' => $destcountry,'user'=> $user  ]);
				 
				}else{
					
				   return 0 ;
				}
			
				
		}
        
        return redirect('/'); 
    }
    
     public function allcarrecords($fromdate = null , $todate =null)
    {
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.allcarrecords']))
				{
					return view('admin.allcarrecords', ['title' => 'All Car Records ','user'=> $user,'fromdate' => $fromdate , 'todate' => $todate  ]);
			        
			    }else{
				
				   return 0 ;
				}
		}
         
         return redirect('/');
    }
    
     public function carreprint($id)
    {
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.reprint']))
				{
					 $allcardata =Car::find($id);
					         $bk = round(array_sum(array_map('floatval', $allcardata->carinspection -> background ))/count($allcardata->carinspection -> background), 2 );
							 $int =   round(array_sum(array_map('floatval', $allcardata->carinspection -> interior ))/count($allcardata->carinspection -> interior), 2 )  ;
							 $ex =  round(array_sum(array_map('floatval', $allcardata->carinspection -> exterior ))/count($allcardata->carinspection -> exterior), 2 ) ;
							 $eng =   round(array_sum(array_map('floatval', $allcardata->carinspection-> engine ))/count($allcardata->carinspection-> engine), 2 );
							 $extras =   round(array_sum(array_map('floatval', $allcardata->carinspection-> extras ))/count($allcardata->carinspection -> extras), 2 );
							 $quartiles = ($bk + $int + $ex + $eng + $extras ) / 5 ;
                     $allcardata -> quartiles = $quartiles ;
                     return view('admin.reprintcarrecord', ['title' => 'Reprint Car Record','allcardata' => $allcardata ,'user'=> $user]);
					
			        
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
    }
    
     public function caredit($id)
    {
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.edit']))
				{
					 $destcountry = DestCountry::all();
					 $prevcountry = PrevCountry::all();
					 $allcardata =Car::find($id);
				  //  return $allcardata ;
					 return view('admin.editcarrecord', ['prevcountry'=> $prevcountry, 'title' => 'Reprint Environmental Record','destcountry' => $destcountry ,'allcardata' => $allcardata ,'user'=> $user ]);
				   
 
			        
			    }else{
				
				   return 0 ;
				}
		}
       
        return redirect('/'); 
    }
    
    public function newenvironmentalrecord()
    {
        //
             if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.newenvironmentalrecord']))
				{
					  $destcountry = DestCountry::all();
       
                      return view('admin.newenvironmentalrecord', [ 'title' => 'New Enviromental Record ' ,'destcountry' => $destcountry ,'user'=> $user  ]);
					
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
    }
    
    public function allenvironmentalrecords()
    {
        //
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.allenvironmentalrecords']))
				{
					  return view('admin.allenvironmentalrecords', [ 'title' => 'All Enviromental Records ' , 'user'=> $user ]);
                      
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
       
    }
     public function newinvoice()
    {
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.invoice.newinvoice']))
				{
					return view('admin.newinvoice', [ 'title' => 'Generate New Invoice  ' ,'user'=> $user ]);
			      
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
      
    }
    
     public function allinvoice()
    {
          
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.invoice.allinvoice']))
				{
					return view('admin.allinvoice', [ 'title' => 'All Invoice Records ' ,'user'=> $user  ]);
					
			      
			    }else{
				
				   return 0 ;
				}
		}
         
       return redirect('/');
    }
    
     public function invoicereprint($id)
    {
        //
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.invoice.reprint']))
				{
					
					 $allinvdata =Invoice::find($id);
 
                    return view('admin.invoicereprint', ['title' => 'Reprint Invoice ','allinvdata' => $allinvdata ,'user'=> $user  ]);
					
					
			      
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
       
    }
    
     public function invoiceedit($id)
    {
        
         $destcountry = DestCountry::all();
         $allenvdata =Environmentalrecord::find($id);
      //return $id ;
         return view('admin.editinvoice', [ 'title' => 'Reprint Environmental Record','destcountry' => $destcountry ,'allenvdata' => $allenvdata ]);
       
    }
    
     public function environmentalreprint($id)
    {
        //
         
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.reprint']))
				{
					$allenvdata =Environmentalrecord::find($id);
 
                    return view('admin.environmentalreprint', ['title' => 'Reprint Environmental Record','allenvdata' => $allenvdata , 'user'=> $user ]);
					
			    }else{
				
				   return 0 ;
				}
		}
         
       return redirect('/');
    }
    
     public function environmentaledit($id)
    {
        //
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.reprint']))
				{
					 $destcountry = DestCountry::all();
					 $allenvdata =Environmentalrecord::find($id);
				 
					 return view('admin.editenvironmentalrecord', ['title' => 'Reprint Environmental Record','destcountry' => $destcountry ,'allenvdata' => $allenvdata ,'user' => $user]);
				   
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
        
    }
    
    
     public function analysiscarrecords()
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.carrecords']))
				{
					return view('charts.carrecords', ['title' => ' Car Records (30 days)' ,'user'=> $user]);
					
					
			      
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/'); 
       
    }
    
     public function analysisenvironmentrecords()
    {
        //
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.envrecords']))
				{
					
					 return view('charts.envrecords', [ 'title' => 'Environmental Records (30 days)' ,'user'=> $user]);
					
			      
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
       
    }
    
     public function quartiles()
    {
        //
           if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.quartiles']))
				{
					  return view('charts.quartiles', ['title' => 'Analysis Quartiles Car Records' ,'user'=> $user ]);
					
					
			      
			    }else{
				
				   return 0 ;
				}
		}
        
        return redirect('/');
       
    }
    
     public function datatable(Request $request )
    {
	    
         DB::statement(DB::raw('set @rownum=0'));
      $allrecords = Car::join('importers', 'importers.id', '=', 'cars.importer_id')
              ->join('users', 'users.id', '=', 'cars.user_id')
              ->select([
              DB::raw('@rownum  := @rownum  + 1 AS rownum'),
              'cars.id','users.first_name as first_name', 'cars.name','cars.chassisno', 'cars.customsno', 'importers.name as importername','cars.engineno', 'cars.created_at', 'cars.updated_at'])
              ->orderBy('id', 'desc')
              ->take(10) ;
     
       
       return Datatables::of($allrecords)
         
       

       ->addColumn('action', function ($allrecords) {
		   $urlPrint = route('admin.car.reprint' ,['id' => $allrecords->id ]);
		   $urlEdit = route('admin.car.edit' ,['id' => $allrecords->id ]);
		  
		 return '<div class="btn-group">
					  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Action <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu">
						<li><a href="'.$urlPrint.'">Reprint</a></li>
						<li><a href="'.$urlEdit.'">Edit</a></li>
					  </ul>
					</div> ';
               })
       ->make(true);
	}
	
	 public function chartdata()
    {
		$now = Carbon::now() ;

$Dfromdate = $now->startOfMonth()->toDateString(); ;
$Dtodate = $now->endOfMonth()->toDateString(); ;
			
	$chartDatas = Car::select([
    DB::raw('DATE(created_at) AS date'),
    DB::raw('COUNT(id) AS count'),
 ])
  ->whereBetween( DB::raw('date(created_at)'), [$Dfromdate, $Dtodate] )
 // ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
 ->groupBy('date')
 ->orderBy('date', 'ASC')
 ->get()
 ;
   
   $chartDataByDay = array();
   
foreach($chartDatas as $data) {
    $chartDataByDay[$data->date] = $data->count;
}

		$date =  Carbon::now();
		for($i = 0; $i < 29; $i++) {
			$dateString = $date->format('Y-m-d');
			if(!isset($chartDataByDay[ $dateString ])) {
				$chartDataByDay[ $dateString ] = 0;
			}
			$date->subDay();
		}
		$DataDays = array();
		$Datacounts = array();
		foreach($chartDataByDay as $day => $countsdata) {
			$DataDays[]=$day ;
			$Datacounts[]=(int)$countsdata ;
		}
		$ret = array(
		'days' => $DataDays ,
		'counts' => $Datacounts 
		);
	  return response()->json($ret) ;
	}
	
	 public function randomgenerator(Request $request)
    {
		
		switch ($request->quart) {
			case "Quartile1":
				
				$min  = 0.0001 ;
				$max  = 0.0285 ;
				$val1 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val2 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val3 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$ret =  $val1." ".$val2." ".$val3;
				break;
			case "Quartile2":
				$min  = 0.0285 ;
				$max  = 0.0571 ;
				$val1 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val2 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val3 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$ret = $val1." ".$val2." ".$val3;
				break;
			case "Quartile3":
				$min  = 0.0571 ;
				$max  = 0.0856 ;
				$val1 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val2 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val3 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$ret = $val1." ".$val2." ".$val3;
				break;
			case "Quartile4":
				$min  = 0.0856 ;
				$max  = 0.1142 ;
				$val1 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val2 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val3 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$ret = $val1." ".$val2." ".$val3;
				break;
			default:
			    $min  = 0.0001 ;
				$max  = 0.0285 ;
				$val1 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val2 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$val3 =   round(($min + lcg_value()*(abs($max - $min))),2);
				$ret =  $val1." ".$val2." ".$val3;
				
		}

		return response()->json($ret) ;
	  
	}
	
	 public function getdailynotification()
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.dashboard.index']))
				{
				  $status[0] = 0 ;
				  $status[1] = 0 ;
				  $EndofDay   = Carbon::tomorrow();
                  $StartOfDay = Carbon::today();
                  
                  $reprintscars = Docprint::where('reprintstatus', 1)->where('status', 1) ->whereBetween('created_at', [$StartOfDay , $EndofDay])->count();
                  $carrecordsprinted = Car::where('status', 1) ->whereBetween('created_at', [$StartOfDay , $EndofDay])->count();
                  $carrecordeditcars = Docprint::where('reprintstatus', 2)->where('status', 1) ->whereBetween('created_at', [$StartOfDay , $EndofDay])->count();
                  
                  if($reprintscars > 0){
				    $status[0] = 2 ;
				    $status[1]++ ;
				  }
				  if($carrecordsprinted > 0){
				    $status[0] = 2 ;
				    $status[1]++ ;
				  }
				  if($carrecordeditcars > 0){
				    $status[0] = 2 ;
				    $status[1]++ ;
				  }
				  
				  $ret = array(
				   'status' =>  $status[0] ,
				   'reprintscars' =>  $reprintscars ,
				   'carrecordsprinted' => $carrecordsprinted,
				   'carrecordeditcars' => $carrecordeditcars,
				   'countnotification' => $status[1]
				   
				  );
				  return response()->json($ret) ;
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
 
    public function reprints($fromdate = null , $todate =null )
    {
       
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints']))
				{
			         return view('admin.reprints', ['title' => 'All Reprints','user'=> $user ,'fromdate' => $fromdate , 'todate' => $todate   ]);
			        
			    }else{
				
				   return 0 ;
				}
		}
         
       return redirect('/');   
        
        
    }
      public function reprint($id)
    {
        //
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints.single']))
				{
					 $car = Car::where('id', $id)->first();
					 return view('admin.reprint', ['title' => 'Single Reprint','user'=> $user ,'chassisno'=> $car -> chassisno ,'carId' => $id  ]);
					
			      
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
       
    }
    
	 public function singledates($id , $userid)
    {
        //
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints.single']))
				{
					 $car = Car::where('id', $id)->first();
					 $userprint = User::where('id', $userid)->first();
					 $userprintnames = $userprint->first_name.' '.$userprint->last_name ;
					 return view('admin.reprintdates', ['title' => 'Single Reprint','user'=> $user ,'chassisno'=> $car -> chassisno ,'userprintname' => $userprintnames ,'userid' => $userid  ,'carId' => $id ]);
					
			      
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
       
    }
    
     public function carrecordsedits($fromdate = null , $todate =null )
    {
       
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.edits']))
				{
			         return view('datatables.caredits', ['title' => 'All Car Edits','user'=> $user ,'fromdate' => $fromdate , 'todate' => $todate   ]);
			        
			    }else{
				
				   return 0 ;
				}
		}
         
       return redirect('/');   
        
        
    }
    
     public function edits($id)
    {
        //
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.edits.single']))
				{
					 $car = Car::where('id', $id)->first();
					
					 return view('datatables.edits', ['title' => 'Single Edit Users','user'=> $user ,'chassisno'=> $car -> chassisno ,'carId' => $id  ]);
					
			      
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
       
    }
     public function singlecareditdates($id , $userid)
    {
        //
        
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.edits.single']))
				{
					 $car = Car::where('id', $id)->first();
					 $useredit = User::where('id', $userid)->first();
					 $usereditnames = $useredit->first_name.' '.$useredit->last_name ;
					 return view('datatables.careditdates', ['title' => 'Single User Edit Dates','user'=> $user ,'chassisno'=> $car -> chassisno ,'usereditnames' => $usereditnames ,'userid' => $userid  ,'carId' => $id ]);
					
			      
			    }else{
				
				   return 0 ;
				}
		}
         
        return redirect('/');
       
    }
    
     public function singlecareditchanges( $id , $edituserid ,$printdate , $editedcarid ,$careditid )
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.edits.single']))
				{
				 // return 'jimmy';
				 $currentCar = Car::where('id', $id)->first();
				 $beforeEditCar = Caredit::where('id', $careditid)->where('car_id', $id)->where('status', 1)->first();
				 $afterEditCar = Caredit::where('id', $editedcarid)->where('car_id', $id)->where('status', 2)->first();
				 return view('edits.caredits', 
				 ['title' => 'Edit Changes',
				 'user'=> $user ,
				 'editdate' => $printdate ,
				 'currentCar' => $currentCar,
				 'beforeEditCar' => $beforeEditCar ,  
				 'afterEditCar' => $afterEditCar 
				 ]);
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
    
	
}
