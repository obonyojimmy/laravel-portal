<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


//The Facades


use Datatables;
use DB ;
use Carbon\Carbon;
use Sentinel ;
use Activation ;
use Reminder ;

// The Models
use App\User ;
use App\Docprint ;


class UsersController extends Controller
{
	
	public function regis(Request $request)
    {
		    $credentials = [
    'email'    => 'jimmy@jimmycliff.me',
    'password' => '123456',
    'username' => 'jimmy',
    'idnumber' => 12234 ,
    'status' => 1 ,
    'superadmin' => 0 ,
    'first_name' => 'jimmy' ,
    'last_name'  => 'cliff',
    'phone' => '0733452812',
    'info' => 'Briefly about me...'
];

		
		if(Sentinel::validForCreation($credentials)){
			
			$user = Sentinel::create($credentials);
			$activation = Activation::create($user);
			if (Activation::complete($user, $activation -> code))
			{
				// Activation was successfull
				// $role = Sentinel::findRoleByName('Admin');
				 $role = Sentinel::getRoleRepository()->createModel()->create([
						'name' => 'Admin',
						'slug' => 'Super Admin',
					]);
					
					   $role->addPermission('admin.users.usersroles');
						$role->addPermission('admin.users.singlerole');
					   $role->save();
	             $role->users()->attach($user);
	             return  $user ;
			}
			else
			{
				return  1 ;
				// Activation not found or not completed.
			}
			
		}else{
		
		return 0 ;
		
		}
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Sentinel::findById(1);

		if ($user->hasAccess(['api.users.index']))
		{
			// Execute this code if the user has permission
			$users = User::all();
            return $users;
		}
		else
		{
			return 0 ;
			// Execute this code if the permission check failed
		}
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//$perms = explode(',', $request->permission);
		$credentials = [
			'first_name'    => $request->fname,
			'last_name' => $request->lname,
			'email'    => $request->email,
			'password' => $request->password,
			'username'    => $request->username,
			'idnumber' => $request->idnumber,
			'status'    => 1 ,
			'superadmin' => 0 ,
			'info'    => $request->info,
			'phone' => $request->phone
		];
		
		if(Sentinel::validForCreation($credentials)){
			
			$user = Sentinel::create($credentials);
			$activation = Activation::create($user);
			if (Activation::complete($user, $activation -> code))
			{
				// Activation was successfull
				 $role = Sentinel::findRoleByName('Admin');
	             $role->users()->attach($user);
	             return  $user ;
			}
			else
			{
				return  1 ;
				// Activation not found or not completed.
			}
			
		}else{
		
		return 0 ;
		
		}
                
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return  $id ;
	 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['api.users.update']))
				{
					
					$userupdate = Sentinel::findById($id);
                    // $rolesarray = array_map('intval', $request->input('options' ));
                   // $user->roles()->sync($rolesarray);
					$credentials = [
						'first_name' => $request->fname,
						'last_name' => $request->lname,
						'email' => $request->email,
						'username' => $request->username,
						'idnumber' => $request->idnumber,
						'phone' => $request->phone,
						'info' => $request->info,
						'password' => $request->password,
					];
					
				if (Sentinel::validForUpdate($userupdate, $credentials))
				{
					$rolesarray = array_map('intval', $request->roles);
					$updateduser = Sentinel::update($userupdate, $credentials);
					$updateduser->roles()->sync($rolesarray);
					
					return 2 ;
					//updated user
				
				}else{
					
					return 1 ;
					//not valid for update
				
				}
					 
			    }else{
				
				   return 0 ;
				   //no access  to upadate users
				}
		}
                               
								
								                    
                              
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Check login 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        //
       
       $credentials = [
			'email'    => $request -> username ,
			'password' => $request -> password ,
		];

		if ($user = Sentinel::findByCredentials($credentials))
		{
			if (Sentinel::validateCredentials($user, $credentials))
			{
				if (Activation::completed($user))
				{
					Sentinel::login($user);
					return $user ;
				}
				else
				{
					// Activation not found or not completed
					
					return 1 ;
				}
			}else{
				// Password Error
				return 2 ;
			}
			
			
			
			
			
		}
		else
		{
			// User Not found
			return 0 ;
		}
        
    }
    
    public function logout(Request $request)
    {
       Sentinel::logout();
       return redirect('/');
    }
    
    public function checklogin(Request $request)
    {
       if ($user = Sentinel::check())
			{
				
				// User is logged in and assigned to the `$user` variable.
				//return $user ;
				return redirect('admin/dashboard');
			}
			else
			{
				// User is not logged in
				
				return view('auth/login');
				
			}
    }
    
     public function datatable()
    {
	    
      $allrecords = User::
            select(['users.id', 'users.first_name', 'users.phone','users.idnumber','users.status as userstatus', 'users.created_at', 'users.updated_at']);
       
      
       
       return Datatables::of($allrecords)
       ->addColumn('action', function ($allrecords) {
		   $urllinkaction = route('admin.users.user' ,['id' => $allrecords->id ]);
		   
		   return '<a href="'.$urllinkaction.'" class="btn bg-orange">Manage User</a>';
		  
          })
        ->addColumn('userstatus', function ($allrecords) {
		  
		  
		  if($allrecords->userstatus){
		      $userstatuslabel = '<span class="label label-success">Active</span>' ;
		  }else{
			  $userstatuslabel = '<span class="label label-danger">Inactive</span>' ;
		  }
		  
		  return $userstatuslabel ;
          })
       ->make(true);
	}

    public function printcounts7days($id)
    {
		
	if ($user = Sentinel::check())
		{
			 $chartDatas = Docprint::select([
    DB::raw('DATE(created_at) AS date'),
    DB::raw('COUNT(id) AS count'),
 ])
 ->where('reprintstatus', 0)
 ->where('status', 1)
 ->where('user_id', $id)
 ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
 ->groupBy('date')
 ->orderBy('date', 'ASC')
 ->get()
 ;
   //~ 
   $chartDataByDay = array();
   //~ 
foreach($chartDatas as $i => $data) {
    $chartDataByDay[$i]['date'] = Carbon::createFromFormat('Y-m-d', $data->date)->timestamp ;
    $chartDataByDay[$i]['count'] = (int)$data->count ;
    
}
//~ 
		$date =  Carbon::now()->startOfWeek();
		for($i = 0; $i < 7; $i++) {
			$dateString = $date->timestamp;
			if(!isset($chartDataByDay[$i]['date'])) {
				$chartDataByDay[$i]['date'] = $dateString ;
                $chartDataByDay[$i]['count'] = 0 ;
				
				
			}
			
			$date->subDay();
		}
		
		usort($chartDataByDay, function($a1, $a2) {
		   $v1 = $a1['date'] ;
		   $v2 = $a2['date'] ;
		   return $v1 - $v2; // $v2 - $v1 to reverse direction
		});
		
		$DataDays = array();
		$Datacounts = array();
		foreach($chartDataByDay as  $e => $datacount ) {
			$DataDays[]=Carbon::createFromTimestamp($datacount['date'])-> toDateString() ;
			$Datacounts[]=$datacount['count'] ;
		}
		$ret = array(
		'days' => $DataDays ,
		'counts' => $Datacounts 
		);
	  return response()->json($ret) ;
		}else{
			return 0 ;
			}
	
	}
	
	 public function setroles(Request $request)
    {
		if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.newsinglerole']))
				{
			      $role = Sentinel::getRoleRepository()->createModel()->create([
						'name' => $request -> name,
						'slug' => $request -> slug,
					]);
					//~ $role = Sentinel::getRoleRepository()->createModel()->create([
						//~ 'name' => 'Admin',
						//~ 'slug' => 'Super Admin',
					//~ ]);
					return $role ;
			    }else{
				
				   return 0 ;
				}
		}
	   	
	}
	
	public function usersetrole(Request $request)
    {
		   $user = Sentinel::findById(1);
		   $role = Sentinel::findRoleByName('Admin');
		   $role->users()->attach($user);
	      return $user ;
    }
    
    public function usersetpermission(Request $request)
    {
		   $user = Sentinel::findById(46);

			$user->permissions = [
				'admin.dashboard.index' => true ,
			];

			$user->save();
	      return $user ;
    }
     public function useraddpermission(Request $request)
    {
		   $user = Sentinel::findById(46);
           $user->addPermission('admin.users.register');
			
			$user->save();
			return $user ;
			//~ $user = Sentinel::findById(46);

            //~ $activation = Activation::create($user);
            
            //~ if (Activation::complete($user, $activation->code))
			//~ {
				//~ // Activation was successfull
			//	 return $user ;
			//~ }
			//~ else
			//~ {
				//~ // Activation not found or not completed.
				 //~ return 0 ;
			//~ }

	       
    }
    
     public function userremovepermission(Request $request)
    {
		   $user = Sentinel::findById(46);

			$user->removePermission('admin.newship');

			$user->save();
	      return $user ;
    }
    
    public function roleaddpermission(Request $request)
    {
		
		if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.users.singlerole']))
				{
					   if($request->status === 0){
						   $r = false ;
						}else{
							 $r = true ;
						}
					   $role = Sentinel::findRoleByName($request->roleId );
					   $role->addPermission($request->name );
						
					   $role->save();
						return $role ;
						
						//~ $role = Sentinel::findRoleByName('Admin');
					   //~ $role->addPermission('admin.users.usersroles');
						
					   //~ $role->save();
						//~ return $role ;
			    }else{
				
				   return 0 ;
				}
		}
		   //~ 
	        
	       
    }



}
