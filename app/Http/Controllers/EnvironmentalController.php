<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Sentinel ;
use Datatables;
use DB ;
use Carbon\Carbon;

use App\Environmentalrecord ;
use App\Environmentalinspection ;
use App\User ;
use App\Ship ;
use App\Clearingagent ;
use App\Importer ;
use App\Docprint ;



class EnvironmentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.newenvironmentalrecord']))
				{
					 $user_id = $user -> id ;
        
        $ship = Ship::firstOrCreate(['name' => $request->ship ,'user_id' =>$user_id ]);
        $ship -> save() ;
        
        $importer = Importer::firstOrCreate(['name' => $request->inputImporters, 'address' =>   $request->inputAddress]);
        $importer -> save() ;
      //  $importer_id = $importer  -> id ;
        
        $clearingagent = Clearingagent::firstOrCreate(['name' => $request->inputClearingAgent ]);
        $clearingagent -> save() ;
        
        $environmentalrecord= Environmentalrecord::firstOrCreate([
         
           'batchcode' => $request->batchcode ,
           'batchsize' => $request->batchsize ,
           'destcountry_id' => $request->destcountry ,
           'ship_id' => $ship -> id ,
           'user_id' => $user_id ,
           'customsno' => $request->customsno ,
           'clearingagent_id' => $clearingagent -> id ,
           'importer_id' => $importer  -> id  ,
           'port' => $request->port ,
           'status' => 1 ,
           'description' => $request->description ,
           'manufacture' => Carbon::createFromFormat('m/d/Y', trim($request->manufacture) ) ,
           'expiry' =>  Carbon::createFromFormat('m/d/Y', trim($request->expiry) )
           ]);
           $environmentalrecord -> save() ;
           
           //$newenvironmentalrecord = Environmentalrecord::find($environmentalrecord->id);
            $environmentalinspection = new Environmentalinspection([
           
           'user_id' => $user_id ,
           'background' => $request->background  ,
           'netcounts' => $request->netcounts ,
           'info' => $request->info 
           
           ]);
           
	      $newenvironmentalinspection= $environmentalrecord->environmentalinspection()->save($environmentalinspection);
           
           $newenvironmentalrecord = array(
           'env_id' => $environmentalrecord->id ,
             'name' => $environmentalrecord->name ,
             'batchcode' => $environmentalrecord->batchcode ,
             'batchsize' => $environmentalrecord->batchsize ,
             'destcountry' => $environmentalrecord->destcountry ->name , 
             'customsno' => $environmentalrecord->customsno ,
             'clearingagent' => $environmentalrecord-> clearingagent -> name ,
             'importername' => $environmentalrecord-> importer -> name  ,
             'importeraddress' => $environmentalrecord->  importer -> address  ,
             'port' => $environmentalrecord->port ,
             'description' => $environmentalrecord->description ,
             'manufacture' => $environmentalrecord->manufacture->format('d/m/Y') ,
             'expiry' => $environmentalrecord->expiry->format('d/m/Y') ,
             'background' => $environmentalrecord-> environmentalinspection -> background  ,
             'envinfo' => $environmentalrecord-> environmentalinspection -> info  ,
             'ship' => $environmentalrecord -> ship -> name ,
             'netcounts' => $environmentalrecord->environmentalinspection -> netcounts
             
           );
           return response()->json($newenvironmentalrecord); 
			    }else{
				
				   return 0 ;
				}
		}
         
           
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.reprint']))
				{
					  $user_id = $user -> id ;
        
						$ship = Ship::firstOrCreate(['name' => $request->ship ,'user_id' => $user_id  ]);
						$ship -> save() ;
						
						$importer = Importer::firstOrCreate(['name' => $request->inputImporters, 'address' =>   $request->inputAddress]);
						$importer -> save() ;
					  
						
						$clearingagent = Clearingagent::firstOrCreate(['name' => $request->inputClearingAgent ]);
						$clearingagent -> save() ;
						
						  $environmentalrecord = Environmentalrecord::where('id', $id)->first();
						  $environmentalrecord->batchcode = $request->batchcode;
						  $environmentalrecord ->  batchsize = $request->batchsize ;
						  $environmentalrecord ->  destcountry_id = $request->destcountry ;
						  $environmentalrecord ->  ship_id = $ship -> id ;
						   $environmentalrecord -> user_id = $user_id ;
						   $environmentalrecord -> customsno = $request->customsno ;
						   $environmentalrecord -> clearingagent_id = $clearingagent -> id ;
							$environmentalrecord -> importer_id = $importer  -> id  ;
						   $environmentalrecord -> port = $request->port ;
						   $environmentalrecord -> description = $request->description ;
						  $environmentalrecord ->  manufacture = Carbon::createFromFormat('m/d/Y', trim($request->manufacture) ) ;
						   $environmentalrecord -> expiry =  Carbon::createFromFormat('m/d/Y', trim($request->expiry) ) ;
						
						
						  $environmentalrecord -> save() ;
						   
						   
							$environmentalinspection =  Environmentalinspection::firstOrNew([
						   
						   'user_id' => $user_id ,
						   'background' => $request->background  ,
						   'netcounts' => $request->netcounts ,
						   'info' => $request->info 
						   
						   ]);
						   
						  $newenvironmentalinspection= $environmentalrecord->environmentalinspection()->save($environmentalinspection);
						   
						   $newenvironmentalrecord = array(
						   'env_id' => $environmentalrecord->id ,
							 'name' => $environmentalrecord->name ,
							 'batchcode' => $environmentalrecord->batchcode ,
							 'batchsize' => $environmentalrecord->batchsize ,
							 'destcountry' => $environmentalrecord->destcountry ->name , 
							 'customsno' => $environmentalrecord->customsno ,
							 'clearingagent' => $environmentalrecord-> clearingagent -> name ,
							 'importername' => $environmentalrecord-> importer -> name  ,
							 'importeraddress' => $environmentalrecord->  importer -> address  ,
							 'port' => $environmentalrecord->port ,
							 'description' => $environmentalrecord->description ,
							 'manufacture' => $environmentalrecord->manufacture->format('d/m/Y') ,
							 'expiry' => $environmentalrecord->expiry->format('d/m/Y') ,
							 'background' => $environmentalrecord-> environmentalinspection -> background  ,
							 'envinfo' => $environmentalrecord-> environmentalinspection -> info  ,
							 'ship' => $environmentalrecord -> ship -> name ,
							 'netcounts' => $environmentalrecord->environmentalinspection -> netcounts
							 
						   );
						   return response()->json($newenvironmentalrecord); 
			    }else{
				
				   return 0 ;
				}
		}
     
        
       // return "jimmy";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
     /**
     * Update print status of  a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateprint(Request $request)
    {
		
		 if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.newenvironmentalrecord']))
				{
					
			         $user_id = $user -> id ;
        
					  $docprint = new Docprint ;
					  
					  $docprint->user_id = $user_id ;
					  $docprint->environmentalrecord_id = $request->id;
					  $docprint->status = $request->status;
					  
					  $docprint->save();
					
					  return $docprint;
			        
			    }else{
				
				   return 0 ;
				}
		}
		
        
	}
	
	 public function datatable()
    {
		
		 if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.environmental.allenvironmentalrecords']))
				{
					
					if ($user->hasAccess(['admin.environmental.reprint' ,'admin.environmental.edit' ]))
				    {
						$allrecords = Environmentalrecord::join('ships', 'environmentalrecords.ship_id', '=', 'ships.id')
						->join('destcountry', 'destcountry.id', '=', 'environmentalrecords.destcountry_id')
						->join('importers', 'importers.id', '=', 'environmentalrecords.importer_id')
						->select(['environmentalrecords.id', 'environmentalrecords.batchcode', 'environmentalrecords.customsno', 'importers.name as importername','ships.name as shipname', 'destcountry.name as destcountryname','environmentalrecords.created_at', 'environmentalrecords.updated_at']);
				   
				  
				   
						   return Datatables::of($allrecords)
						   ->addColumn('action', function ($allrecords) {
							   $urlPrint = route('admin.environmental.reprint' ,['id' => $allrecords->id ]);
							   $urlEdit = route('admin.environmental.edit' ,['id' => $allrecords->id ]);
							  
						   return '<div class="btn-group">
										  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Action <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu">
											<li><a href="'.$urlPrint.'">Reprint</a></li>
											<li><a href="'.$urlEdit.'">Edit</a></li>
										  </ul>
										</div> ';
								  //  return '<a href="#edit-'.$allrecords->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
								})
						   ->make(true);
					}
					
					if ($user->hasAccess(['admin.environmental.reprint']))
				    {
						$allrecords = Environmentalrecord::join('ships', 'environmentalrecords.ship_id', '=', 'ships.id')
						->join('destcountry', 'destcountry.id', '=', 'environmentalrecords.destcountry_id')
						->join('importers', 'importers.id', '=', 'environmentalrecords.importer_id')
						->select(['environmentalrecords.id', 'environmentalrecords.batchcode', 'environmentalrecords.customsno', 'importers.name as importername','ships.name as shipname', 'destcountry.name as destcountryname','environmentalrecords.created_at', 'environmentalrecords.updated_at']);
				   
				  
				   
						   return Datatables::of($allrecords)
						   ->addColumn('action', function ($allrecords) {
							   $urlPrint = route('admin.environmental.reprint' ,['id' => $allrecords->id ]);
							  
							  
						   return '<div class="btn-group">
										  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Action <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu">
											<li><a href="'.$urlPrint.'">Reprint</a></li>
											
										  </ul>
										</div> ';
								  //  return '<a href="#edit-'.$allrecords->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
								})
						   ->make(true);
					}
					
			        if ($user->hasAccess(['admin.environmental.reprint' ,'admin.environmental.edit' ]))
				    {
						$allrecords = Environmentalrecord::join('ships', 'environmentalrecords.ship_id', '=', 'ships.id')
						->join('destcountry', 'destcountry.id', '=', 'environmentalrecords.destcountry_id')
						->join('importers', 'importers.id', '=', 'environmentalrecords.importer_id')
						->select(['environmentalrecords.id', 'environmentalrecords.batchcode', 'environmentalrecords.customsno', 'importers.name as importername','ships.name as shipname', 'destcountry.name as destcountryname','environmentalrecords.created_at', 'environmentalrecords.updated_at']);
				   
				  
				   
						   return Datatables::of($allrecords)
						   ->addColumn('action', function ($allrecords) {
							  
							   $urlEdit = route('admin.environmental.edit' ,['id' => $allrecords->id ]);
							  
						   return '<div class="btn-group">
										  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Action <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu">
											
											<li><a href="'.$urlEdit.'">Edit</a></li>
										  </ul>
										</div> ';
								  //  return '<a href="#edit-'.$allrecords->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
								})
						   ->make(true);
					}
					
			        
			    }else{
				
				   return 0 ;
				}
		}
	    
      
	}
	
	 public function chartdata()
    {
		if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.envrecords']))
				{
					
					$chartDatas = Environmentalrecord::select([
						DB::raw('DATE(created_at) AS date'),
						DB::raw('COUNT(id) AS count'),
					 ])
					 ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
					 ->groupBy('date')
					 ->orderBy('date', 'ASC')
					 ->get()
					 ;
					   
					   $chartDataByDay = array();
   
						foreach($chartDatas as $data) {
							$chartDataByDay[$data->date] = $data->count;
						}

						$date =  Carbon::now();
						for($i = 0; $i < 30; $i++) {
							$dateString = $date->format('Y-m-d');
							if(!isset($chartDataByDay[ $dateString ])) {
								$chartDataByDay[ $dateString ] = 0;
							}
							$date->subDay();
						}
						$DataDays = array();
						$Datacounts = array();
						foreach($chartDataByDay as $day => $countsdata) {
							$DataDays[]=$day ;
							$Datacounts[]=(int)$countsdata ;
						}
						$ret = array(
						'days' => $DataDays ,
						'counts' => $Datacounts 
						);
					  return response()->json($ret) ;
			      
			    }else{
				
				   return 0 ;
				}
		}
			
	
	}

    



}
