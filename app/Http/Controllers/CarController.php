<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Sentinel ;
use Datatables;
use DB ;
use Carbon\Carbon;

use App\Car ;
use App\Caredit ;
use App\Editcarinspections ;
use App\Clearingagent ;
use App\Importer ;
use App\Ship ;
use App\Carinspection ;
use App\User ;
use App\PrevCountry ;
use App\Docprint ;

class CarController extends Controller
{
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.carinspection']))
				{
			           $user_id = $user -> id ;
						
					   
						$background = explode(' ', trim($request->background));
						$exterior = explode(' ', trim($request->exterior));
						$engine = explode(' ', trim($request->engine));
						$interior = explode(' ', trim($request->interior));
						$extras = explode(' ', trim($request->extras));
						   
						   $car= new Car ;
						   $car -> chassisno = $request->chassisno ;
						   $car -> status = 0 ;
						   $car -> save() ;
						   
						   $carmodel = Car::find($car->id);
						   
						   $carinspections = new Carinspection([
						   
						   'user_id' => $user_id ,
						   'background' => $background ,
						   'exterior' => $exterior ,
						   'engine' => $engine,
						   'interior' => $interior ,
						   'extras' => $extras ,
						   'info' => $request->info 
						   
						   ]);
						   
						  $newcar= $carmodel->carinspection()->save($carinspections);
						   
						   return $newcar ;
			    }else{
				
				   return 1;
				}
		}else{
			 return 0 ;
		}
        
       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return $id ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.edit']))
				{
					  
        $user_id = $user -> id ;
        //~ $findoldcarrecord = Car::find($id);
        //~ $findoldcarrecord -> status =  1;
        //~ $oldcarrecord = $findoldcarrecord->replicate(); 
        //~ $oldcarrecord->push();
       
        $background = explode(' ', trim($request->background));
        $exterior = explode(' ', trim($request->exterior));
        $engine = explode(' ', trim($request->engine));
        $interior = explode(' ', trim($request->interior));
        $extras = explode(' ', trim($request->extras));
         
         
        $ship = Ship::firstOrCreate(['name' => $request->ship,'user_id' =>$user_id ]);
        $ship -> save() ;
        
        $importer = Importer::firstOrCreate(['name' => $request->inputImporters, 'address' =>   $request->inputAddress]);
        $importer -> save() ;
        $importer_id = $importer  -> id ;
        
        $clearingagent = Clearingagent::firstOrCreate(['name' => $request->inputClearingAgent ]);
        $clearingagent -> save() ;
        
           $car = Car::where('id', $id)->first();
          
            $clone =  $car->replicate();
			$clone_inspection =  $car->carinspection->replicate();
		
			unset($clone['id'],$clone['user_id'],$clone['status'],$clone['created_at'],$clone['updated_at']);
			unset($clone_inspection['user_id'] ,$clone_inspection['status'],$clone_inspection['id'],$clone_inspection['car_id'],$clone_inspection['created_at'],$clone_inspection['updated_at']);
			
			$dataclone = json_decode($clone, true);
			$dataclone_inpections = json_decode($clone_inspection, true);
			$dataclone['user_id'] = $user_id ;
			$dataclone['car_id'] = $id ;
		    $dataclone['status'] = 1  ; 
		    $clonedcar = Caredit::create($dataclone);
		    
			$dataclone_inpections['caredit_id'] = $clonedcar -> id ;
			$dataclone_inpections['user_id'] = $user_id ;
		    $dataclone_inpections['status'] = 1  ; 
			$clonedcar_inspection = Editcarinspections::create($dataclone_inpections);
          
           $car->name = $request->inputCarMake ;
            $car->chassisno = $request->chassisno ;
            $car->customsno = $request->inputCustoms ;
            $car->colour = $request->inputColour ;
            $car->engineno = $request->inputEngineNumber ;
            $car->year = $request->inputManYear ;
            $car->fuel = $request->inputFuel ;
            $car->prevcountry_id = $request->inputPrevRegCountry ;
            $car->previousreg = $request->inputPreviousReg ;
            $car->destcountry_id = $request->inputDestination ;
           // $car->user_id = $user_id ;
            $car->clearingagent_id = $clearingagent -> id ;
            $car->ship_id = $ship -> id ;
            $car->importer_id = $importer_id ;
           
           
          
           $car -> save() ;
           
           
           // $carmodel = Car::find($car->id);
            //~ //$carmodel = Car::find(8);
           $carinspections =Carinspection::firstOrNew([
           
           //'user_id' => $user_id ,
           'background' => json_encode($background)  ,
           'exterior' =>  json_encode($exterior)  ,
           'engine' =>  json_encode($engine) ,
           'interior' =>  json_encode($interior)  ,
           'extras' =>  json_encode($extras)  ,
           'info' => $request->info 
           
           ]);
           
	      $newcarinspection= $car->carinspection()->save($carinspections);
	      
	       
	        
	        $Editedclonecar =  $car->replicate();
	       
			$Editedclone_inspection = $newcarinspection;
		
			unset($Editedclonecar['carinspection'] , $Editedclonecar['id'],$Editedclonecar['user_id'],$Editedclonecar['status'],$Editedclonecar['created_at'],$Editedclonecar['updated_at']);
			unset($Editedclone_inspection['user_id'] ,$Editedclone_inspection['status'],$Editedclone_inspection['id'],$Editedclone_inspection['car_id'],$Editedclone_inspection['created_at'],$Editedclone_inspection['updated_at']);
			
			$Edited_dataclone_car = json_decode($Editedclonecar, true);
			$Edited_dataclone_car_inpections = json_decode($Editedclone_inspection, true);
			$Edited_dataclone_car['user_id'] = $user_id ;
			$Edited_dataclone_car['car_id'] = $id ;
			$Edited_dataclone_car['caredit_id'] = $clonedcar -> id ;
		    $Edited_dataclone_car['status'] = 2  ; 
		    $Edited_Clonedcar = Caredit::create($Edited_dataclone_car);
		    //~ 
			$Edited_dataclone_car_inpections['caredit_id'] = $Edited_Clonedcar -> id ;
			$Edited_dataclone_car_inpections['user_id'] = $user_id ;
		    $Edited_dataclone_car_inpections['status'] = 2  ; 
			$Edited_Clonedcar_inspection = Editcarinspections::create($Edited_dataclone_car_inpections);
	      
	      
	      
	      
	       $bk = round(array_sum(array_map('floatval', $newcarinspection -> background ))/count($newcarinspection -> background), 2 );
							 $int =   round(array_sum(array_map('floatval', $newcarinspection -> interior ))/count($newcarinspection -> interior), 2 )  ;
							 $ex =  round(array_sum(array_map('floatval', $newcarinspection -> exterior ))/count($newcarinspection -> exterior), 2 ) ;
							 $eng =   round(array_sum(array_map('floatval', $newcarinspection-> engine ))/count($newcarinspection-> engine), 2 );
							 $extras =   round(array_sum(array_map('floatval', $newcarinspection-> extras ))/count($newcarinspection -> extras), 2 );
							 $quartiles = ($bk + $int + $ex + $eng + $extras ) / 5 ;
	      
	      $newCar = array(
	       'carid' => $car->id ,
	        'name' => $car->name ,
            'chassisno' => $car->chassisno ,
            'customsno' => $car->customsno  ,
           'colour' => $car->colour ,
           'engineno' => $car->engineno ,
           'year' => $car->year ,
           'fuel' => $car->fuel ,
           'sticker' => "N/a" ,
         'prevcountry' => $car->prevcountry -> name ,
           'previousreg' => $car->previousreg ,
           'destcountry' => $car->destcountry ->name ,
           'user' => $car -> user -> name ,
           'clearingagent' => $car -> clearingagent -> name ,
           'ship' => $car -> ship -> name ,
           'importer_name' => $car -> importer -> name ,
           'importer_address' => $car -> importer -> address ,
            'quartiles' => $quartiles ,
           'carinspections' => $newcarinspection ,
           'caredit_id' => $clonedcar -> id , // Car id for car just before edit
           'edited_caredit_id' => $Edited_Clonedcar -> id  , // Car id for car just after edit
           // 'clone' => $clone ,
           //  'Edited_dataclone_car_inpections' => $Edited_dataclone_car_inpections 
	      );
	      
	       //~ // return response()->json($newcar);
	       return $newCar ;
                    
			        
			    }else{
				
				   return 0 ;
				}
		}
        
        
        
        
       
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     /**
     * Display the Chasis numbers resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function chasisno(Request $request)
    {
        //
        if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.inspectedcar']))
				{
					  
                     $q = $request->input('q');
                     $res   = Car::where('chassisno', 'LIKE', "%$q%")->where('status', 0)->lists('chassisno');
			           return response()->json($res);
			    }else{
				
				   return '' ;
				}
		}
        
        
      
      return response()->json($res);
    }
    
       public function searchchasisno()
    {
        //
      if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.inspectedcar']))
				{
					  
                     $res   = Car::where('status', 0)->lists('chassisno');
                     return response()->json($res);
			        
			    }else{
				
				   return '' ;
				}
		}
      
        
      
      
    }
    /**
     * Display the single car resource , using chasis number .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function singlecar(Request $request)
    {
        //
        $q = $request->input('chasisno');
        $res   = Car::where('chassisno', $q)->where('status', 0)->with('carinspection')->first();
        
      
      return response()->json($res);
    }
    /**
     * Display the single car resource , using chasis number .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function inspecatedcarsave(Request $request)
    {
        //
       if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.inspectedcar']))
				{
					   $user_id = $user -> id ;
        
						$ship = Ship::firstOrCreate(['name' => $request->ship,'user_id' =>$user_id ]);
						$ship -> save() ;
						
						$importer = Importer::firstOrCreate(['name' => $request->inputImporters, 'address' =>   $request->inputAddress]);
						$importer -> save() ;
						$importer_id = $importer  -> id ;
						
						$clearingagent = Clearingagent::firstOrCreate(['name' => $request->inputClearingAgent ]);
						$clearingagent -> save() ;
						
						   $car= Car::where('chassisno', $request->chassisno)->first();
						   
						   //$car -> chassisno = $request->chassisno ;
						   $car -> customsno = $request->inputCustoms ;
						   $car -> name = $request->inputCarMake ;
						   $car -> colour = $request->inputColour ;
						   $car -> engineno = $request->inputEngineNumber ;
						   $car -> year = $request->inputManYear ;
						   $car -> fuel = $request->inputFuel ;
						   $car -> prevcountry_id = $request->inputPrevRegCountry ;
						   $car -> previousreg = $request->inputPreviousReg ;
						   $car -> destcountry_id = $request->inputDestination ;
						   $car -> user_id =  $user_id;
						   $car -> clearingagent_id =  $clearingagent -> id;
						   $car -> ship_id =  $ship -> id;
						   $car -> status =  1;
						   $car -> importer_id =  $importer_id;
						   $car -> save() ;
						   
						
						  $newcarinspection= $car->carinspection;
						  
							 $bk = round(array_sum(array_map('floatval', $newcarinspection -> background ))/count($newcarinspection -> background), 2 );
							 $int =   round(array_sum(array_map('floatval', $newcarinspection -> interior ))/count($newcarinspection -> interior), 2 )  ;
							 $ex =  round(array_sum(array_map('floatval', $newcarinspection -> exterior ))/count($newcarinspection -> exterior), 2 ) ;
							 $eng =   round(array_sum(array_map('floatval', $newcarinspection-> engine ))/count($newcarinspection-> engine), 2 );
							 $extras =   round(array_sum(array_map('floatval', $newcarinspection-> extras ))/count($newcarinspection -> extras), 2 );
							 $quartiles = ($bk + $int + $ex + $eng + $extras ) / 5 ;
							 
						  
						  
						  
						  
						$newCar = array(
						   'carid' => $car->id ,
							'name' => $car->name ,
							'chassisno' => $car->chassisno ,
							'customsno' => $car->customsno  ,
						   'colour' => $car->colour ,
						   'engineno' => $car->engineno ,
						   'year' => $car->year ,
						   'fuel' => $car->fuel ,
						   'sticker' => "N/a" ,
						 'prevcountry' => $car->prevcountry -> name ,
						   'previousreg' => $car->previousreg ,
						   'destcountry' => $car->destcountry ->name ,
						   'user' => $car -> user -> name ,
						   'clearingagent' => $car -> clearingagent -> name ,
						   'ship' => $car -> ship -> name ,
						   'importer_name' => $car -> importer -> name ,
						   'importer_address' => $car -> importer -> address ,
						   'carinspections' => $newcarinspection ,
						   'quartiles' => $quartiles 
						   
						  );
						return $newCar;
       
			        
			    }else{
				
				   return 0 ;
				}
		}
        
    }
    
    /**
     * Display the Car make  resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function carmakesearch(Request $request)
    {
        //
        $q = $request->input('q');
        $res   = Car::where('name', 'LIKE', "%$q%")->lists('name');
        
      
      return response()->json($res);
    }
    
    /**
     * Display the Car colours  resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function carcolours(Request $request)
    {
        //
        $q = $request->input('q');
        $res   = Car::where('colour', 'LIKE', "%$q%")->lists('colour');
        
      
      return response()->json($res);
    }
    /**
     * Display the Clearing Agents  resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function clearingagents(Request $request)
    {
        //
        $q = $request->input('q');
        $res   = Clearingagent::where('name', 'LIKE', "%$q%")->lists('name');
        
      
      return response()->json($res);
    }
    
     
     /**
     * Display the Importers name  resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function importersnames(Request $request)
    {
        //
        $q = $request->input('q');
        $res   = Importer::where('name', 'LIKE', "%$q%")->lists('name');
        
      
      return response()->json($res);
    }
    
     /**
     * Display the Importers Address  resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function importersaddress(Request $request)
    {
        //
        $q = $request->input('q');
        $res   = Importer::where('address', 'LIKE', "%$q%")->lists('address');
        
      
      return response()->json($res);
    }
    
     /**
     * Store a full car record  resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fullcarsave(Request $request)
    {
        //
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.fullcarrecord']))
				{
				// User is logged in and assigned to the `$user` variable.
				$user_id = $user -> id ;
				
			   
				$background = explode(' ', trim($request->background));
				$exterior = explode(' ', trim($request->exterior));
				$engine = explode(' ', trim($request->engine));
				$interior = explode(' ', trim($request->interior));
				$extras = explode(' ', trim($request->extras));
				 
				 
				$ship = Ship::firstOrCreate(['name' => $request->ship,'user_id' =>$user_id ]);
				$ship -> save() ;
				
				$importer = Importer::firstOrCreate(['name' => $request->inputImporters, 'address' =>   $request->inputAddress]);
				$importer -> save() ;
				$importer_id = $importer  -> id ;
				
				$clearingagent = Clearingagent::firstOrCreate(['name' => $request->inputClearingAgent ]);
				$clearingagent -> save() ;
				
			   if (Car::where('chassisno', '=', $request->chassisno)->exists()) {
				   // user found
				   $car = Car::where('chassisno', $request->chassisno)->first();
				   
				   $car -> customsno = $request->inputCustoms ;
				   $car -> name = $request->inputCarMake ;
				   $car -> colour = $request->inputColour ;
				   $car -> engineno = $request->inputEngineNumber ;
				   $car -> year = $request->inputManYear ;
				   $car -> fuel = $request->inputFuel ;
				   $car -> prevcountry_id = $request->inputPrevRegCountry ;
				   $car -> previousreg = $request->inputPreviousReg ;
				   $car -> destcountry_id = $request->inputDestination ;
				   $car -> user_id =  $user_id;
				   $car -> clearingagent_id =  $clearingagent -> id;
				   $car -> ship_id =  $ship -> id;
				   $car -> status =  1;
				   $car -> importer_id =  $importer_id;
				   $car -> save() ;
					$carmodel = Car::find($car->id);
				   $newcarinspection= $car->carinspection->first();
				   
				             $bk = round(array_sum(array_map('floatval', $newcarinspection -> background ))/count($newcarinspection -> background), 2 );
							 $int =   round(array_sum(array_map('floatval', $newcarinspection -> interior ))/count($newcarinspection -> interior), 2 )  ;
							 $ex =  round(array_sum(array_map('floatval', $newcarinspection -> exterior ))/count($newcarinspection -> exterior), 2 ) ;
							 $eng =   round(array_sum(array_map('floatval', $newcarinspection-> engine ))/count($newcarinspection-> engine), 2 );
							 $extras =   round(array_sum(array_map('floatval', $newcarinspection-> extras ))/count($newcarinspection -> extras), 2 );
							 $quartiles = ($bk + $int + $ex + $eng + $extras ) / 5 ;
				   
				}else{
					 $car= Car::firstOrCreate([
				   'name' => $request->inputCarMake ,
				   'chassisno' => $request->chassisno ,
				   'customsno' => $request->inputCustoms ,
				   'colour' => $request->inputColour ,
				   'engineno' => $request->inputEngineNumber ,
				   'year' => $request->inputManYear ,
				   'fuel' => $request->inputFuel ,
				   'prevcountry_id' => $request->inputPrevRegCountry ,
				   'previousreg' => $request->inputPreviousReg ,
				   'destcountry_id' => $request->inputDestination ,
				   'user_id' => $user_id ,
				   'status' => 1 ,
				   'clearingagent_id' => $clearingagent -> id ,
				   'ship_id' => $ship -> id ,
				   'importer_id' => $importer_id
				   
				   
				   ]);
				   $car -> save() ;
				   
				   
					$carmodel = Car::find($car->id);
				  
				   $newcarinspection = Carinspection::create([
				   'car_id' => $carmodel -> id ,
				   'user_id' => $user_id ,
				   'background' => $background ,
				   'exterior' => $exterior ,
				   'engine' => $engine,
				   'interior' => $interior ,
				   'extras' => $extras ,
				   'status' => 1,
				   'info' => $request->info 
				   
				   ]);
				   
				  //$newcarinspection= $carinspections->save();
				 // $newcarinspection= Carinspection::find($nci->id);
				 
				             $bk = round(array_sum(array_map('floatval', $newcarinspection -> background ))/count($newcarinspection -> background), 2 );
							 $int =   round(array_sum(array_map('floatval', $newcarinspection -> interior ))/count($newcarinspection -> interior), 2 )  ;
							 $ex =  round(array_sum(array_map('floatval', $newcarinspection -> exterior ))/count($newcarinspection -> exterior), 2 ) ;
							 $eng =   round(array_sum(array_map('floatval', $newcarinspection-> engine ))/count($newcarinspection-> engine), 2 );
							 $extras =   round(array_sum(array_map('floatval', $newcarinspection-> extras ))/count($newcarinspection -> extras), 2 );
							 $quartiles = ($bk + $int + $ex + $eng + $extras ) / 5 ;
					
				}
				  
				  
				  $newCar = array(
				   'carid' => $carmodel->id ,
					'name' => $carmodel->name ,
					'chassisno' => $carmodel->chassisno ,
					'customsno' => $carmodel->customsno  ,
				   'colour' => $carmodel->colour ,
				   'engineno' => $carmodel->engineno ,
				   'year' => $carmodel->year ,
				   'fuel' => $carmodel->fuel ,
				   'sticker' => "N/a" ,
				 'prevcountry' => $carmodel->prevcountry -> name ,
				   'previousreg' => $carmodel->previousreg ,
				   'destcountry' => $carmodel->destcountry ->name ,
				   'user' => $carmodel -> user -> name ,
				   'clearingagent' => $carmodel -> clearingagent -> name ,
				   'ship' => $carmodel -> ship -> name ,
				   'importer_name' => $carmodel -> importer -> name ,
				   'importer_address' => $carmodel -> importer -> address ,
				    'quartiles' => $quartiles ,
				   'carinspections' => $newcarinspection 
				  );
				  
				   // return response()->json($newcar);
				   return $newCar ;
					
				}else{
					
				   return 1;
				}
			
				
		}else{
			 return 0 ;
		}
        
        
       
        
    }
     /**
     * Update print status of  a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateprint(Request $request)
    {
		if ($user = Sentinel::check())
		{
			if ($user->hasAccess(['admin.car.reprint']))
				{
        $user_id = $user -> id ;
        
          $docprint = new Docprint ;
          
          $docprint->user_id = $user_id ;
          $docprint->car_id =  $request->id ;
          $docprint->status =  $request->status ;
          $docprint->reprintstatus = ($request->has('reprintstatus') ? $request->reprintstatus : null ) ;
          $docprint->caredit_id = ($request->has('caredit_id') ? $request->caredit_id : null ) ; 
          $docprint->editedcar_id = ($request->has('edited_caredit_id') ? $request->edited_caredit_id : null ) ; 
          $docprint->save();
        
		return $docprint;
		  }else{
			 return 1 ;
		}
	  }else{
			 return 0 ;
		}
	}
	 public function datatable(Request $request , $fromdate = null , $todate =null)
    {
		
		 if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.car.allcarrecords']))
				{
					
					DB::statement(DB::raw('set @rownum=0'));
						
							  $allrecords = Car::join('importers', 'importers.id', '=', 'cars.importer_id')
									->join('users', 'users.id', '=', 'cars.user_id')
									->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									'cars.id', 'users.first_name as username' ,'cars.name','cars.chassisno', 'cars.customsno', 'importers.name as importername','cars.engineno', 'cars.created_at', 'cars.updated_at']);
							   
							  
							   
							   return Datatables::of($allrecords)
							   
							   ->filter(function ($query) use ($fromdate , $todate , $request ) {
								   
									   if($fromdate && $todate){
									   
											$fromdate = Carbon::parse($fromdate) ;
											$todate= Carbon::parse($todate) ;
											$query->whereBetween('cars.created_at', [$fromdate, $todate]);
										} 
										
										  if($request->has('fromdate') && $request->has('todate')){
									   
											$fromdate = Carbon::parse($request->fromdate) ;
											$todate= Carbon::parse($request->todate) ;
											$query->whereBetween('cars.created_at', [$fromdate, $todate]);
										} 
									
								   

									
								})
									
							   ->addColumn('action', function ($allrecords ) use ($user) {
								   $urls= '' ;
								   $uPrint = route('admin.car.reprint' ,['id' => $allrecords->id ]) ;
								   $uEdit =  route('admin.car.edit' ,['id' => $allrecords->id ]) ;
								   
								   $urlPrint = '<li><a href="'.$uPrint.'">Reprint</a></li>';
								   $urlEdit =  '<li><a href="'.$uEdit.'">Edit</a></li>';
								   
								    //~ if ($user->hasAccess(['admin.car.edit','admin.car.reprint'])){
										 //~ $urls .= $urlPrint ;
								         //~ $urls .= $urlEdit ;
									//~ }
									
									
									
									if ($user->hasAccess(['admin.car.reprint'])){
										
								         $urls .= $urlPrint ;
									}
									
									if ($user->hasAccess(['admin.car.edit'])){
										
								         $urls .= $urlEdit ;
									}
								  
								  
								 return '<div class="btn-group">
											  <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Action <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu">
												'.$urls.'
												
											  </ul>
											</div> ';
									   })
							   ->make(true);
					   
					
			        
			    }else{
				
				   return 0 ;
				}
		}
		
	}
	
	 public function chartdata()
    {
		  if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.carrecords']))
				{
							
					$chartDatas = Car::select([
					DB::raw('DATE(created_at) AS date'),
					DB::raw('COUNT(id) AS count'),
				 ])
				 ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
				 ->groupBy('date')
				 ->orderBy('date', 'ASC')
				 ->get()
				 ;
				   
				   $chartDataByDay = array();
				   
				foreach($chartDatas as $data) {
					$chartDataByDay[$data->date] = $data->count;
				}

						$date =  Carbon::now();
						for($i = 0; $i < 30; $i++) {
							$dateString = $date->format('Y-m-d');
							if(!isset($chartDataByDay[ $dateString ])) {
								$chartDataByDay[ $dateString ] = 0;
							}
							$date->subDay();
						}
						$DataDays = array();
						$Datacounts = array();
						foreach($chartDataByDay as $day => $countsdata) {
							$DataDays[]=$day ;
							$Datacounts[]=(int)$countsdata ;
						}
						$ret = array(
						'days' => $DataDays ,
						'counts' => $Datacounts 
						);
					  return response()->json($ret) ;
			      
			    }else{
				
				   return 0 ;
				}
		}
	
	}
	
	 public function quartilespiechartdata(Request $request)
    {
		  if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.quartiles']))
				{
					  
		
					   $cars = Car::where('status', 1)->with('Carinspection')->get();
					   $quartiles = array();
					   foreach($cars as $c => $car) {
							  $bk = round(array_sum(array_map('floatval', $car -> carinspection -> background ))/count($car -> carinspection -> background), 5 );
							// $carinspections['bk'] =  array_map('floatval', $car -> carinspection -> background );  
							 $int =   round(array_sum(array_map('floatval', $car -> carinspection -> interior ))/count($car -> carinspection -> interior), 5 )  ;
							 $ex =  round(array_sum(array_map('floatval', $car -> carinspection -> exterior ))/count($car -> carinspection -> exterior), 5 ) ;
							 $eng =   round(array_sum(array_map('floatval', $car -> carinspection -> engine ))/count($car -> carinspection -> engine), 5 );
							 $extras =   round(array_sum(array_map('floatval', $car -> carinspection -> extras ))/count($car -> carinspection -> extras), 5 );
							 $quartiles[] = ($bk + $int + $ex + $eng + $extras ) / 4 ;
							 
							 
						}
		

				$range1 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0001 && $var <= 0.0285;')));
				$range2 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0285 && $var <= 0.0571;')));
				$range3 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0571 && $var <= 0.0856;')));
				$range4 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0856 && $var <= 0.1142;')));
				$range5 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.1142 ;')));


				  
				   $output = array
				  (
				  "labels" => array("0.0001 - 0.0285" , "0.0285 - 0.0571" ,"0.0571 - 0.0856" ,"0.0856 - 0.1142" ,"0.1142 +") ,
				  "datasets" => array(
									  array(
									   "data" => array($range1,$range2,$range3,$range4,$range5) ,
									   "backgroundColor" => array("rgb(0, 0, 255)" ,"rgb(0, 255, 255)" ,"rgb(255, 255, 26)" ,"rgb(255, 128, 0)" ,"rgb(255, 0, 0)" ) ,
									   "hoverBackgroundColor" => array("rgb(0, 64, 255)" , "rgb(128, 255, 255)" , "rgb(255, 255, 77)" ,"rgb(255, 64, 0)" ,"rgb(204, 0, 0)")
									 ) 
									 ) 
				  
				  );
						
					 // return response()->json($output) ;
					  return $output ;
			      
			    }else{
				
				   return 0 ;
				}
		}
	  
	}
	
	 public function daterangequartilespiechart(Request $request)
    {
		 if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.analysis.quartiles']))
				{
					  $fromdate = Carbon::parse($request->frodate) ;
		              $todate= Carbon::parse($request->todate) ;
		
				   $cars = Car::where('status', 1) ->whereBetween('created_at', [$fromdate, $todate])->with('carinspection')->get();
				   $quartiles = array();
				   foreach($cars as $c => $car) {
						  $bk = round(array_sum(array_map('floatval', $car -> carinspection -> background ))/count($car -> carinspection -> background), 5 );
						// $carinspections['bk'] =  array_map('floatval', $car -> carinspection -> background );  
						 $int =   round(array_sum(array_map('floatval', $car -> carinspection -> interior ))/count($car -> carinspection -> interior), 5 )  ;
						 $ex =  round(array_sum(array_map('floatval', $car -> carinspection -> exterior ))/count($car -> carinspection -> exterior), 5 ) ;
						 $eng =   round(array_sum(array_map('floatval', $car -> carinspection -> engine ))/count($car -> carinspection -> engine), 5 );
						 $extras =   round(array_sum(array_map('floatval', $car -> carinspection -> extras ))/count($car -> carinspection -> extras), 5 );
						 $quartiles[] = ($bk + $int + $ex + $eng + $extras ) / 4 ;
						 
						 
					}
		
					
					$range1 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0001 && $var <= 0.0285;')));
					$range2 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0285 && $var <= 0.0571;')));
					$range3 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0571 && $var <= 0.0856;')));
					$range4 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.0856 && $var <= 0.1142;')));
					$range5 = count(array_filter($quartiles, create_function('$var', 'return $var >= 0.1142 ;')));

					
					  
					   $output = array
					  (
					  "labels" => array("0.0001 - 0.0285" , "0.0285 - 0.0571" ,"0.0571 - 0.0856" ,"0.0856 - 0.1142" ,"0.1142 +") ,
					  "datasets" => array(
										  array(
										   "data" => array($range1,$range2,$range3,$range4,$range5) ,
										   "backgroundColor" => array("rgb(0, 0, 255)" ,"rgb(0, 255, 255)" ,"rgb(255, 255, 26)" ,"rgb(255, 128, 0)" ,"rgb(255, 0, 0)" ) ,
										   "hoverBackgroundColor" => array("rgb(0, 64, 255)" , "rgb(128, 255, 255)" , "rgb(255, 255, 77)" ,"rgb(255, 64, 0)" ,"rgb(204, 0, 0)")
										 ) 
										 ) 
					  
					  );
					
				  return $output ;
					
			      
			    }else{
				
				   return 0 ;
				}
		}
		
	}
	
	
	 public function reprintsdatatable(Request $request , $fromdate = null , $todate =null)
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints']))
				{
					  DB::statement(DB::raw('set @rownum=0'));
						//$matchThese = ['reprintstatus' => 1 , 'status' => 1 ];
						
						 if($request->has('fromdate') && $request->has('todate') ){
					    $fromdate = Carbon::parse($request->fromdate) ;
						$todate= Carbon::parse($request-> todate) ;
						
						
						$allrecords = Docprint::where('prints.reprintstatus', 1)
						                 ->whereBetween('prints.created_at', [$fromdate, $todate])
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									 \DB::raw('count(prints.car_id) as count'),
									'cars.chassisno','cars.customsno','cars.name' ,'prints.car_id'])
									->join('cars', 'cars.id', '=', 'prints.car_id')
									->groupBy('prints.car_id');
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							  
							   ->addColumn('action', function ($allrecords ) {
								  
								   
								     $carIdUrl =  route('admin.reprints.single' ,['id' => $allrecords->car_id ]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View Users</a>' ;
									
									})
									   
							   ->make(true);
							   
											 
					     }else{
							 
							 $allrecords = Docprint::where('reprintstatus', 1)
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									 \DB::raw('count(prints.car_id) as count'),
									'cars.chassisno','cars.customsno','cars.name' ,'prints.car_id'])
									->join('cars', 'cars.id', '=', 'prints.car_id')
									->groupBy('prints.car_id');
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							   
							   ->filter(function ($query) use ($fromdate , $todate ) {
								   
									   if($fromdate && $todate){
									   
											$fromdate = Carbon::parse($fromdate) ;
											$todate= Carbon::parse($todate) ;
											$query->whereBetween('prints.created_at', [$fromdate, $todate]);
										} 
									
								   

									
								})
									
							   ->addColumn('action', function ($allrecords ) {
								  
								   
								     $carIdUrl =  route('admin.reprints.single' ,['id' => $allrecords->car_id ]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View Users</a>' ;
									
									})
									   
							   ->make(true);
							 
						}
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
     public function singlereprintdatatable($id , Request $request)
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints']))
				{
				  
				  DB::statement(DB::raw('set @rownum=0'));
						//$matchThese = ['reprintstatus' => 1 , 'status' => 1 ];
							  $allrecords = Docprint::where('reprintstatus', 1)
							              ->where('car_id', $id)
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									 \DB::raw('count(prints.user_id) as count'),
									'users.first_name as first_name','users.last_name as last_name','prints.car_id','prints.user_id'])
									->join('users', 'users.id', '=', 'prints.user_id')
									->groupBy('prints.user_id');
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							   
							   
									
							   ->addColumn('action', function ($allrecords ) {
								  
								   
								     $carIdUrl =  route('admin.reprints.singledates' ,['id' => $allrecords->car_id ,'userid' => $allrecords->user_id]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View Dates Reprinted</a>' ;
									
									})
							 ->editColumn('first_name', '{{$first_name}} {{$last_name}}')
									   
							   ->make(true);
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
    
     public function singlereprintwithdatesdatatable($id , $userid , Request $request)
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints']))
				{
				  
				  DB::statement(DB::raw('set @rownum=0'));
						//$matchThese = ['reprintstatus' => 1 , 'status' => 1 ];
							  $allrecords = Docprint::where('reprintstatus', 1)
							              ->where('car_id', $id)
							              ->where('user_id', $userid)
							              ->join('users', 'users.id', '=', 'prints.user_id')
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									'users.first_name as first_name','users.last_name as last_name','prints.created_at']);
									
									
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							   
							 ->editColumn('first_name', '{{$first_name}} {{$last_name}}')
									   
							   ->make(true);
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
     public function editsdatatable(Request $request , $fromdate = null , $todate =null)
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.reprints']))
				{
					  DB::statement(DB::raw('set @rownum=0'));
						//$matchThese = ['reprintstatus' => 1 , 'status' => 1 ];
						
						 if($request->has('fromdate') && $request->has('todate') ){
					    $fromdate = Carbon::parse($request->fromdate) ;
						$todate= Carbon::parse($request-> todate) ;
						
						
						$allrecords = Docprint::where('prints.reprintstatus', 2)
						                  ->where('prints.status', 1)
						                 ->whereBetween('prints.created_at', [$fromdate, $todate])
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									 \DB::raw('count(prints.car_id) as count'),
									'cars.chassisno','cars.customsno','cars.name' ,'prints.car_id'])
									->join('cars', 'cars.id', '=', 'prints.car_id')
									->groupBy('prints.car_id');
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							  
							   ->addColumn('action', function ($allrecords ) {
								  
								   
								     $carIdUrl =  route('admin.edits.single' ,['id' => $allrecords->car_id ]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View Users</a>' ;
									
									})
									   
							   ->make(true);
							   
											 
					     }else{
							 
							 $allrecords = Docprint::where('reprintstatus', 2)
							             ->where('prints.status', 1)
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									 \DB::raw('count(prints.car_id) as count'),
									'cars.chassisno','cars.customsno','cars.name' ,'prints.car_id'])
									->join('cars', 'cars.id', '=', 'prints.car_id')
									->groupBy('prints.car_id');
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							   
							   ->filter(function ($query) use ($fromdate , $todate ) {
								   
									   if($fromdate && $todate){
									   
											$fromdate = Carbon::parse($fromdate) ;
											$todate= Carbon::parse($todate) ;
											$query->whereBetween('prints.created_at', [$fromdate, $todate]);
										} 
									
								   

									
								})
									
							   ->addColumn('action', function ($allrecords ) {
								  
								   
								     $carIdUrl =  route('admin.edits.single' ,['id' => $allrecords->car_id ]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View Users</a>' ;
									
									})
									   
							   ->make(true);
							 
						}
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
      public function singleditdatatable(Request $request ,$id  )
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.edits.single']))
				{
				  
				  DB::statement(DB::raw('set @rownum=0'));
						//$matchThese = ['reprintstatus' => 1 , 'status' => 1 ];
							  $allrecords = Docprint::where('reprintstatus', 2)
							              ->where('prints.status', 1)
							              ->where('prints.car_id', $id)
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									 \DB::raw('count(prints.user_id) as count'),
									'users.first_name as first_name','users.last_name as last_name','prints.car_id','prints.user_id'])
									->join('users', 'users.id', '=', 'prints.user_id')
									->groupBy('prints.user_id');
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							   
							   
									
							   ->addColumn('action', function ($allrecords ) {
								  
								   
								     $carIdUrl =  route('admin.edits.singlecareditdates' ,['id' => $allrecords->car_id ,'userid' => $allrecords->user_id]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View Dates Edited</a>' ;
									
									})
							 ->editColumn('first_name', '{{$first_name}} {{$last_name}}')
									   
							   ->make(true);
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    
    public function singlereeditwithdatesdatatable(Request $request , $id , $userid )
    {
         if ($user = Sentinel::check())
		{
			  if ($user->hasAccess(['admin.edits.single']))
				{
				  
				  DB::statement(DB::raw('set @rownum=0'));
						//$matchThese = ['reprintstatus' => 1 , 'status' => 1 ];
							  $allrecords = Docprint::where('reprintstatus', 2)
							              ->where('prints.status', 1)
							              ->where('prints.car_id', $id)
							              ->where('prints.user_id', $userid)
							              ->join('users', 'users.id', '=', 'prints.user_id')
							             ->select([
									 DB::raw('@rownum  := @rownum  + 1 AS rownum'),
									'prints.caredit_id','prints.editedcar_id','users.first_name as first_name','users.last_name as last_name','prints.created_at','prints.car_id','prints.user_id']);
									
									
									
									
									
							   
							  
							   
							   return Datatables::of($allrecords)
							    ->addColumn('action', function ($allrecords ) {
								     $date = Carbon::parse($allrecords -> created_at) ->timestamp ;
								   
								     $carIdUrl =  route('admin.edits.singlecareditchanges' ,
								     ['id' => $allrecords->car_id ,
								     'edituserid' => $allrecords->user_id ,
								     'printdate' => $date ,
								     'editedcarid'=> $allrecords->editedcar_id ,
								     'careditid'=> $allrecords->caredit_id 
								     
								     ]) ;
								   
								     return '<a class="btn btn-success" href="'.$carIdUrl.'">View  Edit Changes</a>' ;
									
									})
							 ->editColumn('first_name', '{{$first_name}} {{$last_name}}')
									   
							   ->make(true);
			      
			    }else{
				
				   return 1 ;
				}
		}
        
       return 0 ;
       
    }
    public function testreplicate(){
		$id = 2 ;
		$user_id  = 1 ;
		$car = Car::where('id', $id)->first();
          
          $clone =  $car->replicate();
			$clone_inspection =  $car->carinspection->replicate();
		
			unset($clone['id'],$clone['user_id'],$clone['status'],$clone['created_at'],$clone['updated_at']);
			unset($clone_inspection['user_id'] ,$clone_inspection['status'],$clone_inspection['id'],$clone_inspection['car_id'],$clone_inspection['created_at'],$clone_inspection['updated_at']);
			
			$dataclone = json_decode($clone, true);
			$dataclone_inpections = json_decode($clone_inspection, true);
			$dataclone['user_id'] = $user_id ;
			$dataclone['car_id'] = $id ;
		    $dataclone['status'] = 1  ; 
		    $clonedcar = Caredit::create($dataclone);
		    
			$dataclone_inpections['caredit_id'] = $clonedcar -> id ;
			$dataclone_inpections['user_id'] = $user_id ;
		    $dataclone_inpections['status'] = 1  ; 
			$clonedcar_inspection = Editcarinspections::create($dataclone_inpections);
            return $clone_inspection ;
		
	}
    
    
    
    
    


}
