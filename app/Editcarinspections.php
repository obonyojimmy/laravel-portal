<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editcarinspections extends Model
{
    //
    protected $table = 'editcarinspections';
     protected $fillable = [
        'caredit_id', 'user_id', 'background','exterior','engine' ,'interior' ,'extras' ,'info' ,'status'
    ];
    
    public function setBackgroundAttribute($value)
    {
        $this->attributes['background'] = json_encode($value);
    }
    public function setExteriorAttribute($value)
    {
        $this->attributes['exterior'] = json_encode($value);
    }
    public function setEngineAttribute($value)
    {
        $this->attributes['engine'] = json_encode($value);
    }
    public function setInteriorAttribute($value)
    {
        $this->attributes['interior'] = json_encode($value);
    }
    public function setExtrasAttribute($value)
    {
        $this->attributes['extras'] = json_encode($value);
    }
    
    protected $casts = [
        'background' => 'array',
        'exterior' => 'array',
        'engine' => 'array',
        'interior' => 'array',
        'extras' => 'array',
    ];
    
    
    public function user()
    {
        return $this->belongsTo('App\User' );
    }
    
    public function caredit()
    {
		return $this->belongsTo('App\Caredit','caredit_id');
        
    }
}
