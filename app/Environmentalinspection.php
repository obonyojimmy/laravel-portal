<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Environmentalinspection extends Model
{
    //
    
    protected $table = 'environmentalinspection';
    
     protected $fillable = [
        'environmentalrecord_id', 'user_id', 'background','netcounts','info' 
    ];
    
     public function user()
    {
        return $this->belongsTo('App\User' );
    }
    
    public function environmentalrecord()
    {
		return $this->belongsTo('App\Environmentalrecord', 'environmentalrecord_id');
        
    }
}
