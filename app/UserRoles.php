<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    //
     protected $table = 'role_user';
     
     protected $fillable = [
        'user_id',
        'role_id'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
