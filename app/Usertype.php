<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usertype extends Model
{
    //
     protected $fillable = [
        'name'
    ];
    protected $table = 'usertype';
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
