<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->default('notavailable');
            $table->string('idnumber')->default('notavailable');
            $table->string('address')->default('notavailable');
             $table->boolean('status')->default(0);
            $table->string('phone')->default('notavailable');
            $table->integer('user_id')->unsigned();
            $table->float('subtotal');
            $table->float('tax');
            $table->float('total');
            $table->longText('otherinfo');
            $table->longText('items');
            $table->timestamp('paymentdue')->nullable();;
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
