<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnvironmentalrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('environmentalrecords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('batchcode')->nullable();
            $table->string('batchsize')->nullable();
            $table->integer('destcountry_id')->unsigned()->nullable();
            $table->integer('ship_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('customsno')->unique()->nullable();
            $table->string('port')->nullable();
            $table->integer('importer_id')->unsigned()->nullable();
            $table->integer('clearingagent_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->timestamp('manufacture')->nullable();
            $table->timestamp('expiry') ->nullable();
            $table->timestamps();
            
            $table->foreign('ship_id')->references('id')->on('ships');
            $table->foreign('destcountry_id')->references('id')->on('destcountry');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('clearingagent_id')->references('id')->on('clearingagents');
            $table->foreign('importer_id')->references('id')->on('importers');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('environmentalrecords');
    }
}
