<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('voyage');
             $table->longText('info');
            $table->timestamp('arrival');
            $table->timestamp('departure');
            $table->boolean('status');
            $table->integer('user_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
        
        //~ Schema::table('ships', function ($table) {
			//~ $table->string('name')->nullable()->change();;
             //~ $table->longText('info')->nullable()->change();;
            //~ $table->timestamp('arrival')->nullable()->change();;
            //~ $table->timestamp('departure') ->nullable()->change();;
            //~ $table->integer('user_id')->unsigned() ->nullable()->change();;
		//~ });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ships');
    }
}
