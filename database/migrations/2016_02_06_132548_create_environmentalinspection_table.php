<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnvironmentalinspectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('environmentalinspection', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('environmentalrecord_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('background');
            $table->string('netcounts');
            $table->longText('info');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('environmentalrecord_id')->references('id')->on('environmentalrecords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('environmentalinspection');
    }
}
