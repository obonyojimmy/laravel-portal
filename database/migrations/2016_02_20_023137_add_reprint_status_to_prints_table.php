<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReprintStatusToPrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prints', function (Blueprint $table) {
            //
            $table->boolean('reprintstatus')->nullable(); // null = no reprint , 1 = reprint only , 2 = edit + reprint ,
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prints', function (Blueprint $table) {
            //
        });
    }
}
