<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caredits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('chassisno')->nullable();
            $table->string('customsno')->nullable()->nullable();
            $table->string('colour')->nullable();
            $table->integer('prevcountry_id')->unsigned()->nullable();
            $table->string('engineno')->nullable();
            $table->integer('year')->nullable();
            $table->string('fuel')->nullable();
            $table->string('sticker_no')->default('N/A');
            $table->string('previousreg')->nullable();
            $table->integer('destcountry_id')->unsigned()->nullable();
            $table->boolean('status')->default(0); // 1 = before edit , 2 = after edit
            $table->integer('ship_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('clearingagent_id')->unsigned()->nullable();
            $table->integer('importer_id')->unsigned()->nullable();
            $table->integer('car_id')->unsigned()->nullable();
          
            $table->timestamps();
            
            $table->foreign('ship_id')->references('id')->on('ships');
            $table->foreign('car_id')->references('id')->on('cars');
            $table->foreign('prevcountry_id')->references('id')->on('prevcountry');
            $table->foreign('destcountry_id')->references('id')->on('destcountry');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('clearingagent_id')->references('id')->on('clearingagents');
            $table->foreign('importer_id')->references('id')->on('importers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('caredits');
    }
}
