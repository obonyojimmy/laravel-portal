<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCareditIdToPrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prints', function (Blueprint $table) {
            //
             $table->integer('caredit_id')->unsigned()->nullable(); // id of car before edit refrencing in caredits table
             
             $table->foreign('caredit_id')->references('id')->on('caredits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prints', function (Blueprint $table) {
            //
        });
    }
}
