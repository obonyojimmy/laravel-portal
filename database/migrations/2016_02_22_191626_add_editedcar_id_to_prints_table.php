<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEditedcarIdToPrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prints', function (Blueprint $table) {
            //
             $table->integer('editedcar_id')->unsigned()->nullable(); // id of car after edit  refrencing in caredits table
           
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prints', function (Blueprint $table) {
            //
        });
    }
}
