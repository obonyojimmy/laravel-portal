<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditcarinspectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('editcarinspections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caredit_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('background');
            $table->string('exterior');
            $table->string('engine');
            $table->string('interior');
            $table->string('extras');
            $table->boolean('status')->default(0); // 1 = before edit , 2 = after edit
            $table->longText('info')->nullable();
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('caredit_id')->references('id')->on('caredits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('editcarinspections');
    }
}
