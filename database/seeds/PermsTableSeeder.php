<?php

use Illuminate\Database\Seeder;

class PermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         // DB::table('perms')->delete();
        $permissions = array(
            'Dashboard' => 'admin.dashboard.index' ,
            'Save Ship' => 'admin.newship'  ,
            'Save Car Inspection ' => 'admin.car.carinspection'  ,
            'Save Inspected Car' => 'admin.car.inspectedcar' ,
            'Save Full Car' => 'admin.car.fullcarrecord' ,
            'Table All Cars' => 'admin.car.allcarrecords' ,
            'Reprint Car Record' => 'admin.car.reprint' ,
            'Edit Car Record' => 'admin.car.edit' ,
            'New Environmental Record' => 'admin.environmental.newenvironmentalrecord',
            'All Environmental Records' => 'admin.environmental.allenvironmentalrecords',
            'Reprint Environmental Record' => 'admin.environmental.reprint' ,
            'Edit Environmental Record' => 'admin.environmental.edit' ,
            'New Invoice Record' => 'admin.invoice.newinvoice' ,
            'All Invoice Records' => 'admin.invoice.allinvoice',
            'Reprint Invoice Record' => 'admin.invoice.reprint' ,
            'Analysis Chart Car records' => 'admin.analysis.carrecords',
            'Analysis Chart Environmental records' => 'admin.analysis.envrecords' ,
            'Analysis Quartiles Car  records' => 'admin.analysis.quartiles' ,
            'New User ' => 'admin.users.register' ,
            'Manage User Roles' => 'admin.users.usersroles' ,
            'Manage Single Roles' => 'admin.users.singlerole',
            'New Single Roles' => 'admin.users.newsinglerole',
            'All Users Records' => 'admin.users.listuser',
             'Manage Single User' => 'admin.users.user',
             'Update Single User' => 'api.users.update',
             'All Reprints' => 'admin.reprints',
             'Users Reprinted A single Car-record' => 'admin.reprints.single',
             'All Car Record Edits' => 'admin.edits',
            'Users Edited A single Car-record' => 'admin.edits.single',
            );
            $perms = array();
            foreach($permissions  as $p => $permissions ){
				 array_push($perms ,['slug' => $p ,'name' => $permissions] );
			}
         
         
        DB::table('perms')->insert($perms);
    }
}
