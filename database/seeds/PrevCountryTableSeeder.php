<?php

use Illuminate\Database\Seeder;
use CountryState as CountrySt;
use App\PrevCountry as Prev ;

class PrevCountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('prevcountry')->delete();
        
        $countries = CountrySt::getCountries();
        
       
           foreach($countries as $c => $country){
			   
			      $prev= new Prev ;
				  $prev -> name = $country ;
				  $prev -> code = $c ;
				  $prev -> save() ;
			   
			
			}
         
        //DB::table('prevcountry')->insert($count);
    }
}
