<?php

use Illuminate\Database\Seeder;

class UsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->delete();
        
         $roles = array(
            ['id' => 1, 'name' => 'Role 1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name' => 'Role 2', 'created_at' => new DateTime, 'updated_at' => new DateTime], 
            ['id' => 3, 'name' => 'Role 3', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'name' => 'Role 4', 'created_at' => new DateTime, 'updated_at' => new DateTime] 
           );
         
        DB::table('roles')->insert($roles);
        
        
    }
}
