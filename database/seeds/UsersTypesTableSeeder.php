<?php

use Illuminate\Database\Seeder;

class UsersTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('usertype')->delete();
        
         $usertype = array(
            ['id' => 1, 'name' => 'Usertype 1', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name' => 'Usertype 2', 'created_at' => new DateTime, 'updated_at' => new DateTime], 
            ['id' => 3, 'name' => 'Usertype 3', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 4, 'name' => 'Usertype 4', 'created_at' => new DateTime, 'updated_at' => new DateTime] 
           );
         
        DB::table('usertype')->insert($usertype);
    }
}
