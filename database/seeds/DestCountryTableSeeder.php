<?php

use Illuminate\Database\Seeder;
use CountryState as CountrySt ;
use  App\DestCountry as Dest ;
class DestCountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('destcountry')->delete();
        
        $countries = CountrySt::getCountries();
        
          foreach($countries as $c => $country){
			   
			      $dest= new Dest ;
				  $dest -> name = $country ;
				  $dest -> code = $c ;
				  $dest -> save() ;
			   
			
			}
         
       // DB::table('destcountry')->insert($count);
    }
}
