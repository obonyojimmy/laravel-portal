var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.sass('app.scss');
    
    mix.copy('resources/assets/vendor/AdminLTE/dist/js/**/*.js', 'public/js');
    mix.copy('resources/assets/vendor/AdminLTE/dist/css/AdminLTE.min.css', 'resources/assets/css/main.min.css');
    mix.copy('resources/assets/vendor/AdminLTE/dist/css/skins/*.css', 'public/css/skins');
    mix.copy('resources/assets/vendor/AdminLTE/dist/img', 'public/img');
    
    mix.copy('resources/assets/vendor/components-font-awesome/fonts', 'public/fonts');
    mix.copy('resources/assets/vendor/components-font-awesome/css/font-awesome.min.css', 'public/css');
    
    mix.copy('resources/assets/vendor/Ionicons/fonts', 'public/fonts');
    
    
    mix.copy('resources/assets/vendor/Ionicons/css/ionicons.min.css', 'public/css');
   
    mix.copy('resources/assets/vendor/AdminLTE/plugins', 'public/plugins');
    
    mix.copy('resources/assets/vendor/bootstrap/fonts', 'public/fonts');
    
    mix.copy('resources/assets/vendor/moment/min', 'public/plugins/moment');
    
    mix.copy('resources/assets/css/fonts', 'public/fonts');
    
    mix.copy('resources/assets/vendor/typeahead.js/dist', 'public/plugins/typeahead');
    
    mix.copy('resources/assets/vendor/datatables.net/js/jquery.dataTables.min.js', 'public/js');
    
    mix.copy('resources/assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js', 'public/js');
    
    mix.copy('resources/assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css', 'public/css');
    
    mix.copy('resources/assets/vendor/datatables.net-buttons/js', 'public/js');
   
    mix.copy('resources/assets/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js', 'public/js');
     
    mix.copy('resources/assets/vendor/datatables.net-buttons-bs/css/buttons.bootstrap.min.css', 'public/css');
    
    mix.copy('resources/assets/vendor/datatables.net-responsive/js/dataTables.responsive.min.js', 'public/js');
    
    mix.copy('resources/assets/vendor/jszip/dist/jszip.min.js', 'public/js');
    
    mix.copy('resources/assets/vendor/pdfmake/build/pdfmake.min.js', 'public/js');
    
    mix.copy('resources/assets/vendor/pdfmake/build/vfs_fonts.js', 'public/js');
    
    mix.copy('resources/assets/vendor/Chart.js/Chart.js', 'public/js');
    
     mix.copy('resources/assets/vendor/growl/stylesheets/jquery.growl.css', 'public/css');
   
    mix.copy('resources/assets/vendor/growl/javascripts/jquery.growl.js', 'public/js');
    
    mix.copy('resources/assets/vendor/bootstrap-checkbox/js/bootstrap-checkbox.js', 'public/js');
    
    mix.copy('resources/assets/vendor/JsBarcode', 'resources/assets/js/JsBarcode');
    
    mix.copy('resources/assets/vendor/watermarkjs/dist/watermark.min.js', 'public/js');
    
    mix.less('bootstrap.less');
    //~ mix.less('theme/**/*.less','public/stylesheet');
    
    mix.scripts([
    '../vendor/jquery/dist/jquery.js',
    '../vendor/bootstrap/dist/js/bootstrap.js',
     '../vendor/bootbox.js/bootbox.js'
    ], 'public/js/bootstrap.js');
    
     mix.scripts([
    'bloodhoundship.js' ,
    'bloodhoundcarmake.js',
    'bloodhoundcarcolours.js',
    'bloodhoundclearingagents.js',
    'bloodhoundimportersname.js',
    'bloodhoundimportersaddress.js'
    ], 'public/js/bloodhoundLaravel.js');
    
     //~ mix.scripts([
    //~ 'newuserrole.js' ,
   
    //~ ], 'public/js/userrolesperms.js');
    
   
    
     mix.scripts([
    'singleuserchart.js' 
    ], 'public/js/appcharts.js');
    
     mix.scripts([
    'JsBarcode/CODE128.js',
    'JsBarcode/CODE39.js',
    'JsBarcode/EAN_UPC.js',
    'JsBarcode/ITF.js',
    'JsBarcode/ITF14.js',
    'JsBarcode/pharmacode.js',
    'JsBarcode/JsBarcode.js' 
    ], 'public/js/JsBarcode.all.js');
    
    mix.scripts([
    'chartcarrecords.js' 
    ], 'public/js/chartcarrecords.js');
    
    mix.scripts([
    'daterangequartilespiechart.js' 
    ], 'public/js/daterangequartilespiechart.js');
    
    mix.scripts([
    'chartenvrecords.js' 
    ], 'public/js/chartenvrecords.js');
    
    
    
    
     mix.scripts([
    'quartilespiechart.js' 
    ], 'public/js/quartilespiechart.js');
    
    mix.scripts([
    'dashchartcarrecords.js' 
    ], 'public/js/dashchartcarrecords.js');
    
    mix.scripts([
    'randomgenerator.js' 
    ], 'public/js/randomgenerator.js');
    
    mix.scripts([
    'getdailynotification.js' 
    ], 'public/js/getdailynotification.js');
   
    mix.styles([
        'fonts.css',
        'print.css',
        'theme.css',
        'main.min.css' 
        
        ]);
  
    
     mix.scripts([
         'main.js',
         'newuser.js' ,
         'login.js' ,
         'newship.js' ,
         'newcarinspection.js',
         'saveinspectedcar.js',
         'printInspectedcar.js',
         'fullcarsave.js',
         'printfullcar.js',
         'updateuser.js',
         'editfullcarsave.js',
         'printeditfullcarsave.js',
         'fullenvironmentalrecordsave.js',
         'fullenvironmentalrecordprint.js',
         'editenvironmentalsave.js',
         'editfullenvironmentalrecordprint.js',
         'singleinvoiceadditem.js' ,
         'singleinvoicesave.js' ,
         'singleinvoiceprint.js'
        
    ]);
    
  
});

  
